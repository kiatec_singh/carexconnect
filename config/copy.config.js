module.exports = {
    copyFontAwesome: {
      src: ['{{ROOT}}/node_modules/@fortawesome/fontawesome-free/webfonts/**/*'],
      dest: '{{WWW}}/assets/fonts'
    }
  };