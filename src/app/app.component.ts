import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home.page';
import { StorageService } from '../providers/storageservice/storageservice';
import { BaseRestService } from '../providers/restservice/base.rest.service';
import { Keyboard } from '@ionic-native/keyboard';
import { ConfigurationService } from '../providers/utils/configservices';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public rootPage: any = HomePage;
  public deployChannel = 'master';
  public downlaodProgress = 0;
  public appconfig;
  public bg_color;


  pages: Array<{ title: string, component: any }>;
  constructor(public platform: Platform, public statusBar: StatusBar, 
    public keyboard: Keyboard, public splashScreen: SplashScreen,
    public configurationService: ConfigurationService,
    public baserestService: BaseRestService, public storageService: StorageService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.checkForUpdates();
      this.statusBar.styleLightContent();
      this.statusBar.overlaysWebView(true);
      //this.keyboard.disableScroll(false);
     // this.keyboard.hideFormAccessoryBar(false);
      //this.keyboard.hideKeyboardAccessoryBar(false);
      this.splashScreen.hide();
    });
    this.platform.resume.subscribe(() => {
        this.checkForUpdates();
      });

  }

  checkForUpdates() {
    let version = this.configurationService.getAppVersionNumber();
    this.configurationService.checkForceUpdate(version);
  }
}