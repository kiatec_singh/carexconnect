import { Injectable } from '@angular/core';
// import { Toast } from '@ionic-native/toast';
import { ToastController } from 'ionic-angular';
import { NotificationsPage } from '../../pages/notifications/notifications.page';
import { App } from "ionic-angular";
import { AuthService } from '../authenticationservice/auth.service';
import { ChangelanguageService } from '../utils/changelanguageservice';


@Injectable()
export class PushNotificationService {

    private notifycountvalue;
    public appconfig;
    public selectedlanguage;
    private notificationsotoastContent;
    constructor(private app: App, private lang:ChangelanguageService,private toastCtrl: ToastController, private auth: AuthService) {
        this.auth.notifycount.subscribe(
            (notifycountvalue) => {
                    if (notifycountvalue) {
                        this.notifycountvalue = notifycountvalue;
                    }
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    if (appconfig && appconfig.environment) {
                        this.appconfig = appconfig
                    }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if (selectedlanguage) {
                    this.selectedlanguage = selectedlanguage;
                }
                }
           );
    }

    showtoptoastMessage(data) {
        this.notificationsotoastContent = this.selectedlanguage.notificationstoast;
        let nav = this.app.getActiveNav();
        let toast = this.toastCtrl.create({
            message: data.body,
            duration: this.notificationsotoastContent.duration,
            position: this.notificationsotoastContent.position,
            showCloseButton: true,
            closeButtonText: this.notificationsotoastContent.show
        });
        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                nav.setRoot(NotificationsPage)
                console.log('closed...');
            }
        });
        toast.present();
    }
    getnotificationCount(data) {
        if (data) {
            this.notifycountvalue = this.notifycountvalue++;
            this.auth.setnotifycount(this.notifycountvalue);
            // this.ref.detectChanges();
        }
    }
    // notificaitonSwitch(data) {
    //     let nav = this.app.getActiveNav();
    //     if (data.wasTapped) {
    //         alert(data.message);
    //        nav.setRoot(NotificationsPage)
    //       //  this.navCtrl.push(NotificationsPage);
    //         console.log("Received in background");
    //     } else {
    //         this.showtoptoastMessage(data.message);
    //         console.log("in foreground");
    //     };
    // }
}