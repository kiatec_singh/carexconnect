import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm';
import { Platform, App } from 'ionic-angular';
import { Push } from '@ionic-native/push';
import { PushNotificationService } from './pushnotificationservice';
import { BaseRestService } from '../restservice/base.rest.service';
import { AuthService } from '../authenticationservice/auth.service';
import { Device } from '@ionic-native/device';
// import * as firebase from 'firebase';
// import * as firebase from 'firebase/app';
import { NotificationsPage } from '../../pages/notifications/notifications.page';
import { HomePage } from '../../pages/home/home.page';
import { HelperSerice } from '../utils/helperService';

@Injectable()
export class FcmProvider {

    private userinfo: any;
    private autorisation: any;
    private appconfig: any;
    public hostname: any;
    rootPage: any = HomePage;
    private notifycountvalue;

    constructor(public fcm: FCM, public push: Push, public app: App, private helperservice: HelperSerice, public baserestService: BaseRestService, private auth: AuthService, private device: Device,
        private platform: Platform) {
        this.auth.user.subscribe(
            (userinfo) => {
                this.userinfo = userinfo;
            }
        );
        this.auth.notifycount.subscribe(
            (notifycountvalue) => {
                if (notifycountvalue) {
                    this.notifycountvalue = notifycountvalue;

                }
            }
        );
        this.auth.autorisation.subscribe(
            (autorisation) => { this.autorisation = autorisation }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                this.appconfig = appconfig;
            }
        );
    }

    subscribforNotifications() {
        let ismobileapp = this.helperservice.isApp();
        if (ismobileapp === true) {
            this.platform.ready().then(() => {
                if (this.platform.is('cordova')) {
                    this.fcm.subscribeToTopic('all');
                    this.fcm.getToken().then(token => {
                        this.registeruserToken(token);
                    });
                    // deprecated Token Refresh as per docs.
                    // this.fcm.onTokenRefresh().subscribe(token => {
                    //     console.log("token refreshed");
                    //     console.log(token);
                    //     this.registeruserToken(token);
                    // });
                }
            });
        }
        else {
            this.checkforunreadNotifications();
        }

    }
    registeruserToken(token) {
        if (token) {
            let deviceuuid = this.device.uuid;
            if(deviceuuid){
                this.fcmRegister(deviceuuid,token);
            }
        }
    }
    fcmRegister(deviceuuid,token) {
        let platform = this.platform.is('ios') ? 'ios' : 'android';
        let devicesrno = this.device.serial;
        let status = 'aktiv';
        this.baserestService.fcmtokenRegister(deviceuuid, devicesrno, this.userinfo.id, token, this.autorisation, status, platform).then(success => {
            console.log(success);
            this.checkforunreadNotifications();
            this.onNotification();
        });
    }
    onNotification() {
        let nav = this.app.getActiveNav();
        this.fcm.onNotification().subscribe((data) => {
            console.log(data);
            // this.push.hasPermission().then(res => {
            //     console.log(res);
            //     //   this.pushnotificationservice.getnotificationCount(data);
            //     if (res.isEnabled && !data.wasTapped) {
            //         this.pushnotificationservice.showtoptoastMessage(data);
            //         console.log("Received in background");
            //         console.log(res);
            //     } else {
            //         console.log("Received in foreground");
            //         console.log(res);
            //     }
            // })
            if (data.wasTapped) {
                nav.setRoot(NotificationsPage);
                nav.popToRoot();
            }
        });
    }
    unsubscribforNotifications(status) {
        this.baserestService.turnoffnotification(this.autorisation, status).then(success => {
            console.log(success);
        });
    }
    getnotificationCount() {
        let d = this.notifycountvalue;
        d++;
        this.notifycountvalue = d;
        this.auth.setnotifycount(d);
        // this.ref.detectChanges();
    }
    checkforunreadNotifications() {
        this.baserestService.getnotifications(this.autorisation).then(
            allstorednotifications => {
                this.getcount(allstorednotifications);
            },
            error => {
                console.log("something went wrong")
            }
        )
    }
    getcount(allstorednotifications) {
        let notifycount = 0;
        if (allstorednotifications && allstorednotifications.status) {
            this.auth.setnotifycount(allstorednotifications.status.antal_unread);
        }
        else {
            this.auth.setnotifycount(notifycount);
        }
    }
}