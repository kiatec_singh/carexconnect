import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertService {

    private isOpened=false;

    constructor(private alrtCtrl: AlertController) {

    }
    showinternetError() {
        let alert: any;
        if (!this.isOpened) {
            this.isOpened = true;
            const alert = this.alrtCtrl.create({
                title: '<ion-icon ios="ios-warning" style="color: yellow;" md="md-warning"></ion-icon> ! No internet connection',
                subTitle: 'It looks like your internet connection is off. Please turn it on and try again.!',
                buttons: ['Ok']
            });
            alert.present();
        }
        alert.onDidDismiss(() => {
            this.isOpened = false;
        });
    }
}