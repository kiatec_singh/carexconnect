import { Injectable } from '@angular/core';
import { PopoverController, AlertController, Platform } from 'ionic-angular';
import { StorageService } from '../storageservice/storageservice';
import { BaseRestService } from '../restservice/base.rest.service';
import { Market } from '@ionic-native/market';
import { AuthService } from '../authenticationservice/auth.service';
import { Appinfo } from './appinfo';
import { environment } from '../../environments/environment';
import { ChangelanguageService } from './changelanguageservice';
import { HelperSerice } from './helperService';


@Injectable()
export class ConfigurationService {
    public deployChannel;
    public isBeta;
    public downloadProgress: any;
    public extractProgress: any;
    public isOpened: any;
    public appconfig: any;
    public selectedlanguage;



    constructor(private alertCtrl: AlertController, private market: Market,
        private helperservice: HelperSerice,
        private lang: ChangelanguageService,
        private storageService: StorageService, private platform: Platform, private auth: AuthService,
        public popoverCtrl: PopoverController, private appinfo: Appinfo,
        private baserestService: BaseRestService) {
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                {
                    this.appconfig = appconfig;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                this.selectedlanguage = selectedlanguage;
            }
        );
    }
    getAppVersionNumber() {
        return environment.version;
    }
    checkForceUpdate(version) {
        this.appinfo.setAppversion(version);
        this.storageService.set('version', version);
        let ismobileapp = this.helperservice.isApp();
        if (ismobileapp === true && this.platform.is('cordova')) {
            if (!this.appconfig || !this.appconfig.environment) {
                this.baserestService.getappConfig().then(
                    (appconfig) => {
                        if (appconfig) {
                            this.deployApp(version, appconfig);
                        }
                    },
                    (error) => { console.log(error) }
                );
            }
            if (this.appconfig && this.appconfig.environment) {
                this.deployApp(version, this.appconfig);
            }
        }
    }


    deployApp(version, appconfig) {
        console.log(appconfig);
        let update = this.compare(version, appconfig.environment.version);
        console.log(update);
        if (update == -1) {
            this.showAlert(appconfig);
        }
    }

    showAlert(appconfig) {
        let alert: any;
        let configdata;
        //needs refactor 
        if (this.selectedlanguage) {
            configdata = this.selectedlanguage.forceupdate;
        }
        if (!this.selectedlanguage) {
            configdata = appconfig.configuration.texts.da.forceupdate;
        }
        if (!this.isOpened) {
            this.isOpened = true;
            alert = this.alertCtrl.create({
                title: configdata.title,
                subTitle: configdata.subtitle,
                enableBackdropDismiss: false,
                buttons: [
                    {
                        text: configdata.text,
                        handler: data => {
                            this.isOpened = true;
                            if (this.platform.is('android')) {
                                let appid = configdata.androidappid;
                                this.market.open(appid);
                            } else {
                                let appid = configdata.iosappid;
                                this.market.open(appid);
                            }
                        }
                    }]
            });
            alert.setMode('ios');
            alert.present();
        }
        alert.onDidDismiss(() => {
            this.isOpened = false;
        });
    }


    // Return 1 if a > b
    // Return -1 if a < b
    // Return 0 if a == b
    compare(a, b) {
        //a = a.__zone_symbol__value;
        if (a && b) {
            if (a === b) {
                return 0;
            }
            var a_components = a.split(".");
            var b_components = b.split(".");
            var len = Math.min(a_components.length, b_components.length);
            // loop while the components are equal
            for (var i = 0; i < len; i++) {
                // A bigger than B
                if (parseInt(a_components[i]) > parseInt(b_components[i])) {
                    return 1;
                }
                // B bigger than A
                if (parseInt(a_components[i]) < parseInt(b_components[i])) {
                    return -1;
                }
            }
            // If one's a prefix of the other, the longer one is greater.
            if (a_components.length > b_components.length) {
                return 1;
            }
            if (a_components.length < b_components.length) {
                return -1;
            }
            // Otherwise they are the same.
            return 0;
        }

    }


}