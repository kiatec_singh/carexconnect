
import { Injectable } from '@angular/core';
import { AuthService } from '../authenticationservice/auth.service';
import { Location } from '@angular/common';
import { HomePage } from '../../pages/home/home.page';
import { Platform, App } from 'ionic-angular';

@Injectable()
export class HelperSerice {

    public hostname: any;
    public navbarlist: any;
    public tabstodisplay=[];
    constructor(private auth: AuthService,private location: Location, public platform: Platform, public app: App) {
        this.auth.navbarlist.subscribe(
            (navbarlist) => {
                this.navbarlist = navbarlist;
            } );
    }

    initializeApp() {
        this.auth.setwebtabsContent(null);
        this.auth.setwebtabstoDisplay(null);
        this.auth.setdetailContent(null);
        this.auth.setwebcontactContent(null);
        this.auth.setwebstressContent(null);
        this.auth.setwebhistroryquestionarieContent(null);
        this.auth.setwebsmartsearchContent('');
        this.auth.setwebQuestionaireContent(null);
        this.auth.setwebsearchdtlContent('');
        this.auth.setSpinner(false);

    }
    home() {
        this.auth.setSpinner(true);
        let activepage = this.app.getActiveNav().getActive.name;
        // if (!activepage || activepage != 'HomePage') {
        //     this.app.getActiveNav().setRoot(HomePage, {}, { animate: true, direction: 'back' });
        // }
        if (this.platform.width() > 1140) {
            let newnavbarlist = [];
            this.app.getActiveNav().setRoot(HomePage, {}, { animate: false, direction: 'back' });
            this.auth.setwebtabsContent(null);
            this.auth.setwebtabstoDisplay(null);
            this.auth.setdetailContent(null);
            this.auth.setwebstressContent(null);
            this.auth.setwebcontactContent(null);
            this.auth.setwebhistroryquestionarieContent(null);
            this.auth.setwebQuestionaireContent(null);
            this.navbarlist = newnavbarlist;
            this.auth.setnavbarlist(newnavbarlist);
            this.auth.setwebsmartsearchContent('');
            this.auth.setwebsearchdtlContent('');
        }
        else{
            this.app.getActiveNav().setRoot(HomePage, {}, { animate: true, direction: 'back' });
        }
        this.auth.setSpinner(false);
    }
    isApp() {
        let loc: any = this.location
        this.hostname = loc._platformStrategy._platformLocation.location.href;
        if (this.hostname.indexOf('carex') !== -1) {
            return false;
        }
        else {
            return true;
        }
    }
    setlistdetailsmenu(tabs) {
        let tabstodisplay = [];
        for (let item in tabs) {
            if (tabs[item].elementtype == 'button' && tabs[item].click_action) {
                tabstodisplay.push({
                    'name': tabs[item].funktionsnavn,
                    'icon': tabs[item].ikon,
                    'id': tabs[item].skema_uuid,
                    'type': tabs[item].elementtype,
                    'action': tabs[item].click_action,
                    'click_action_parameters': tabs[item].click_action_parameters ? tabs[item].click_action_parameters : null
                });
            }
            else {
                tabstodisplay.push({
                    'name': tabs[item].funktionsnavn,
                    'icon': tabs[item].ikon,
                    'id': tabs[item].skema_uuid,
                    'type': tabs[item].elementtype,
                    'tabstoDisplay': tabs[item].submenu,
                    'submenu_text':tabs[item].submenu_text,
                });
            }
        }
        this.tabstodisplay = tabstodisplay;
    }
    getlistdetailsmenu(){
    return this.tabstodisplay;
    }
}