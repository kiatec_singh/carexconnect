import { Injectable } from '@angular/core';
import { AuthService } from '../authenticationservice/auth.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class ChangelanguageService {

    private appconfig;
    public selectedlanguage = new BehaviorSubject<any>('');
    public userlanguage = new BehaviorSubject<any>('');


    constructor(private auth: AuthService) {
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    this.appconfig = appconfig;
            }
        );
    }
    setselectedlanguge(name) {
        this.selectedlanguage.next(this.appconfig.configuration.texts[name]);
    }
    setuserlang(name) {
        this.userlanguage.next(name);
    }
}