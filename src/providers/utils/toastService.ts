
import { Injectable } from '@angular/core';
import { Toast } from '@ionic-native/toast';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../authenticationservice/auth.service';
import { ChangelanguageService } from './changelanguageservice';
import { HelperSerice } from './helperService';

@Injectable()
export class ToastService {

    public appId: any;
    public envi;
    public selectedlanguage=null;
    public toastContent=null;
    public appconfig;

    constructor(private lang: ChangelanguageService, private helperService: HelperSerice, private toastr: ToastrService, private toastCtrl: Toast,private auth:AuthService) {
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if (selectedlanguage) {
                    this.selectedlanguage = selectedlanguage;
                    this.toastContent = this.selectedlanguage.toast;
                }}
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                this.appconfig = appconfig;
            }
          );
    }

    successblock(messagebody, messagetitle) {
        let isapp = this.helperService.isApp();
        if(this.toastContent && this.toastContent.toastdration){
                if (isapp === true) {
            const toast = this.toastCtrl.show(messagebody, this.toastContent.toastdration, this.toastContent.toastposition).subscribe(
                toast => {
                    console.log('toast');
                });
        }
        else {
            this.toastr.success(messagebody, messagetitle, {
                timeOut: this.toastContent.toastdration,
                positionClass: this.toastContent.webtoastposition
            });

        }   
        }
 
    }
    errorblock(messagebody, messagetitle) {
        let isApp = this.helperService.isApp();
        if(this.toastContent && this.toastContent.toastdration){
        if (isApp === true) {
            const toast = this.toastCtrl.show(messagebody, this.toastContent.toastdration, this.toastContent.toastposition).subscribe(
                toast => {
                    console.log('toast');
                });
        }
        else {
            let message_body = messagebody && messagebody!=''?messagebody:this.toastContent.submiterrormsg;
            let message_title = messagetitle && messagetitle!=''?messagetitle:this.toastContent.submiterrortitle;

            this.toastr.success(message_body, message_title, {
                timeOut: this.toastContent.toastdration,
                positionClass: this.toastContent.webtoastposition
            });
        }
    }
    }
}
