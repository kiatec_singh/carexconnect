
import { Injectable } from '@angular/core';
import { BaseRestService } from '../restservice/base.rest.service';
import { AuthService } from '../authenticationservice/auth.service';
import { EventLoggerProvider } from '../firebase/eventlogger';
import { Appinfo } from './appinfo';
import { HelperSerice } from './helperService';
import { ToastrService } from 'ngx-toastr';
import { ChangelanguageService } from './changelanguageservice';
import { Toast } from '@ionic-native/toast';



@Injectable()
export class MenugeneratorService {

    private slctdItem;
    private detailsdata;
    private tabs;
    private tabsgroup;
    // private navlist = [];
    private counter = 0;
    private tabscounter = 0;
    private appconfig: any;
    public navbarlist: any;
    public pagemenuButtons: any;
    public selectedlanguage: any;
    public questionairedataContent: any;


    constructor(private lang: ChangelanguageService, private toastCtrl: Toast,
        private baserestService: BaseRestService, private toastr: ToastrService, private helperService: HelperSerice, private auth: AuthService, private logprovider: EventLoggerProvider, private appInfo: Appinfo) {
        this.auth.appconfig.subscribe(
            (appconfig) => {
                this.appconfig = appconfig;
            }
        );
        this.auth.navbarlist.subscribe(
            (navbarlist) => {
                this.navbarlist = navbarlist;
            }
        );
        this.auth.pagemenuButtons.subscribe(
            (pagemenuButtons) => {
                this.pagemenuButtons = pagemenuButtons;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if (selectedlanguage) {
                    this.selectedlanguage = selectedlanguage;
                    this.questionairedataContent = selectedlanguage.questionaire;
                }
            }
        );
    }

    detailsPage(slctdItem, autorisation) {
        this.slctdItem = slctdItem;
        let tabstoDisplay = []
        if (this.slctdItem.click_action && this.slctdItem.elementtype !== "group_tabs" && this.slctdItem.elementtype !== "group_buttons") {
            if (this.slctdItem.click_action === "ReadSurveyAndAnswers") {
                let actionparameterkey = 'survey_uuid';
                let aktivitet_uuid = this.slctdItem.aktivitet_uuid ? this.slctdItem.aktivitet_uuid : '';
                if (this.appconfig.environment.trackanalytics === true) {
                    this.logprovider.logpageView(this.slctdItem.funktionsnavn);
                }
                this.baserestService.getquestionairdetails(this.slctdItem.click_action, actionparameterkey, this.slctdItem.click_action_parameters.survey_uuid, aktivitet_uuid, autorisation).then(
                    allquestionsdata => {
                        this.appInfo.setQuestionaireData(allquestionsdata);
                        this.auth.setwebQuestionaireContent(allquestionsdata);
                        this.generateNavigation();
                        this.auth.setSpinner(false);
                    },
                    error => { this.showError(); this.auth.setSpinner(false); }
                );
            }
            else if (this.slctdItem.click_action === "getSurveyResults") {
                if (this.appconfig.environment.trackanalytics === true) {
                    this.logprovider.logpageView(this.slctdItem.funktionsnavn);
                }
                this.baserestService.getsurveyResults(this.slctdItem.click_action, this.slctdItem.click_action_parameters.survey_uuid, autorisation).then(
                    webhistroryquestionarieContent => {
                        webhistroryquestionarieContent
                        this.auth.setwebhistroryquestionarieContent(webhistroryquestionarieContent);
                        this.naviDetails();
                        this.generateNavigation();
                    },
                    error => { this.showError(); this.auth.setSpinner(false); }
                )

            }
            else {
                if (this.slctdItem.click_action === "listObjectsSimpel") {
                    if (this.appconfig.environment.trackanalytics === true) {
                        this.logprovider.logpageView(this.slctdItem.funktionsnavn);
                    }
                }

                this.baserestService.getdetails(slctdItem.click_action, slctdItem.skema_uuid, autorisation).then(
                    detailsdata => {
                        this.detailsdata = detailsdata;
                        this.naviDetails();
                        this.generateNavigation();
                        if (this.slctdItem.elementtype && this.slctdItem.elementtype === "button" && this.slctdItem.click_action === "getSkemaIndhold") {
                            this.showdetailsPage(tabstoDisplay);
                        }
                    },
                    error => {
                        this.auth.setSpinner(false);
                    }
                )
            }

        }
        else if (this.slctdItem.elementtype && this.slctdItem.elementtype === "group_buttons") {
            this.generateNavigation();
            this.pagemenuButtons = this.slctdItem.submenu;
            this.auth.setpagemenuButtons(this.slctdItem.submenu);
            if (this.appconfig.environment.trackanalytics === true) {
                this.logprovider.logpageView(this.slctdItem.funktionsnavn);
            }
            if (this.slctdItem.submenu_text && this.slctdItem.submenu_text.length > 0) {
                this.auth.setdetailContent(this.slctdItem.submenu_text);
            }
        }
        else if (this.slctdItem.elementtype && this.slctdItem.elementtype === "group_tabs") {
            let tabsContent: any;
            this.tabs = this.slctdItem.submenu;
            let tabstoDisplay = [];
            let action = this.tabs[0].click_action;
            let id = this.tabs[0].skema_uuid;

            for (let item in this.tabs) {
                if (this.tabs[item].click_action) {
                    tabstoDisplay.push({
                        'name': this.tabs[item].funktionsnavn,
                        'icon': this.tabs[item].ikon,
                        'id': this.tabs[item].skema_uuid,
                        'type': this.tabs[item].elementtype,
                        'action': this.tabs[item].click_action
                    });
                }
                else {
                    tabstoDisplay.push({
                        'name': this.tabs[item].funktionsnavn,
                        'icon': this.tabs[item].ikon,
                        'id': this.tabs[item].skema_uuid,
                        'type': this.tabs[item].elementtype,
                    });
                }
            }

            this.baserestService.getdetails(action, id, autorisation).then(
                detailsdata => {
                    this.detailsdata = detailsdata;
                    this.showdetailsPage(tabstoDisplay);
                },
                error => {
                    console.log(error);
                    this.showError();
                    this.auth.setSpinner(false);
                }
            )
        }

    }
    showdetailsPage(tabstoDisplay) {
        this.auth.setdetailContent(this.detailsdata.indhold.indhold);
        this.auth.setwebtabstoDisplay(tabstoDisplay);
        // this.naviDetails();
        this.generateNavigation();
        if (this.appconfig.environment.trackanalytics === true) {
            if (this.navbarlist && this.navbarlist.length != 0) {
                let eventname = this.slctdItem.funktionsnavn;
                // let oldevent = this.navbarlist[this.navbarlist.length - 1];
                this.logprovider.logpageView(eventname);
            }

        }
        //this.navCtrl.push(AlldetailsPage, { 'tabsContent': this.tabsgroup.indhold.indhold, 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.slctdItem.funktionsnavn });
    }
    naviDetails() {
        let tabsContent = [];
        tabsContent = this.detailsdata && this.detailsdata.indhold && this.detailsdata.indhold.indhold ? this.detailsdata.indhold.indhold : [];
        let tabstoDisplay = [];

        if (this.slctdItem.click_action && this.slctdItem.click_action === "listObjectsSimpel") {
            this.appInfo.setSlctdItem(this.slctdItem);
            this.auth.setwebstressContent(this.detailsdata);
            this.auth.setSpinner(false);
        }
        if (this.slctdItem.click_action && this.slctdItem.click_action === "getSurveyResults") {
            this.appInfo.setSlctdItem(this.slctdItem);
            // this.auth.setwebstressContent(this.detailsdata);
            this.auth.setSpinner(false);
        }
        if (this.slctdItem.click_action && this.slctdItem.click_action === "getSkemaForm") {
            this.appInfo.setSlctdItem(this.slctdItem);
            this.appInfo.setContactData(this.detailsdata);
            this.auth.setwebcontactContent(this.detailsdata);
            this.auth.setSpinner(false);
        }
        if (this.slctdItem.click_action && this.slctdItem.click_action === "getSkemaForm") {
            this.appInfo.setSlctdItem(this.slctdItem);
            this.appInfo.setContactData(this.detailsdata);
            this.auth.setwebcontactContent(this.detailsdata);
            this.auth.setSpinner(false);
        }
    }

    generateNavigation() {
        if (this.navbarlist && this.navbarlist.length < 1) {
            this.navbarlist.push(this.slctdItem);
            this.auth.setnavbarlist(this.navbarlist);
        }
        else {
            let mainmenu: any = this.appInfo.getPagemenuItems();
            mainmenu.pagemenuObj.forEach(element => {
                if ((this.slctdItem.funktionsnavn == element.funktionsnavn) && (this.slctdItem.click_action === element.click_action)) {
                    if (this.navbarlist && this.navbarlist.length) {
                        let navlist: any = this.navbarlist;
                        navlist.length = 0;
                        navlist.push(this.slctdItem);
                        this.navbarlist = navlist;
                        this.auth.setnavbarlist(this.navbarlist);
                    }
                    else {
                        let navlist = [];
                        navlist.push(this.slctdItem);
                        this.navbarlist = navlist;
                        this.auth.setnavbarlist(this.navbarlist);
                    }
                }
                else {
                    if (element.submenu) {
                        element.submenu.forEach(innerelement => {
                            if ((this.slctdItem.funktionsnavn == innerelement.funktionsnavn) && (this.slctdItem.click_action === innerelement.click_action) && (this.slctdItem.brugervendtnoegle === innerelement.brugervendtnoegle)) {
                                var result = element.submenu.filter(o => this.navbarlist.some(({ funktionsnavn }) => o.funktionsnavn === funktionsnavn));
                                if (!result || result.length == 0) {
                                    let navlist = this.navbarlist;
                                    navlist.push(this.slctdItem);
                                    this.navbarlist = navlist;
                                    this.auth.setnavbarlist(this.navbarlist);
                                }
                                if (result && result.length > 0) {
                                    let navlist = this.navbarlist;
                                    navlist.pop();
                                    navlist.push(this.slctdItem);
                                    this.navbarlist = navlist;
                                    this.auth.setnavbarlist(this.navbarlist);
                                }
                            }
                        });
                    }
                }
            });

        }
        this.auth.setSpinner(false);

    }
    showError() {
        let isApp = this.helperService.isApp();
        if (isApp === true) {
            const toast = this.toastCtrl.show(this.questionairedataContent.submitsuccessmsg, this.questionairedataContent.toastdration, this.questionairedataContent.toastposition).subscribe(
                toast => {
                    console.log(toast);
                });
        }
        else {
            this.toastr.success(this.questionairedataContent.submiterrormsg, this.questionairedataContent.submiterrortitle, {
                timeOut: this.questionairedataContent.toastdration,
                positionClass: this.questionairedataContent.webtoastposition
            });

        }

    }

}