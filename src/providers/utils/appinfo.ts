import { Injectable } from '@angular/core';


@Injectable()
export class Appinfo {

    public orgId: any;
    public appId: any;
    public brandId:any;
    public searchObj:any;
    public pagemenuObj:any;
    public appversion:any;
    public contactData:any;
    public questionaireData:any;
    public usermenuObject:any;
    public item:any;

    constructor() {

    }
    setApplicationinfo(organizationid, applicationid, branduuid) {
        this.orgId = organizationid;
        this.appId = applicationid;
        this.brandId= branduuid;

    }
    getApplicationinfo() {
        return {
            'appId': this.appId,
            'orgId': this.orgId,
            'brandId': this.brandId
        }
    }
    setSearchedList(searchObj) {
        this.searchObj = searchObj;

    }
    getSearchedList() {
        return {
            'searchObj': this.searchObj
        }
    }
    setPagemenuItems(pagemenuObj) {
        this.pagemenuObj = pagemenuObj;

    }
    getPagemenuItems() {
        return {
            'pagemenuObj': this.pagemenuObj
        }
    }
    getAppversion() {
        return {
            'appversion': this.appversion
        }
        // return this.appversion;
    }
    setAppversion(appversion) {
        this.appversion = appversion;

    }
    getusermenuObject() {
        return {
            'appversion': this.usermenuObject
        }
        // return this.appversion;
    }
    setusermenuObject(usermenuObject) {
        this.usermenuObject = usermenuObject;

    }
    setContactData(contactData) {
        this.contactData = contactData;

    }
    setSlctdItem(item) {
        this.item = item;

    }
    getSlctdItem() {
        return {
            'item': this.item
        }

    }
    getContactData() {
        return {
            'contactData': this.contactData
        }
    }
    setQuestionaireData(questionaireData) {
        this.questionaireData = questionaireData;

    }
    getQuestionaireData() {
        return {
            'questionaireData': this.questionaireData
        }

    }
}