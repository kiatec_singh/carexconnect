
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageService {
    user: Promise<string>;
    checklistdata: Promise<string>;
    themeid: Promise<string>;
    selectedlanguage: Promise<string>;

    constructor(public storage: Storage) {
        console.log('storage  Initialisation Started...');
        this.storage.ready().then(() => {
            console.log('Storage Driver: ' + this.storage.driver);
        });
        this.user = storage.ready()
            .then(() => storage.get('user'));
        this.checklistdata = storage.ready()
            .then(() => storage.get('checklistdata'));
        this.themeid = storage.ready()
            .then(() => storage.get('themeid'));
        this.selectedlanguage = storage.ready()
            .then(() => storage.get('selectedlanguage'));

    }
    set(name, value): any {
        this.storage.set(name, value).then((val) => {
            return val;
        });
    }

    get(name): Promise<boolean> {
        return this.storage.ready().then(() => this.storage.get(name));
    }
    remove(name): Promise<boolean> {
        return this.storage.ready().then(() => this.storage.remove(name));
    }
    logout(){
        this.remove('checklistdata');
        // this.remove('cprsave');
        // this.remove('terms');
        this.remove('user');
        // this.remove('userpiddata');
        this.remove('version');
        this.remove('welcome');
    }
    clear() {
        this.storage.clear();

    }

}