import { Injectable } from '@angular/core';
import { Nav, App } from 'ionic-angular';
import { StorageService } from '../storageservice/storageservice';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { stringify } from '@angular/compiler/src/util';


@Injectable()
export class AuthService {
    // URL to web api
    private navCtrl: Nav;
    private nav: any;
    private environmentUrl;
    private homedata;
    private usernemiddata;

    public userchecklistdata = new BehaviorSubject<any>([]);
    public user = new BehaviorSubject<any>([]); //new Subject<string>(); 
    public subtitle = new BehaviorSubject<any>([]);
    public appconfig = new BehaviorSubject<any>([]);
    public sticktoheader = new BehaviorSubject<any>([]);
    public activequesSlide = new BehaviorSubject<any>([]);
    public isscrolled = new BehaviorSubject<any>([]);
    public appinfo = new BehaviorSubject<any>([]);
    public detailedContent = new BehaviorSubject<any>([]);
    public headerpagetopicTitle = new BehaviorSubject<any>([]);
    public autorisation = new BehaviorSubject<any>([]);  
    public webtabsContent = new BehaviorSubject<any>([]);  
    public websmartsearchContent = new BehaviorSubject<any>([]);  
    public websearchdtlContent = new BehaviorSubject<any>([]);  
    public webstressContent = new BehaviorSubject<any>([]);  
    public webtabstoDisplay = new BehaviorSubject<any>([]);  
    public webcontactContent = new BehaviorSubject<any>([]);  
    public navbarlist = new BehaviorSubject<any>([]);
    public pagemenuButtons = new BehaviorSubject<any>([]);
    public showback  = new BehaviorSubject<any>([]);
    public agreed  = new BehaviorSubject<any>([]);
    public iframes  = new BehaviorSubject<any>([]);
    public spinner  = new BehaviorSubject<any>([]);
    public islist  = new BehaviorSubject<any>([]);
    public notifycount  = new BehaviorSubject<any>([]);
    public webquestionarieContent  = new BehaviorSubject<any>([]);
    public webhistroryquestionarieContent  = new BehaviorSubject<any>([]);
    
    
    constructor(private app: App, private storageservice: StorageService) {
        //  this.navCtrl = app.getActiveNavs();
    }


    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        console.log("in auth servicce");
    }

    setEnvironment(environmentUrl) {
        this.environmentUrl = environmentUrl;
    }
    getEnvironment() {
        return this.environmentUrl;

    }
    setUserinfo(value:any) {
        this.user.next(value);
    }
    getUserInfo() {
        return this.user;

    }
    setHomedata(homedata) {
        this.homedata = homedata;
    }
    getHomedata() {
        return this.homedata;

    }
    getUsernemiddata(){
        return this.usernemiddata;
    }
    setUsernemiddata(usernemiddata){
         this.usernemiddata = usernemiddata;
    }
    getuserchecklistData(){
        return this.userchecklistdata;
    }
    setuserchecklistData(value:any){
        this.userchecklistdata.next(value); 
        // this.userchecklistdata = userchecklistdata;
    }
    setsubtitle(value:any){
        this.subtitle.next(value); 
    }
    getsubtitle(value:any){
        return this.subtitle;
    }
    setappconfig(value:any){
        this.appconfig.next(value);
    }
    setsticktoheader(value:any){
        this.sticktoheader.next(value);
    }
    setactivequesSlide(value:any){
        this.activequesSlide.next(value);
    }
    setisscrolled(value:any){
        this.isscrolled.next(value);
    }
    setdetailContent(value:any){
        this.detailedContent.next(value);
    }
    setheaderpageotopicTitle(value:any){
        this.headerpagetopicTitle.next(value);
    }
    setauthorisation(value:any){
        this.autorisation.next(value);
    }
    setwebtabsContent(value:any){
        this.webtabsContent.next(value);
    }
    setwebsmartsearchContent(value:any){
        this.websmartsearchContent.next(value);
    }
    setwebsearchdtlContent(value:any){
        this.websearchdtlContent.next(value);
    }
    setwebstressContent(value:any){
        this.webstressContent.next(value);
    }
    setwebcontactContent(value:any){
        this.webcontactContent.next(value);
    }
    setwebtabstoDisplay(value:any){
        this.webtabstoDisplay.next(value);
    }
    setIframes(value:any){
        this.iframes.next(value);
    }
    setnavbarlist(value:any){
        this.navbarlist.next(value);
    }
    setpagemenuButtons(value:any){
        this.pagemenuButtons.next(value);
    }
    setAppinfo(value:any){
        this.appinfo.next(value);
    }
    setShowback(value:any){
        this.showback.next(value);
    }
    setagreed(value:any){
        this.agreed.next(value);
    }
    setSpinner(value:any){
        this.spinner.next(value);
    }
    setislist(value:any){
        this.islist.next(value);
    }
    setnotifycount(value:any){
        this.notifycount.next(value);
    }
    setwebQuestionaireContent(value:any){
        this.webquestionarieContent.next(value);
    }
    setwebhistroryquestionarieContent(value:any){
        this.webhistroryquestionarieContent.next(value);
    }
    
}



