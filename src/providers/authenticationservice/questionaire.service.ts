import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';



@Injectable()
export class QuestionaireService {
    public activeSlidenumber = new BehaviorSubject<any>([]);
    public totalSlideCountarray = new BehaviorSubject<any>([]);
    public completeProgressvalue = new BehaviorSubject<any>([]);
    public totalansweredCountvalue = new BehaviorSubject<any>([]);
    constructor() {
    }


    setactiveSlidenumber(value: any) {
        this.activeSlidenumber.next(value);
    }
    settotalSlideCountarray(value: any) {
        this.totalSlideCountarray.next(value);
    }
    setcompleteProgressvalue(value: any) {
        this.completeProgressvalue.next(value);
    }
    settotalansweredCountvalue(value: any) {
        this.totalansweredCountvalue.next(value);
    }

}



