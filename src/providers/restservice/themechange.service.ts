import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { DomController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Appinfo } from '../utils/appinfo';
import cssVars from 'css-vars-ponyfill'; //only to support IE 11
import { enviSerice } from '../utils/enviService';


interface Theme {
  name: string;
  styles: ThemeStyle[];
}

interface ThemeStyle {
  themeVariable: string;
  value: string;
}


@Injectable()
export class ThemeChangeService {

  private themes: Theme[] = [];
  private currentTheme: number = 0;
  private formdata;
  private headers;
  private options;
  public appversion_number:any;

  constructor(private domCtrl: DomController, private appInfo: Appinfo, @Inject(DOCUMENT) private document, private enviservice: enviSerice, private http: HttpClient) {
    this.themes = [
      {
        name: 'appskin',
        styles: [
          { themeVariable: '--primary', value: '#a08755' },//0
          { themeVariable: '--gradient', value: '#0000ff' },//1
          { themeVariable: '--primarytext_color', value: '#ffffff' },//2
          { themeVariable: '--buttonbackground', value: 'rgb(232,237,240)' },//3
          { themeVariable: '--buttonspanels', value: 'rgb(232,237,240)' },//4
          { themeVariable: '--buttonspanelstext_color', value: 'rgb(232,237,240)' },//5
          { themeVariable: '--pagecontent', value: '#ffffff' },//6
          { themeVariable: '--menuactive', value: '#BFCBD3' },//7
          { themeVariable: '--menuinactive', value: '#BFCBD3' },//8
          { themeVariable: '--links', value: '#0000ff' },//9
          { themeVariable: '--fontsize', value: '1.2em' },//10
          { themeVariable: '--fontweight', value: '300' },//11
          { themeVariable: '--buttonstextcolor', value: 'rgb(232,237,240)' },//12
          { themeVariable: '--backbuttonspanels', value: '#fff' },//13
          { themeVariable: '--menubackground', value: 'rgb(232,237,240)' },//14
          { themeVariable: '--notification-read', value: '#fff' },//15
          { themeVariable: '--notification-unread', value: '#e6f7ff' },//16
          { themeVariable: '--submenu_background', value: '#e6f7ff' },//17  // new colors
          { themeVariable: '--submenu_buttonbackground', value: '#e6f7ff' },//18
          { themeVariable: '--submenu_buttontext_color', value: '#e6f7ff' },//19
          { themeVariable: '--submenu_text_color', value: '#e6f7ff' },//20
          { themeVariable: '--spinner_color', value: '#e6f7ff' },//21
          { themeVariable: '--menuactive_textcolor', value: '#e6f7ff' },//21
          { themeVariable: '--menuinactive_textcolor', value: '#e6f7ff' },//21
          { themeVariable: '--pagecontent_text_color', value: '#000' },//22
        ]
      }
    ]
  }
  setTheme(): void {
    this.domCtrl.write(() => {
      this.themes[0].styles.forEach(style => {
        document.documentElement.style.setProperty(style.themeVariable, style.value);
      });
    });
  }
  changetoDynamicTheme(allcolorsvariables:any) {
      let colorsvariables = allcolorsvariables.configuration;
      this.themes[0].styles[0].value = colorsvariables.primary;
      this.themes[0].styles[1].value = colorsvariables.gradient;
      this.themes[0].styles[2].value = colorsvariables.primarytext_color;
      this.themes[0].styles[3].value = colorsvariables.buttonbackground;
      this.themes[0].styles[4].value = colorsvariables.buttonspanels;
      this.themes[0].styles[5].value = colorsvariables.buttonspanelstext_color;
      this.themes[0].styles[6].value = colorsvariables.pagecontent;
      this.themes[0].styles[7].value = colorsvariables.menuactive;
      this.themes[0].styles[8].value = colorsvariables.menuinactive;
      this.themes[0].styles[9].value =  colorsvariables.links;
      this.themes[0].styles[10].value = colorsvariables.fontsize;
      this.themes[0].styles[11].value = colorsvariables.fontweight;
      this.themes[0].styles[12].value = colorsvariables.homebutton_text_color; //primarytext_color
      this.themes[0].styles[13].value = colorsvariables.backbuttonspanels;
      this.themes[0].styles[14].value = colorsvariables.menubackground;
      this.themes[0].styles[15].value = colorsvariables.notification_read;
      this.themes[0].styles[16].value = colorsvariables.notification_unread;
      this.themes[0].styles[17].value = colorsvariables.submenu_background;  //new colors
      this.themes[0].styles[18].value = colorsvariables.submenu_buttonbackground;
      this.themes[0].styles[19].value = colorsvariables.submenu_buttontext_color;
      this.themes[0].styles[20].value = colorsvariables.submenu_text_color;
      this.themes[0].styles[21].value = colorsvariables.spinner_color;
      this.themes[0].styles[22].value = colorsvariables.menuactive_textcolor;
      this.themes[0].styles[23].value = colorsvariables.menuinactive_textcolor;
      this.themes[0].styles[24].value = colorsvariables.pagecontent_text_color;
      this.setTheme();
}
}