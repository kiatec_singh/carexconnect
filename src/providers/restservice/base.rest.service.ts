import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { App } from 'ionic-angular';
// import { NotificationsPage } from '../../pages/notifications/notifications.page'
import { LoginPage } from '../../pages/login/login.page';
import { StorageService } from '../storageservice/storageservice';
import { AuthService } from '../authenticationservice/auth.service';
import { environment } from '../../environments/environment';
import { Appinfo } from '../utils/appinfo';
import { ChangelanguageService } from '../utils/changelanguageservice';


@Injectable()
export class BaseRestService {
    private nav: any;
    private headers;
    private options;
    private baseUrl = '/';
    private udvenvi = " https://udv-tryg.carex.dk";
    private formdata;
    private appversion_number;
    private appconfig;
    private userlanguage;
    constructor(private app: App, private storageservice: StorageService,
        private lang: ChangelanguageService,
        private appInfo: Appinfo, private auth: AuthService, private http: HttpClient) {
        this.appversion_number = environment.version;
        this.appInfo.setAppversion(environment.version);
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                if (appconfig && appconfig.environment) {
                    this.appconfig = appconfig;
                }
            }
        );
        this.lang.userlanguage.subscribe(
            (userlanguage: any) => {
                this.userlanguage = userlanguage;
            }
        );
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
            'Accept': 'application/json',
            "Access-Control-Allow-Headers": "X-Requested-With"
        });

        this.options = new RequestOptions({ headers: this.headers });
    }
    navigateTo(pagename, options) {
        let navoptions = options ? options : {};
        this.app.getRootNav().push(LoginPage);
    }

    getTermsandconditionsData(uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'read_samtykke_skabelonfile');
        this.formdata.append('bruger_uuid', uuid);
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('applikation_uuid', appinfo.appId);
        this.formdata.append('skabelon_procestrin', 'generalaccept');
        this.formdata.append('skabelon_organisation', appinfo.orgId);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }

    saveTermsandconditions(activestatus, uuid) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'samtykke_create');
        this.formdata.append('applikation_uuid', appinfo.appId);
        this.formdata.append('procestrin', 'GeneralAccept');
        this.formdata.append('bruger_uuid', uuid);
        this.formdata.append('organisation', appinfo.orgId);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('response', activestatus);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    saveCPR(pid, cpr, activestatus, uuid) {
        this.formdata = new FormData();
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'samtykke_create_cpr');
        //this.formdata.append('listedefinition_uuid', 'e4388fbc-c139-4542-894e-b0d14f34db30');
        this.formdata.append('bruger_uuid', uuid);
        this.formdata.append('applikation_uuid', appinfo.appId); // tryg app
        this.formdata.append('procestrin', 'cpr');
        this.formdata.append('nemidpid', pid);
        this.formdata.append('cpr', cpr);
        this.formdata.append('organisation', appinfo.orgId);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('response', activestatus);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        // return this.http.post('https://api.carex.dk/api/endpoints/api_services.php', this.formdata, this.options).toPromise();
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    checkactiveList(uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'samtykke_list_aktive');
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('bruger_uuid', uuid);
        this.formdata.append('applikation_uuid', appinfo.appId);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getCustomerData(uuid) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'list_menuitems');
        this.formdata.append('bruger_uuid', uuid);
        this.formdata.append('applikation_uuid', appinfo.appId);
        this.formdata.append('organisation', appinfo.orgId);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getdetails(actionname, skemaid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', actionname);
        this.formdata.append('skema_uuid', skemaid);
        this.formdata.append('autorisation', authid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    logout(url) {
        this.storageservice.clear();
        return this.http.get(url + '/logout.php', this.options).toPromise();
    }
    login(username, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'login');
        this.formdata.append('id', username);
        this.formdata.append('password', password);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    OTP(username) {
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata = new FormData();
        this.formdata.append('action', 'OTP');
        this.formdata.append('username', username);
        this.formdata.append('applikation_uuid', appinfo.appId);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    verifyOTP(username,otp) {
        this.formdata = new FormData();
        this.formdata.append('action', 'verifyOTP');
        this.formdata.append('username', username);
        this.formdata.append('otp', otp);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    sendOtp(username, email) {
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata = new FormData();
        this.formdata.append('action', 'getOTP');
        this.formdata.append('username', username);
        this.formdata.append('email', email);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('applikation_uuid', appinfo.appId);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    getUser(username) {
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata = new FormData();
        this.formdata.append('action', 'getUser');
        this.formdata.append('username', username);
        this.formdata.append('applikation_uuid', appinfo.appId);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    verifyOtp(id, otp) {
        this.formdata = new FormData();
        this.formdata.append('action', 'validateOTP');
        this.formdata.append('id', id);
        this.formdata.append('otp', otp);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    validateCPR(pid, cpr) {
        this.formdata = new FormData();
        this.formdata.append('action', 'validatecpr');
        this.formdata.append('pid', pid);
        this.formdata.append('cprnumber', cpr);
        return this.http.post(this.appconfig.environment.nemidvalidateUrl, this.formdata, this.options).toPromise();
    }
    changePassword(id, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'changePassword');
        this.formdata.append('id', id);
        this.formdata.append('password', password);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    loginUdv(id, password) {
        this.formdata = new FormData();
        this.formdata.append('action', 'login');
        this.formdata.append('id', id);
        this.formdata.append('password', password);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }

    getappConfig() {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'get_configuration');
        this.formdata.append('version', this.appversion_number);
        this.formdata.append('itsystem_uuid', appinfo.appId);
        this.formdata.append('organisation_uuid', appinfo.orgId);
        return this.http.post(environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    getbrandConfig() {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'getBrand');
        this.formdata.append('version', this.appversion_number);
        this.formdata.append('itsystem_uuid', appinfo.appId);
        this.formdata.append('organisation_uuid', appinfo.orgId);
        this.formdata.append('objekt_uuid', appinfo.brandId);
        return this.http.post(environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getbrandConfigbyId(brandId) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'getBrand');
        this.formdata.append('version', this.appversion_number);
        this.formdata.append('itsystem_uuid', appinfo.appId);
        this.formdata.append('organisation_uuid', appinfo.orgId);
        this.formdata.append('objekt_uuid', brandId);
        return this.http.post(environment.apiUrl, this.formdata, this.options).toPromise();
    }
    // getappConfigbyId(orgid) {
    //     this.formdata = new FormData();
    //     let appinfo = this.appInfo.getApplicationinfo();
    //     this.formdata.append('action', 'get_configuration');
    //     this.formdata.append('version', this.appversion_number);
    //     this.formdata.append('itsystem_uuid', appinfo.appId);
    //     this.formdata.append('organisation_uuid', orgid);
    //     return this.http.post(environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    // }
    logAnalytics(name, platform, authid, userid, eventname) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        let userrelation = this.appInfo.getusermenuObject();
        let userrel;
        let rel_appid;
        if (userrelation.appversion && userrelation.appversion.brand) {
            userrel = userrelation.appversion.brand;
            this.formdata.append('relation_organizationid', userrelation.appversion.relationer[0].organisation_uuid);	
            this.formdata.append('relation_organizationname', userrelation.appversion.relationer[0].org);	
            this.formdata.append('cvr', userrelation.appversion.relationer[0].cvr);	
            rel_appid = userrelation.appversion.brand;
        } else {
            rel_appid = appinfo.orgId;
        }
        this.formdata.append('action', 'Analytics');
        this.formdata.append('name', name);
        this.formdata.append('platform', platform);
        this.formdata.append('autorisation', authid);
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('eventname', eventname);
        this.formdata.append('itsystem_uuid', appinfo.appId);
        this.formdata.append('applikation_uuid', rel_appid);
        this.formdata.append('version', this.appversion_number);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    logyoutubeAnalytics(name, platform, authid, userid, eventname, url) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        let userrelation = this.appInfo.getusermenuObject();
        let userrel;
        let rel_appid;
        if (userrelation.appversion && userrelation.appversion.brand) {
            userrel = userrelation.appversion.brand;
            this.formdata.append('relation_organizationid', userrelation.appversion.relationer[0].organisation_uuid);	
            this.formdata.append('relation_organizationname', userrelation.appversion.relationer[0].org);	
            this.formdata.append('cvr', userrelation.appversion.relationer[0].cvr);	
            rel_appid = userrelation.appversion.brand;
        } else {
            rel_appid = appinfo.orgId;
        }
        this.formdata.append('action', 'Analytics');
        this.formdata.append('name', name);
        this.formdata.append('platform', platform);
        this.formdata.append('autorisation', authid);
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('eventname', eventname);
        this.formdata.append('itsystem_uuid', appinfo.appId);
        this.formdata.append('applikation_uuid', rel_appid);
        this.formdata.append('version', this.appversion_number);
        this.formdata.append('url', url);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    logCatalogEvent(name, platform, authid, userid, eventname, item) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        let userrelation = this.appInfo.getusermenuObject();
        let userrel;
        let rel_appid;
        if (userrelation.appversion && userrelation.appversion.brand) {
            userrel = userrelation.appversion.brand;
            this.formdata.append('relation_organizationid', userrelation.appversion.relationer[0].organisation_uuid);	
            this.formdata.append('relation_organizationname', userrelation.appversion.relationer[0].org);	
            this.formdata.append('cvr', userrelation.appversion.relationer[0].cvr);	
            rel_appid = userrelation.appversion.brand;
        } else {
            rel_appid = appinfo.orgId;
        }
        this.formdata.append('action', 'Analytics');
        this.formdata.append('name', name);
        this.formdata.append('platform', platform);
        this.formdata.append('autorisation', authid);
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('eventname', eventname);
        this.formdata.append('itsystem_uuid', appinfo.appId);
        this.formdata.append('applikation_uuid', rel_appid);
        this.formdata.append('version', this.appversion_number);
        this.formdata.append('Catalog', item);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    logpageviewAnalytics(name, platform, authid, userid, eventname) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        let userrelation = this.appInfo.getusermenuObject();
        let userrel;
        let rel_appid;
        if (userrelation.appversion && userrelation.appversion.brand) {
            userrel = userrelation.appversion.brand;
            this.formdata.append('relation_organizationid', userrelation.appversion.relationer[0].organisation_uuid);	
            this.formdata.append('relation_organizationname', userrelation.appversion.relationer[0].org);	
            this.formdata.append('cvr', userrelation.appversion.relationer[0].cvr);	
            rel_appid = userrelation.appversion.brand;
        } else {
            rel_appid = appinfo.orgId;
        }
        this.formdata.append('action', 'Analytics');
        this.formdata.append('name', name);
        this.formdata.append('platform', platform);
        this.formdata.append('autorisation', authid);
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('eventname', eventname);
        this.formdata.append('itsystem_uuid', appinfo.appId);
        this.formdata.append('applikation_uuid', rel_appid);
        this.formdata.append('version', this.appversion_number);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    //ERIK PROCESS
    fcmtokenRegister(deviceuuid, devicesrno, userid, fcmtoken, authid, status, platform) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'deviceinfo_register');
        let deviceinfo = {
            'version': environment.version,
            'applikationUuid': appinfo.appId,
            'brugerUuid': userid,
            'deviceSerialId': devicesrno,
            'appId': fcmtoken,
            'deviceId': deviceuuid,
            'platform': platform,
            'applikationVersionUuid': environment.application_version_uuid,
            'gyldighed': status
        }
        this.formdata.append('autorisation', authid);
        this.formdata.append('deviceinfo', JSON.stringify(deviceinfo));
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    //SINGh
    fcmdeviceRegister(platform, userid, brugernavn, fcmtoken, authid, deviceinfo) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'deviceinfo');
        this.formdata.append('autorisation', authid);
        this.formdata.append('version', environment.version);
        this.formdata.append('applikationuuid', appinfo.appId);
        this.formdata.append('organisation', appinfo.orgId);
        this.formdata.append('brugeruuid', userid);
        this.formdata.append('brugernavn', brugernavn);
        this.formdata.append('fcmtoken', fcmtoken);
        this.formdata.append('platform', platform);
        this.formdata.append('deviceinfo', deviceinfo);
        this.formdata.append('gyldighed', 'aktiv');
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    fcmdeviceunRegister(userid, authid, status) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'turnoffnotification');
        this.formdata.append('autorisation', authid);
        this.formdata.append('applikationuuid', appinfo.appId);
        this.formdata.append('brugeruuid', userid);
        this.formdata.append('gyldighed', status);
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    turnoffnotification(authid, notificationvalue) {
        this.formdata = new FormData();
        this.formdata.append('action', 'updateProfileLanguage');
        this.formdata.append('autorisation', authid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('notification_push', notificationvalue);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    fcmwebtokenRegister(deviceuuid, devicesrno, userid, fcmtoken, authid) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'deviceinfo_register');
        this.formdata.append('autorisation', authid);
        this.formdata.append('version', environment.version);
        this.formdata.append('applikation_uuid', appinfo.appId);
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('device_serial_id', devicesrno);
        this.formdata.append('app_token_id', fcmtoken);
        this.formdata.append('virkning_fra', new Date);
        this.formdata.append('virkning_til', new Date);
        this.formdata.append('device_token_id', deviceuuid);
        this.formdata.append('gyldighed', 'aktiv');
        return this.http.post(this.appconfig.environment.idpservicesUrl_v2, this.formdata, this.options).toPromise();
    }
    getnotifications(authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'NotifikationBrugerList');
        this.formdata.append('autorisation', authid);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    updateProfile(phonenumber, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'updateProfileLanguage');
        this.formdata.append('autorisation', authid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('phonenumber', phonenumber);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    updateallProfile(phonenumber, email, lang, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'updateProfileLanguage');
        this.formdata.append('autorisation', authid);
        this.formdata.append('language', lang);
        this.formdata.append('phonenumber', phonenumber);
        this.formdata.append('email', email);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    readnotification(notificationid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'notifikationStateUpdate');
        this.formdata.append('autorisation', authid);
        this.formdata.append('notifikation_uuid', notificationid);
        this.formdata.append('notifikation_tilstand', 'seen');
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getnavigationPath(authid, buttonname) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getButton');
        this.formdata.append('autorisation', authid);
        this.formdata.append('knap_brugervendtnoegle', buttonname);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    checkauthforNav(autorisation, action, skema_uuid) {
        this.formdata = new FormData();
        this.formdata.append('action', action);
        this.formdata.append('autorisation', autorisation);
        this.formdata.append('skema_uuid', skema_uuid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    contactForm(action, uuid, updateObject, authid): any {
        this.formdata = new FormData();
        this.formdata.append('action', action);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('update_objekt', updateObject);
        this.formdata.append('autorisation', authid);
        this.formdata.append('procestrin', "videregivelse_af_oplysninger");
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    gethistroyDetails(uuid, listId, authid, action) {
        this.formdata = new FormData();
        this.formdata.append('action', action);
        this.formdata.append('skema_uuid', uuid);
        this.formdata.append('objekt_uuid', listId);
        this.formdata.append('autorisation', authid);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    deletehistroyItem(skemauuid, objectuuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'CALL_delete_objekt');
        this.formdata.append('skema_uuid', skemauuid);
        this.formdata.append('autorisation', authid);
        this.formdata.append('objekt_uuid', objectuuid);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    deletesurveyresultItem(survey_uuid, aktivitet_uuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'deleteSurveyResult');
        this.formdata.append('survey_uuid', survey_uuid);
        this.formdata.append('aktivitet_uuid', aktivitet_uuid);
        this.formdata.append('autorisation', authid);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getsurveyResults(actionname, survey_uuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', actionname);
        this.formdata.append('survey_uuid', survey_uuid);
        this.formdata.append('autorisation', authid);   //authid //'ad187e4e-de89-4800-b8b4-96a852f62263'  '370fa886-04dc-4e7d-b707-5534cd07e139'
        //this.formdata.append('applikation_uuid', 'ae9055e5-210e-4a0b-9d39-b334c12cf744'); //
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }

    getquestionairdetails(actionname, actionparameterkey, actionparameterkeyId, aktivitet_uuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', actionname);
        this.formdata.append(actionparameterkey, actionparameterkeyId);
        this.formdata.append('autorisation', authid);
        if (aktivitet_uuid) {
            this.formdata.append('aktivitet_uuid', aktivitet_uuid);
        }
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getquestionairehistroyDetails(survey_uuid, aktivitet_uuid, autorisation) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getAnswersDetails');
        if (aktivitet_uuid) {
            this.formdata.append('aktivitet_uuid', aktivitet_uuid);
        }
        this.formdata.append('autorisation', autorisation);
        this.formdata.append('survey_uuid', survey_uuid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    updateAnswers(survey_uuid, aktivitet_uuid, surveyEmneUuid, surveySvarUuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'SaveAnswer');
        this.formdata.append('autorisation', authid);
        if (aktivitet_uuid) {
            this.formdata.append('aktivitet_uuid', aktivitet_uuid);
        }
        this.formdata.append('survey_uuid', survey_uuid);
        this.formdata.append('surveyEmneUuid', surveyEmneUuid);
        this.formdata.append('surveySvarUuid', surveySvarUuid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getanswersPercentage(survey_uuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'GetAnswers');
        this.formdata.append('autorisation', authid);
        this.formdata.append('survey_uuid', survey_uuid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    submitQuestionaire(survey_uuid, aktivitet_uuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getAnswersAndCalculateScore');
        this.formdata.append('autorisation', authid);
        this.formdata.append('survey_uuid', survey_uuid);
        if (aktivitet_uuid) {
            this.formdata.append('aktivitet_uuid', aktivitet_uuid);
        }
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getcontactTermsData(authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'read_samtykke_skabelonfile');
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('applikation_uuid', appinfo.appId);
        this.formdata.append('skabelon_procestrin', 'videregivelse_af_oplysninger');
        this.formdata.append('skabelon_organisation', appinfo.orgId);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('autorisation', authid);
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    getquestionaireResult(surveyuuid, activityuuid, authid) {
        this.formdata = new FormData();
        this.formdata.append('action', 'getSurveyResults');
        this.formdata.append('aktivitet_uuid', activityuuid);
        this.formdata.append('survey_uuid', surveyuuid);
        this.formdata.append('autorisation', authid);
        this.formdata.append('language', this.userlanguage);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('latest_only',true);
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
    saveLanguage(userid, lang) {
        this.formdata = new FormData();
        let appinfo = this.appInfo.getApplicationinfo();
        this.formdata.append('action', 'updateProfileLanguage');
        this.formdata.append('bruger_uuid', userid);
        this.formdata.append('applikation_uuid', appinfo.appId);
        this.formdata.append('language', lang);
        this.formdata.append('application_platform', 'app');
        this.formdata.append('application_version', this.appversion_number);
        return this.http.post(this.appconfig.environment.apiUrl, this.formdata, this.options).toPromise();
    }
}