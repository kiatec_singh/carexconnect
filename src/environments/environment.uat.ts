export const environment = {
  production: true,
  baseURL :"https://uat-api.carex.dk/api/endpoints/api_services.php",
  contentUrl: "https://uat-workplace.carex.dk/",
  apiUrl: "https://uat-api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://uat-idp.carex.dk/endpoints/login_services.php",
  idpservicesUrl:"https://uat-idp.carex.dk/endpoints/idp_service.php",
  idpservicesUrl_v2: "https://uat-idp.carex.dk/v2/endpoints/idp_service.php",
  nemidloginUrl: "https://uat-nemid.carex.dk/nemidlogon.php",
  nemidvalidateUrl: "https://uat-nemid.carex.dk/validatecpr.php",
  webUrl:'https://uat-workplace.carex.dk',
  picturesUrl: "https://uat-admin.carex.dk/pictures/",
  bilagUrl: "https://uat-admin.carex.dk/temp/",
  version: '1.3.66',
  fcmwebSenderId:'116581142159',
  application_version_uuid:'cef0269c-d625-42d6-ae0f-f0cb1c0eb97c',
  envi: 'uat'
};