export const environment = {
  production: true,
  baseURL :"https://api.carex.dk/api/endpoints/api_services.php",
  contentUrl: "https://workplace.carex.dk/",
  apiUrl: "https://api.carex.dk/api/endpoints/api_services.php",
  loginservicesUrl: "https://idp.carex.dk/endpoints/login_services.php",
  idpservicesUrl:"https://idp.carex.dk/endpoints/idp_service.php",
  idpservicesUrl_v2: "https://idp.carex.dk/v2/endpoints/idp_service.php",
  nemidloginUrl: "https://nemid.carex.dk/nemidlogon.php",
  nemidvalidateUrl: "https://nemid.carex.dk/validatecpr.php",
  webUrl:'https://workplace.carex.dk',
  picturesUrl: "https://admin.carex.dk/pictures/",
  bilagUrl: "https://admin.carex.dk/temp/",
  version: '1.3.66',
  fcmwebSenderId:'116581142159',
  application_version_uuid:'cef0269c-d625-42d6-ae0f-f0cb1c0eb97c',
  envi: 'prod'
};