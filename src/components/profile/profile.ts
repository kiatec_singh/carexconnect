import { Component, ChangeDetectorRef } from '@angular/core';
import { ModalController, ViewController } from 'ionic-angular';
import { ProfilePage } from '../../pages/profile/profile.page';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { WelcomePage } from '../../pages/welcome/welcome.page'
import { NavController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { StorageService } from '../../providers/storageservice/storageservice'
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { environment } from '../../environments/environment';
import { changeenviComponent } from '../changeenvi/changeenvi';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import { Appinfo } from '../../providers/utils/appinfo';
import { HelperSerice } from '../../providers/utils/helperService';
import { ChangelanguagePage } from '../../pages/changelanguage/changelanguage.page';
import { changelanguageComponent } from '../changelanguage/changelanguage';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import { ToastrService } from 'ngx-toastr';
import { StatusBar } from '@ionic-native/status-bar';
import { ThemeChangeService } from '../../providers/restservice/themechange.service';

@Component({
    selector: 'profile-viewer',
    templateUrl: 'profile.html'
})
export class ProfileComponent {
    rootPage: any = ProfilePage;
    private loggedout;
    private enviUrl;
    private user: any;
    private profileContent: any;
    private loading: boolean;
    private username;
    private name;
    private emailmodel;
    private title;
    private appconfig;
    private bg_color;
    private text_color;
    private profil_pic;
    public appVersion_no: any;
    private phonenumber: any;
    private editphonenumbermodel = false;
    private editprofileinfo = false;
    private isLoading = false;
    private autorisation;
    private savedphonenumber = '';
    private userobj: any;
    private userlanguage;
    private emailvalid=false;
    private pnumbervalid=false;
    private submited=false;
    private emailvalidRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    private phonenumbervalidRegx = /^\d+$/;
    private savedemail='';
 
    constructor(private baserestService: BaseRestService,private themechangeService:ThemeChangeService,
        public statusbar: StatusBar,private toastCtrl: Toast,private toastr: ToastrService,  private appinfo: Appinfo, private logprovider: EventLoggerProvider, private helperService: HelperSerice, private lang:ChangelanguageService,
        private viewCtrl: ViewController,
        private navCtrl: NavController,
        private appinfoservice: Appinfo,
        private modalCtrl: ModalController,
        private ref: ChangeDetectorRef,
        private auth: AuthService, private storageService: StorageService) {
        this.auth.user.subscribe(
            (user) => {
                    this.user = user;
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    this.appconfig = appconfig;
            }
        );
        this.lang.userlanguage.subscribe(
            (userlanguage) => {
                    if (userlanguage) {
                        this.userlanguage = userlanguage;
                        this.profileContent = this.appconfig.configuration.texts[this.userlanguage].profile;
                    }
            }
        );
        this.auth.autorisation.subscribe(
            (autorisation: any) => {
                    this.autorisation = autorisation;
            }
        );
    }

    ngOnInit() {
        console.log("in progile compo")
        this.userobj = this.appinfoservice.getusermenuObject();
        this.enviUrl = this.appconfig.environment.picturesUrl;
        if (this.user.brugernavn) {
            this.username = this.user.brugernavn;
            this.emailmodel = this.user.email;
            this.name = this.user.username;
        }
        else {
            this.username = this.user[2];
            this.emailmodel = this.user[1];
            this.name = this.user[0];
        }
        this.title = this.profileContent.heading;
        this.profil_pic = this.profileContent.picturename;
        this.phonenumber = this.userobj.appversion && this.userobj.appversion.profil && this.userobj.appversion.profil.phonenumber ? this.userobj.appversion.profil.phonenumber : this.savedphonenumber;
        this.emailmodel = this.userobj.appversion && this.userobj.appversion.profil && this.userobj.appversion.profil.email ? this.userobj.appversion.profil.email : this.savedemail;
        this.loading = false
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logpageView('profilepage');
        }
    }


    logout() {
        this.isLoading = true;
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logEvent('logout');
        }
        this.initializeApp();
    }
    initializeApp() {
        this.storageService.logout();
        let newnavbarlist = [];
        this.auth.setnavbarlist(newnavbarlist);
        this.helperService.initializeApp();
        this.initializetheme();
 
    }
    initializetheme(){
        let org = this.appinfo.getApplicationinfo();
        if(org.orgId !== this.appconfig.configuration.appversion.organisationid){
            this.baserestService.getappConfig().then(
                (appconfig: any) => {
                    this.statusbar.backgroundColorByHexString(appconfig.configuration.primary);
                    if (appconfig.configuration.statusbartext == 'light') {
                      this.statusbar.styleLightContent();
                    }
                    if (appconfig.configuration.statusbartext == 'dark') {
                      this.statusbar.styleDefault();
                    }
                    this.auth.setappconfig(appconfig)
                    this.themechangeService.changetoDynamicTheme(appconfig);
                    this.appconfig = appconfig;
                    if (!this.ref['destroyed']) {
                      this.ref.detectChanges();
                    }
                    this.navigate();
                  });
        }
        else{
          this.navigate();  
        }
    }
    navigate(){
        this.storageService.get('selectedlanguage').then(
            (selectedlanguage:any) => {
              if(selectedlanguage){
                this.lang.setselectedlanguge(selectedlanguage); 
                this.lang.setuserlang(selectedlanguage);
                this.isLoading =false;
                this.navCtrl.setRoot(WelcomePage);
              }
              else{
                this.isLoading =false;
                this.navCtrl.setRoot(ChangelanguagePage);
              }
            });
    }
    editprofile() {
        this.phonenumber = this.userobj.appversion && this.userobj.appversion.profil && this.userobj.appversion.profil.phonenumber ? this.userobj.appversion.profil.phonenumber : this.savedphonenumber;
        this.emailmodel = this.userobj.appversion && this.userobj.appversion.profil && this.userobj.appversion.profil.email ? this.userobj.appversion.profil.email : this.savedemail;
        this.editprofileinfo = this.editprofileinfo? false:true;
        this.emailvalid  = this.emailvalidRegx.test(String(this.emailmodel).toLowerCase());
        this.ref.detectChanges();
    }

    save() {
        this.submited=true;
        this.emailvalid  = this.emailvalidRegx.test(String(this.emailmodel).toLowerCase());
        if(this.phonenumber.length==0){
            this.pnumbervalid = true;  
        }
        if(this.phonenumber){
         this.pnumbervalid = this.phonenumbervalidRegx.test(this.phonenumber);  
        }

        if(this.pnumbervalid === true && this.emailvalid === true){
         this.isLoading = true;
        this.baserestService.updateallProfile(this.phonenumber,this.emailmodel, this.userlanguage, this.autorisation).then(
            saveprofile => {
                console.log(saveprofile);
                if (this.userobj.appversion && this.userobj.appversion.profil && this.userobj.appversion.profil.phonenumber) {
                    this.userobj.appversion.profil.phonenumber = this.phonenumber;
                    this.userobj.appversion.profil.email = this.emailmodel;
                }
                this.savedphonenumber = this.phonenumber;
                this.savedemail = this.emailmodel;
                this.editphonenumbermodel = false;
                this.editprofileinfo = false;
                this.ref.detectChanges();
                    this.successblock();
                  
            },
            error => { console.error(error); this.errorblock(); }
        )
        }

        console.log(this.pnumbervalid);
        console.log(this.emailvalid);
        this.ref.detectChanges();
    }
    successblock() {
        this.isLoading = false;
        let isapp = this.helperService.isApp();
        if (isapp === true) {
            const toast = this.toastCtrl.show(this.profileContent.submitsuccessmsg, this.profileContent.toastdration, this.profileContent.toastposition).subscribe(
                toast => {
                    console.log(toast);
                });
        }
        else {
            this.toastr.success(this.profileContent.submitsuccessmsg, this.profileContent.submitsuccesstitle, {
                timeOut: this.profileContent.toastdration,
                positionClass: this.profileContent.webtoastposition
            });
        }
    }
errorblock() {
        this.isLoading = false;
        let isApp = this.helperService.isApp();
        if (isApp === true) {
            const toast = this.toastCtrl.show(this.profileContent.submiterrormsg, this.profileContent.toastdration, this.profileContent.toastposition).subscribe(
                toast => {
                    console.log(toast);
                });
        }
        else {
            this.toastr.success(this.profileContent.submiterrormsg, this.profileContent.submiterrortitle, {
                timeOut: this.profileContent.toastdration,
                positionClass: this.profileContent.webtoastposition
            });
        }

    }
    cancel() {
        this.editprofileinfo = false;
        this.phonenumber = this.userobj.appversion && this.userobj.appversion.profil && this.userobj.appversion.profil.phonenumber ? this.userobj.appversion.profil.phonenumber : this.savedphonenumber;
        this.emailmodel = this.userobj.appversion && this.userobj.appversion.profil && this.userobj.appversion.profil.email ? this.userobj.appversion.profil.email : this.savedemail;


        this.emailvalid  = this.emailvalidRegx.test(String(this.emailmodel).toLowerCase());
        if(this.phonenumber.length==0){
            this.pnumbervalid = true;  
        }
        if(this.phonenumber){
         this.pnumbervalid = this.phonenumbervalidRegx.test(this.phonenumber);  
        }


        this.ref.detectChanges();
        console.log("cancel");
    }
    selectLang() {
        const modal = this.modalCtrl.create(changelanguageComponent, { modal: true });
        modal.present();
    }
    closemodal() {
        this.viewCtrl.dismiss();
    }
}


