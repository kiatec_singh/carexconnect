import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { StorageService } from '../../providers/storageservice/storageservice';
import { User } from '../../models/user.model';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { Environment } from '../../models/environment.model';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { UsernamePage } from '../../pages/username/username';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import { IdverifyPage } from '../../pages/idverify/idverify.page';
import { CPRPage } from '../../pages/cpr/cpr.page';
import { HomePage } from '../../pages/home/home.page';
import { TermsconditionPage } from '../../pages/termsconditions/termsconditions.page';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';



@Component({
    selector: 'login-viewer',
    templateUrl: 'login.html'
})
export class LoginComponent {


    private loginForm: FormGroup;
    private source: string = '';
    private userinfo;
    private termsread;
    private user: User;
    private environment;
    private envi: Environment;
    private iframeUrl;
    private loggedUser;
    private isLoading;
    private userInfo;
    private error = false;
    private serviceerror = false;
    private showlogin = false;
    private loginData: any;
    private username;
    private password;
    private errormsg;
    private submit;
    private forgetpasswordlabel;
    private cprsaved;
    private checklistdata;
    private appinfo: any;
    private appconfig: any;
    private selectedlanguage;
    private userlanguage;
    private serviceerrormsg;


    constructor(private fb: FormBuilder, private navCtrl: NavController, private lang:ChangelanguageService, private logprovider:EventLoggerProvider,
        private auth: AuthService,private baserestService: BaseRestService, private storageService: StorageService) {
        this.loginForm = this.fb.group({
            userid: ['', Validators.required],
            password: ['', Validators.required],
        });
        this.auth.appinfo.subscribe(
            (appinfo) => {
                this.appinfo = appinfo;
            }
        )
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    if (appconfig) {
                        this.appconfig = appconfig;
                    }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
            }
        );
        this.lang.userlanguage.subscribe(
            (userlanguage: any) => {
                    this.userlanguage = userlanguage;
            }
        );
    }

    ngOnInit() {
        this.isLoading = true;
        this.getloginData();
        this.storageService.get('user').then(
            loggedUser => {
                this.loggedUser = loggedUser;
                if (loggedUser) {
                    this.user = new User(loggedUser);
                }
            });
    }


    login() {
        this.isLoading = true;
        this.serviceerror = false;
        this.error = false;
        this.baserestService.login(this.loginForm.value.userid, this.loginForm.value.password).then(
            userInfo => {
                this.userInfo = userInfo;
                this.user = new User(userInfo);
                this.loggedIn(userInfo);
            },
            error => {
                console.log(error);
                this.error = true;
                this.isLoading = false;
            }
        );
    }
    getloginData() {
        this.loginData = this.selectedlanguage.login;
        this.setData();

    }
    setData() {
        this.submit = this.loginData.submit;
        this.password = this.loginData.password;
        this.errormsg = this.loginData.errormsg;
        this.forgetpasswordlabel = this.loginData.forgetpassword;
        this.username = this.loginData.username;
        this.isLoading = false;
    }

    loggedIn(userInfo) {
        if (userInfo) {
            this.storageService.set('user', userInfo);
            this.auth.setUserinfo(userInfo);
            this.storageService.set('welcome', true);
            this.isLoading = true;
            this.baserestService.checkactiveList(userInfo.id).then(
                checklistdata => {
                    this.checklistdata = checklistdata;
                    this.storageService.set('checklistdata', checklistdata);
                    this.auth.setuserchecklistData(checklistdata);
                   this.saveuserlanguage(userInfo);
                },
                error => { console.log(error); 
                    this.isLoading = false;
                     this.serviceerror = true;
                     this.serviceerrormsg = error.error.besked;
                    }
            );
        }
        else{
            this.isLoading =false;
        }

    }
    forgetpassword(event) {
        this.navCtrl.push(UsernamePage);
    }
    saveuserlanguage(userInfo){
        if(this.checklistdata.result && this.checklistdata.result.profile.language){
            if(this.checklistdata.result.profile.language === this.userlanguage){
                this.decideflow();
            }
            else{
                this.baserestService.saveLanguage(userInfo.id, this.userlanguage).then(
                    languagechanged => { this.decideflow(); },
                    error => { this.isLoading = false;})
            }
        }
        else{
            this.baserestService.saveLanguage(userInfo.id, this.userlanguage).then(
                languagechanged => { this.decideflow(); },
                error => { this.isLoading = false;})
        }
    }
    decideflow() {
        this.isLoading =false;
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logpageView("Loginpage");
        }
        if (!this.checklistdata.result || this.checklistdata.result.length == 0) {
            this.navCtrl.push(IdverifyPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && !this.checklistdata.result.cpr && !this.checklistdata.result.nemid) {
            this.navCtrl.push(IdverifyPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && this.checklistdata.result.cpr === false && !this.checklistdata.result.nemid) {
            this.navCtrl.push(CPRPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && this.checklistdata.result.cpr === false && this.checklistdata.result.nemid) {
            this.navCtrl.push(CPRPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && this.checklistdata.result.generalaccept === true && this.checklistdata.result.cpr === false && !this.checklistdata.result.nemid) {
            this.navCtrl.push(CPRPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && this.checklistdata.result.generalaccept === true && this.checklistdata.result.cpr === false && this.checklistdata.result.nemid) {
            this.navCtrl.push(CPRPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && this.checklistdata.result.generalaccept === true && this.checklistdata.result.cpr === true && this.checklistdata.result.nemid) {
            this.navCtrl.setRoot(HomePage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && this.checklistdata.result.cpr === true && this.checklistdata.result.nemid) {
            this.navCtrl.push(TermsconditionPage);
            return;
        }
    }
}