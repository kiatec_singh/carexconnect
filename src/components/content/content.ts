import { Component,ChangeDetectorRef, NgZone, Input } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';


@Component({
  selector: 'content-viewer',
  templateUrl: 'content.html'
})
export class ContentComponent {

  public contentchngFlag: boolean;
  public contentdata;
  public dataContent;
  public iframedata = [];
  @Input() htmlcontent: any;
  constructor(private auth: AuthService, private zone: NgZone) {
    this.auth.detailedContent.subscribe(
      (tabsContent) => {
        this.contentchngFlag = false;
        this.zone.run(() => {
          this.dataContent = tabsContent;
         setTimeout(() => {
       this.contentchngFlag = true;
         }, 10);
        });
        this.checkforIframes(tabsContent);
      }
    );
  }
  ngOnInit() {
    // if(this.htmlcontent){
    //   this.dataContent = this.htmlcontent;
    // }
    console.log(this.dataContent);
    this.checkforIframes(this.dataContent);
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.dataContent =[];
  }

  checkforIframes(tabsContent) {
    if (tabsContent && tabsContent.length != 0) {
      let totaliframes = [];
      tabsContent.forEach(element => {
        if (element.indexOf('<iframe') != -1) {
          totaliframes.push(element);
        }
      });
      this.iframedata = totaliframes;
      this.auth.setIframes(this.iframedata);
    }
  }
}
