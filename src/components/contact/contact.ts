import { Component, Input, ViewChild, SelfDecorator, ChangeDetectorRef } from '@angular/core';
import { ViewController, NavParams, ModalController, Platform, NavController, Content } from 'ionic-angular';
import { FormBuilder, FormArray } from '@angular/forms';
import { TermsComponent } from '../terms/terms';
import { Toast } from '@ionic-native/toast';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { Keyboard } from '@ionic-native/keyboard';
import { ToastrService } from 'ngx-toastr';
import { Appinfo } from '../../providers/utils/appinfo';
import { HelperSerice } from '../../providers/utils/helperService';
import { PasswordValidation } from '../../providers/validators/password-validator';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';


@Component({
    selector: 'contact-viewer',
    templateUrl: 'contact.html'
})
export class ContactComponent {

    private pagetitle = '';
    private contactformdata;
    private contactForm;
    private items: any;
    private isLoading = false;
    private submitbutton: any;
    private cancelbutton: any;
    private showterms: any;
    private checkboxbutton: any;
    private appconfig: any;
    private termsModel = false;
    private uuid = '';
    private autorisation;
    private termsdata;
    private submited = false;
    private slctItem: any;
    private contactContent: any;
    private heading;
    private navbarlist;
    private userchecklistdata;
    private isresultpage = true;
    private iscprvalid;
    private userpid;
    private cprinvaliderror = true;
    private cprvalue;
    private cprmismatcherrormessage = true;
    private tempcprmismatcherrormessage;
    private selectedlanguage;
    @ViewChild('errorinput') errorinput: HTMLElement;
    @ViewChild('errordropdown') errordropdown: HTMLElement;
    @ViewChild('contactPage') contactPage: Content;

    constructor(public viewCtrl: ViewController,   private logprovider: EventLoggerProvider, public navParams: NavParams, private navCtrl: NavController, private appinfo: Appinfo, private keyboard: Keyboard, private helperService: HelperSerice,
        private toastr: ToastrService, private auth: AuthService, private baserestService: BaseRestService, private ref: ChangeDetectorRef,
        public modalCtrl: ModalController, private toastCtrl: Toast, private lang: ChangelanguageService, public fb: FormBuilder) {
        // this.pagetitle = this.navParams.get('pagetitle');
        this.heading = this.navParams.get('heading');
        this.uuid = this.navParams.get('uuid');
        this.isresultpage = this.navParams.get('isresultpage');
        this.contactForm = this.fb.group({});
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                {
                    this.appconfig = appconfig;
                }
            }
        );
        this.auth.autorisation.subscribe(
            (autorisation) => {
                {
                    this.autorisation = autorisation;
                }
            }
        );
        this.auth.navbarlist.subscribe(
            (navbarlist) => {
                {
                    this.navbarlist = navbarlist;
                }
            }
        );
        this.auth.userchecklistdata.subscribe(
            (userchecklistdata) => {
                {
                    this.userchecklistdata = userchecklistdata;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
                }
           );
    }

    ngOnInit() {
        // this.keyboard.disableScroll(false);
        this.contactContent = this.selectedlanguage.contactform;
        this.contactformdata = this.appinfo.getContactData();
        this.slctItem = this.appinfo.getSlctdItem();
        this.pagetitle = this.contactContent.pagename;
        this.createDyanicForm();
    }
    createDyanicForm() {
        this.contactForm = this.fb.group({
            items: this.fb.array([])
        });
        this.addItem();
    }
    addItem(): void {
        this.items = this.contactForm.get('items') as FormArray;
        let tabItems = this.contactformdata.contactData.result.detaljer;
        let allitems = [];
        for (var cf in tabItems) {
            this.items.push(this.createItem(tabItems[cf]));
        }
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logpageView('kontakt');
        }
        this.ref.detectChanges();
    }
    createItem(item) {
        if (item.type === "input" && item.ld_brugervendtnoegle != 'surveyscore' && item.ld_brugervendtnoegle != 'cpr') {
            let selected_values = item.selected_values && item.selected_values.length > 0 ? item.selected_values[0] : '';
            return this.fb.group({
                name: item.titel,
                value: item.selected_values,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                selected_value: selected_values

            });
        }
        if (item.type === "input" && item.ld_brugervendtnoegle === 'cpr') {
            let selected_values = '';
            if (item.selected_values && item.selected_values.length > 0) {
                selected_values = item.selected_values[0];
                this.cprinvaliderror = true;
                this.tempcprmismatcherrormessage = true;
            }
            else {
                this.cprinvaliderror = false;
            }
            this.cprvalue = selected_values;
            return this.fb.group({
                name: item.titel,
                value: item.selected_values,
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                selected_value: selected_values
            });
        }
        if (item.type === "input" && item.ld_brugervendtnoegle === 'surveyscore') {
            let selected_values = item.selected_values && item.selected_values.length > 0 ? item.selected_values[0] : '';
            return this.fb.group({
                name: item.titel,
                value: item.selected_values[0].replace(/<[^>]*>/g, ''),
                type: item.type,
                readonly: item.readonly,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                selected_value: selected_values

            });
        }
        if (item.type == "date") {
            return this.fb.group({
                name: item.titel,
                value: item.selected_values,
                type: item.type,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle

            });
        }
        if (item.type == "modal") {
            return this.fb.group({
                name: item.titel,
                type: item.type,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle

            });
        }
        if (item.type === "dropdown") {
            var index;
            var defaultslcditem = '';
            var valuelistarray = [];
            for (var el in item.valuelist) {
                valuelistarray.push(item.valuelist[el]);
            }
            if (item.selected_values) {
                for (var elem in item.valuelist) {
                    if (item.selected_values[0] === elem) {
                        index = valuelistarray.indexOf(item.valuelist[elem]);
                        defaultslcditem = item.valuelist[elem];
                    }
                }
            }
            return this.fb.group({
                name: item.titel,
                //options: [source],
                selected_index: index,
                multiselect: item.multiselect,
                type: item.type,
                slctditem: defaultslcditem,
                required: item.mandatory,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                valuelist: item.valuelist,
                options: item.valuelist,
                selected_list: [valuelistarray]

            });

        }
        if (item.click_action === "cancel") {
            let cancelbutton = {
                name: item.titel,
                value: item.valuelist,
                type: item.type,
                action: item.click_action,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,

            }
            this.cancelbutton = cancelbutton;
            return this.fb.group(cancelbutton);
        }
        if (item.click_action === "createAktivitet") {
            let submitbutton = {
                name: item.titel,
                value: item.valuelist,
                type: item.type,
                action: item.click_action,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,

            }
            this.submitbutton = submitbutton;
            return this.fb.group(submitbutton);
        }
        if (item.type === "checkbox") {
            let checkboxbutton = {
                ikon: item.ikon,
                name: item.titel,
                value: item.valuelist,
                type: item.type,
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
            }
            this.checkboxbutton = checkboxbutton;
            return this.fb.group(checkboxbutton);
        }
    }
    updateSelect(slect, item) {
        console.log(item);
    }
    openTerms() {
        this.isLoading = true;
        this.baserestService.getcontactTermsData(this.autorisation).then(
            termsdata => {
                this.termsdata = termsdata;
                this.showmodal();
            }
        )
    }
    ScrollInput() {
        console.log("in keyboad");
        this.keyboard.show();
    }
    validateCpr(e, item) {
        this.cprvalue = e.target.value;
        this.cprinvaliderror = true;
        this.tempcprmismatcherrormessage = true;
        this.cprmismatcherrormessage = true;
        let cprformat = new RegExp(/^[0-9]{10}$/); //new RegExp("/[0-9]{6}\-[0-9]{4}$/");
        if (cprformat.test(e.target.value) == false) {
            this.cprinvaliderror = false;
        }else{
            if (this.userchecklistdata && this.userchecklistdata.result && this.userchecklistdata.result.nemid) {
                this.userpid = this.userchecklistdata.result.nemid;
            }
            this.baserestService.validateCPR(this.userpid, this.cprvalue).then(
                iscprvalid => {
                    this.iscprvalid = iscprvalid;
                    if (iscprvalid === null) {
                        this.isLoading = false;
                        this.tempcprmismatcherrormessage= false;
                        this.ref.detectChanges();
                    }
                    else {
                        this.tempcprmismatcherrormessage= true;
                    }
                },
                error => {
                    console.log(error);
                    this.isLoading = false;
                    this.tempcprmismatcherrormessage= false;
                    this.ref.detectChanges();
                }
            );

        }
    }
    showmodal() {
        const modal = this.modalCtrl.create(TermsComponent, { modal: true, 'termsdata': this.termsdata.result, 'formdata': this.contactForm, 'contacttextContent': this.contactContent });
        this.isLoading = false;
        modal.present();
    }
    termsAccept(event) {
        // console.log(event);
        this.ref.detectChanges();
    }
    submitform() {
        this.submited = true;
        this.cprmismatcherrormessage = this.tempcprmismatcherrormessage;
        this.ref.detectChanges();
        if (!this.errordropdown && !this.errorinput && this.cprmismatcherrormessage === true) {
            this.isLoading = true;
            this.keyboard.hide();
            this.contactPage.scrollToTop();
            this.ref.detectChanges();
            let itemid = this.slctItem.item.id ? this.slctItem.item.id : this.slctItem.item.skema_uuid;
            let updateObject: any = [];
            let formdata: any = [];
            formdata.push(this.contactForm.controls.items.value.map(this.preparesendData, this));
            formdata[0].map(function (key, index) {
                if (key != 'undefined') {
                    updateObject.push(key);
                }
            });
            this.baserestService.contactForm(this.submitbutton.action, itemid, JSON.stringify(updateObject), this.autorisation).then(
                successdata => {
                    this.successblock();
                },
                error=>{
                    this.errorblock();
                }
            )
        }
    }
    successblock() {
        this.isLoading = false;
        let isapp = this.helperService.isApp();
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logEvent(this.submitbutton.action);
        }
        if (isapp === true) {
            const toast = this.toastCtrl.show(this.contactContent.submitsuccessmsg, this.contactContent.toastdration, this.contactContent.toastposition).subscribe(
                toast => {
                    console.log(toast);
                });
        }
        else {
            this.toastr.success(this.contactContent.submitsuccessmsg, this.contactContent.submitsuccesstitle, {
                timeOut: this.contactContent.toastdration,
                positionClass: this.contactContent.webtoastposition
            });

        }
        this.gotoHome();

    }
errorblock() {
        this.isLoading = false;
        let isApp = this.helperService.isApp();
        if (isApp === true) {
            const toast = this.toastCtrl.show(this.contactContent.submiterrormsg, this.contactContent.toastdration, this.contactContent.toastposition).subscribe(
                toast => {
                    console.log(toast);
                });
        }
        else {
            this.toastr.success(this.contactContent.submiterrormsg, this.contactContent.submiterrortitle, {
                timeOut: this.contactContent.toastdration,
                positionClass: this.contactContent.webtoastposition
            });
        }

    }
    gotoHome() {
        this.helperService.home();
    }
    preparesendData(item) {
        let createupdateObject: any;

        if (item.type === 'dropdown') {
            let valueid;
            for (var prop in item.valuelist) {
                if (item.valuelist[prop] === item.slctditem) {
                    valueid = prop;
                }
            }
            createupdateObject = {
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                value: [valueid]
            };
        }
        if (item.type === 'input') {
            createupdateObject = {
                ld_brugervendtnoegle: item.ld_brugervendtnoegle,
                value: [item.value]
            }
        }
        return createupdateObject ? createupdateObject : 'undefined';
    }
    goback() {
        var viewpages = this.navCtrl.getViews();
        if (viewpages && viewpages.length && viewpages.length > 1) {
            this.navCtrl.pop();
        }
        else {
            this.appinfo.setContactData(null);
            this.auth.setwebcontactContent(null);
            let navlist = this.navbarlist;
            navlist.pop();
            this.auth.setnavbarlist(navlist);
        }

    }
}
