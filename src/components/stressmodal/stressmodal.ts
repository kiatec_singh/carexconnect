import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
    selector: 'stressmodal-viewer',
    templateUrl: 'stressmodal.html'
})
export class StressmodalComponent {

    private searchselectedItem;
    private heading;
    private image;
    private teaser;
    private detailsparagraphs;
    private pagetitle = '';
    private bilagurl;
    private bilagName;
    private stressdata: any;
    private clickedItem: any;
    private stressDetailsdata = [];
    private contacttextContent: any;
    private appconfig: any;
    private selectedlanguage;

    constructor(public viewCtrl: ViewController, private params: NavParams,
        private lang:ChangelanguageService) {
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if(selectedlanguage){
                    this.selectedlanguage = selectedlanguage;
                    this.contacttextContent = selectedlanguage.contactform;
                }}
           );
        this.stressdata = this.params.get('stressdata');
        this.clickedItem = this.params.get('clickedItem');
    }

    ngOnInit() {
        if (this.stressdata && this.stressdata.result) {
            let details = [];
            for (let i in this.stressdata.result) {
                details.push({
                    name: i,
                    value: this.stressdata.result[i]
                })
            }
            this.pagetitle = this.clickedItem.date.day + ' ' + this.clickedItem.date.month + ' ' + this.clickedItem.date.year;
            this.stressDetailsdata = details;
        }
    }

    closemodal() {
        this.viewCtrl.dismiss();
    }
}


