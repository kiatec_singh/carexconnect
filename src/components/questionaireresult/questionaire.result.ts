import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { Appinfo } from '../../providers/utils/appinfo';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { contactPage } from '../../pages/contact/contact.page';
import { HelperSerice } from '../../providers/utils/helperService';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
@Component({
    selector: 'questionaireresult-viewer',
    templateUrl: 'questionaire.result.html'
})
export class QuestionaireresultComponent {


    private questionaireresultContent: any;
    private resultData: any;
    private score: any;
    private allquestions: any;
    private isLoading = false;
    private autorisation: any;
    private contactformData: any;
    private heading:any;
    private appconfig:any;

    private pagetitle;
    public selectedlanguage;

    constructor(private params: NavParams, private navCtrl: NavController, 
        private lang:ChangelanguageService, 
        private logprovider: EventLoggerProvider,
        private appInfo: Appinfo, private auth: AuthService, private baserestService: BaseRestService,private helperService:HelperSerice) {
        this.resultData = this.params.get('resultData');
        this.allquestions = this.params.get('allquestions');
        this.autorisation = this.params.get('autorisation');
        this.heading = this.params.get('heading');
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    if (appconfig) {
                        this.appconfig = appconfig;
                    }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if(selectedlanguage){
                    this.selectedlanguage = selectedlanguage;
                    this.questionaireresultContent = selectedlanguage.questionaireresult;
                }}
           );

    }

    ngOnInit() {
        this.pagetitle =  this.questionaireresultContent.pagename
        this.heading = this.heading ? this.heading :this.params.get('pagename');
        this.score = this.resultData.result.score;
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logpageView('Resultat');
        }
    }

    gotoContactpage() {
        this.isLoading = true;
        this.allquestions.skema_uuid = '54ae5f99-5c00-4af5-a9f0-38a910042ed2';
        this.baserestService.getdetails('getSkemaForm', this.allquestions.skema_uuid, this.autorisation).then(
            contactformData => {
                this.contactformData = contactformData;
                this.appInfo.setSlctdItem(this.allquestions);
                this.appInfo.setContactData(this.contactformData);
                this.auth.setwebcontactContent(this.contactformData);
                this.navitoContact();
            },
            error => { this.isLoading = false }
        )

    }
    navitoContact() {
        this.isLoading = false;
        this.navCtrl.push(contactPage, { 'data': this.contactformData, 'pagetitle': this.heading, 'heading': this.pagetitle, 'uuid': this.allquestions.id, 'isresultpage': true});
    }
    gotoHome() {
        this.helperService.home();
    }

}


