
import { Component, ViewChild, ViewChildren, ChangeDetectorRef } from '@angular/core';
import { NavParams, Navbar, AlertController } from 'ionic-angular';
import { Button, NavController, List } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { AlldetailsPage } from '../../pages/alldetails/alldetails.page';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import { contactPage } from '../../pages/contact/contact.page';
import { StressPage } from '../../pages/stress/stress.page';
import { QuestionairePage } from '../../pages/questionaire/questionaire.page';
import { questionairehistoryPage } from '../../pages/questionairehistory/questionairehistory.page';
import { Appinfo } from '../../providers/utils/appinfo';
import { deeplinkingService } from '../../providers/utils/deeplinkingService';
import { ListdetailsPage } from '../../pages/listdetails/listdetails.page';
import { HelperSerice } from '../../providers/utils/helperService';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';

@Component({
  selector: 'listdetails-viewer',
  templateUrl: 'listdetails.html'
})
export class ListdetailsComponent {
  private selectedMenuItem;
  @ViewChild(Navbar) navbar: Navbar;
  @ViewChildren('navbuttons') navbuttons: Button;
  @ViewChild('detailsContent') detailsContent;
  @ViewChildren('pager') pager: List;
  private tabs;
  private pagetitle;
  private heading;
  private tabsContent: any = [];
  private user;
  private selectedPage;
  private tabsdata;
  private tabstoDisplay: any = [];
  private appconfig: any;
  private selectedItem: any;
  private autorisation: any;
  private detailsdata;
  private isLoading = true;
  private listcontent: any = [];
  private tabsgroup;
  private content;
  private anchorContent;
  private isdownload;
  private isdelete;
  private isvitaltest;
  private langContent;
  private userlanguage;
  private url;


  constructor(private navParam: NavParams, private deeplinkingservice: deeplinkingService, private iab:InAppBrowser,private lang: ChangelanguageService,
    private ref: ChangeDetectorRef, private auth: AuthService, private appinfo: Appinfo,
    private helperservice : HelperSerice,private alrtCtrl: AlertController,
    private logprovider: EventLoggerProvider, private navCtrl: NavController, private baserestService: BaseRestService) {
    this.pagetitle = this.navParam.get('pagetitle');
    this.heading = this.navParam.get('heading');
    this.listcontent = this.navParam.get('submenu_text');
    this.tabstoDisplay = this.helperservice.getlistdetailsmenu();
    this.auth.autorisation.subscribe(
      (autorisation) => {
          this.autorisation = autorisation;
      });
    this.auth.appconfig.subscribe(
      (appconfig: any) => {
          this.appconfig = appconfig;
      });
  this.auth.setsubtitle(this.pagetitle);
    this.auth.user.subscribe(
      (user) => {
          this.user = user;
      });
    this.lang.userlanguage.subscribe(
      (userlanguage) => {
              if (userlanguage) {
                  this.userlanguage = userlanguage;
                  this.langContent = this.appconfig.configuration.texts[this.userlanguage];
              }});
  }
  ngOnInit() {
    this.isLoading = false;
  }
  gotoDetails(slctitem) {
    this.isLoading = true;
    this.ref.detectChanges();
    this.selectedItem = slctitem;
    if (this.selectedItem.type == 'button' && this.selectedItem.action && this.selectedItem.action!='externallink') {
      if (this.selectedItem.action === "ReadSurveyAndAnswers") {
        let actionparameterkey = 'survey_uuid';
        let aktivitet_uuid  = this.selectedItem.aktivitet_uuid? this.selectedItem.aktivitet_uuid: '';
        this.baserestService.getquestionairdetails(this.selectedItem.action, actionparameterkey, this.selectedItem.click_action_parameters.survey_uuid,aktivitet_uuid, this.autorisation).then(
          allquestionsdata => {
            this.appinfo.setQuestionaireData(allquestionsdata);
            this.isLoading = false;
            this.navCtrl.push(QuestionairePage, { 'pagetitle': this.selectedItem.name, 'heading': this.pagetitle, 'fromnotification': true });
          },
          error => { this.isLoading = false; this.deeplinkingservice.errorblock(); }
        );
      }
      else if (this.selectedItem.action === "getSurveyResults") {

        this.baserestService.getsurveyResults(slctitem.action, this.selectedItem.click_action_parameters.survey_uuid, this.autorisation).then(
          detailsdata => {
            this.detailsdata = detailsdata;
            if (detailsdata) {
              this.naviDetails();
            }

          },
          error => { this.isLoading = false; this.deeplinkingservice.errorblock(); }
        )
      }

      else {
        this.baserestService.getdetails(slctitem.action, slctitem.id, this.autorisation).then(
          detailsdata => {
            this.detailsdata = detailsdata;
            this.naviDetails();
          },
          error => { this.isLoading = false; this.deeplinkingservice.errorblock(); }
        )
      }
    }
    if (this.selectedItem.type && this.selectedItem.type === "group_buttons") {
      this.isLoading = false;
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent(this.selectedItem.name);
      }
      this.helperservice.setlistdetailsmenu(this.selectedItem.tabstoDisplay);
     this.navCtrl.push(ListdetailsPage, { 'selectedItem': this.selectedItem, 'submenu_text': this.selectedItem.submenu_text, 'pagetitle': this.selectedItem.name, 'heading':this.pagetitle}, { animate: true, direction: 'forward' });
    }
    if (this.selectedItem.action === 'externallink') {
      this.url= this.selectedItem.click_action_parameters.survey_uuid;
        this.openexternalUrl();
    }
    else {
      this.detailsPage();
    }


    if (this.appconfig.environment.trackanalytics === true) {
      this.logprovider.logEvent(this.selectedItem.name);
    }
  }
  detailsPage() {
    console.log("details")
    if (this.selectedItem.type && this.selectedItem.type === "group_tabs") {
      let tabsContent: any;
      this.tabs = this.selectedItem.tabstoDisplay;
      let tabstoDisplay = [];
      let action = this.tabs[0].click_action;
      let id = this.tabs[0].skema_uuid;

      for (let item in this.tabs) {
        if (this.tabs[item].click_action) {
          tabstoDisplay.push({
            'name': this.tabs[item].funktionsnavn,
            'icon': this.tabs[item].ikon,
            'id': this.tabs[item].skema_uuid,
            'type': this.tabs[item].elementtype,
            'action': this.tabs[item].click_action
          });
        }
        else {
          tabstoDisplay.push({
            'name': this.tabs[item].funktionsnavn,
            'icon': this.tabs[item].ikon,
            'id': this.tabs[item].skema_uuid,
            'type': this.tabs[item].elementtype,
          });
        }
      }
      this.baserestService.getdetails(action, id, this.autorisation).then(
        detailsdata => {
          this.tabsgroup = detailsdata;
          this.showdetailsPage(tabstoDisplay);
        },
        error => { this.isLoading = false }
      )
    }

  }
  showdetailsPage(tabstoDisplay) {
    if (this.appconfig.environment.trackanalytics === true) {
      this.logprovider.logEvent(tabstoDisplay[0].name);
    }
    this.isLoading = false;
    this.auth.setdetailContent(this.tabsgroup.indhold.indhold);
    this.navCtrl.push(AlldetailsPage, { 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.selectedItem.name, 'heading': this.pagetitle });
  }

  naviDetails() {
    let tabsContent = [];
    tabsContent = this.detailsdata.indhold && this.detailsdata.indhold.indhold ? this.detailsdata.indhold.indhold : '';

    let tabstoDisplay = [];
    if (this.selectedItem.action) {
      if (this.selectedItem.action === "getSkemaForm") {
        this.appinfo.setContactData(this.detailsdata);
        this.appinfo.setSlctdItem(this.selectedItem);
        this.navCtrl.push(contactPage, { 'data': this.detailsdata, 'pagetitle': this.selectedItem.name, 'heading': this.pagetitle, 'uuid': this.selectedItem.id, 'isresultpage': true });
      }
      else if (this.selectedItem.action === "listObjectsSimpel") {
        this.appinfo.setSlctdItem(this.selectedItem);
        this.navCtrl.push(StressPage, { 'data': this.detailsdata, 'pagetitle': this.selectedItem.name, 'heading': this.pagetitle, 'selectedItem': this.selectedItem });
      }
      else if (this.selectedItem.action === "getSurveyResults") {
        this.navCtrl.push(questionairehistoryPage, { 'data': this.detailsdata, 'pagetitle': this.selectedItem.name, 'heading': this.pagetitle, 'selectedItem': this.selectedItem });
      }

      else if (this.detailsdata.indhold) {
        tabstoDisplay.push(this.detailsdata.indhold.tab_titel);
        this.navCtrl.push(AlldetailsPage, { 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.selectedItem.name, 'heading': this.pagetitle });
      }
      else if (this.detailsdata.group_tabs) {
        tabstoDisplay.push(this.detailsdata.indhold.tab_titel);
      }
      else {
        this.navCtrl.push(AlldetailsPage, { 'tabstoDisplay': tabstoDisplay, 'pagetitle': this.selectedItem.name, 'heading': this.pagetitle });
      }
      this.auth.setdetailContent(tabsContent);
    }
    this.isLoading = false;
  }
  openexternalUrl() {
    this.isdownload = this.url.indexOf('downloadreport') != -1 ? true : false;
    this.isdelete = this.url.indexOf('delete') != -1 ? true : false;
    this.isvitaltest = this.url.indexOf('vitaltest') != -1 ? true : false;
    if (this.isdelete) {
        let deleteurl = this.url + 'userId=' + this.user.id + '&delete=true';
        let alert = this.alrtCtrl.create({
            title: this.langContent.anchor.deletetitle,
            message: this.langContent.anchor.deletemessage,
            buttons: [
                {
                    text: this.langContent.anchor.deletecancel,
                    role: this.langContent.anchor.deletecancel,
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.langContent.anchor.deleteconfirm,
                    handler: () => {
                        this.iab.create(deleteurl, "_system", "location=yes,hardwareback=yes");
                        if (this.appconfig.environment.trackanalytics === true) {
                            this.logprovider.logEvent("VitalDeletereport");
                        }
                    }
                }
            ]
        });
        alert.present();
    }
    if (this.isdownload) {
        let downlaodurl = this.url + 'userId=' + this.user.id;
        this.iab.create(downlaodurl, "_system", "location=yes,hardwareback=yes");
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logEvent("VitalDownloadreport");
        }
    }
    if (this.isvitaltest) {
        let vitalurl = this.url + this.user.id;
        this.iab.create(vitalurl, "_system", "location=yes,hardwareback=yes");
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logEvent("VitalTest");
        }
    }
    if (!this.isdelete && !this.isdownload && !this.isvitaltest) {
        this.iab.create(this.url, "_system", "location=yes,hardwareback=yes");
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logEvent(this.url);
        }
    }
    this.isLoading = false;
}
}