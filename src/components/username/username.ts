import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { StorageService } from '../../providers/storageservice/storageservice';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { OtpPage } from '../../pages/otp/otp';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
    selector: 'username-viewer',
    templateUrl: 'username.html'
})
export class UsernameComponent {


    private loginForm: FormGroup;
    private success;
    private username;
    private userdata;
    private error = false;
    private noemailerror = false;
    private usercontent;
    private appconfig;
    private isLoading = false;
    public spinnercolor;
    private selectedlanguage;
    private noauthorisation = false;


    constructor(private fb: FormBuilder, private navCtrl: NavController, private lang: ChangelanguageService,
        private auth: AuthService, private baserestService: BaseRestService, private storageService: StorageService) {
        this.loginForm = this.fb.group({
            userid: ['', Validators.required]
        });
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                this.appconfig = appconfig;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                this.selectedlanguage = selectedlanguage;
            }
        );
    }

    ngOnInit() {
        this.spinnercolor = this.appconfig.configuration.buttonspanelstext_color;
        this.usercontent = this.selectedlanguage.username;
    }

    verifyuser() {
        this.isLoading = true;
        this.username = this.loginForm.value.userid;
        this.error = false;
        this.noauthorisation = false;
        this.noemailerror = false;
        this.baserestService.OTP(this.username).then(
            (success) => {
                this.setuserData(success);
            },
            error => {
                console.log(error);
                // noemailerror
                this.error = true;
                this.isLoading = false;

            }
        )
    }
    setuserData(success) {
        this.isLoading = false;
        if (success === true) {
            this.navCtrl.push(OtpPage, { username: this.username });
            return;

        }
        if (success === false) {
            this.noemailerror = true;
            return;
        }
        else {
            this.noauthorisation = true;
            return;

        }
    }
    uservalueChange() {
        this.error = false;
    }
}