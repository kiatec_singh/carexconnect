import { Component, ViewChild, EventEmitter, Output, NgZone } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { ModalController, Slides, Events, Platform } from 'ionic-angular';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';


@Component({
  selector: 'alldetails-viewer',
  templateUrl: 'alldetails.html'
})
export class AlldetailsComponent {

  private tabstoDisplay;
  private pagetitle;
  private appconfig;
  @ViewChild('tabsviewer') tabsviewer: any
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;
  //private isScrolled;
  @Output() isScrolled = new EventEmitter<Boolean>();
  private showsearchbutton = false;
  private tabsdata;
  private searchContent;
  private loading: boolean;
  private textfilteredList: any = [];
  private options: any;
  private heading;
  private user: any;
  private slctopic: any;
  private tabsContent;


  constructor(private navParam: NavParams, @Inject(DOCUMENT) document, private logprovider: EventLoggerProvider, private events: Events, private zone: NgZone,
    private auth: AuthService, public modalCtrl: ModalController,
    private platform: Platform,
    private navCtrl: NavController, private baserestService: BaseRestService) {
    this.tabstoDisplay = this.navParam.get('tabstoDisplay');
    this.pagetitle = this.navParam.get('pagetitle');
    this.heading = this.navParam.get('heading');
    this.auth.setsubtitle(this.navParam.get('pagetitle'));
    this.auth.user.subscribe(
      (user) => {
          this.user = user;
      }
    );
    this.auth.appconfig.subscribe(
      (appconfig: any) => {
        this.appconfig = appconfig;
      }
    );
  }
  ngOnInit() {
    if (this.tabstoDisplay[0] && this.tabstoDisplay[0].name) {
      this.auth.setheaderpageotopicTitle(this.tabstoDisplay[0].name);
    }
    this.auth.setShowback(false);
  }

  ngAfterViewInit(): void {
    this.content.ionScrollEnd.subscribe((e) => {
      if (e.scrollTop < 28) {
        this.events.publish('scroll', false);
      }
    });

    this.content.ionScroll.subscribe((e) => {
      this.zone.run(() => {
        if (e.scrollTop > 28) {
          this.events.publish('scroll', true);
        }
        else {
          this.events.publish('scroll', false);
        }
      });
    });
  }
}
