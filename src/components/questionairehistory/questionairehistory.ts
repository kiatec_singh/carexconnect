
import { Component, NgZone, ChangeDetectorRef } from '@angular/core';
import { NavParams, ViewController, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { ModalController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { CharthistorymodalComponent } from '../charthistorymodal/charthistorymodal';
import { Appinfo } from '../../providers/utils/appinfo';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';



@Component({
    selector: 'questionairehistory-viewer',
    templateUrl: 'questionairehistory.html'
})
export class QuestionairehistoryComponent {

    private searchselectedItem;
    private heading;
    private image;
    private teaser;
    private detailsparagraphs;
    private pagetitle: any;
    private bilagurl;
    private bilagName;
    private islist = true;
    private histroyData: any;
    private autorisation: any;
    private uuid: any;
    private histroydataArray = [];
    private stressdata: any;
    private sucessdata: any;
    private months = [];
    private isLoading = false;
    private selectedItem: any;
    private clickedItem: any;
    private dateSorted = true;
    private categorySorted = false;
    private ascdateOrder = false;
    private asccategoryOrder = false;
    private questionairehistoryContent: any;
    private showdots = true;
    private draggeditemIndex = undefined;
    public webhistroryContent: any;
    private slctAction: any;
    private activeitemSlide: any;
    private selectedlanguage;

    constructor(public modalCtrl: ModalController, private navParams: NavParams, private alrtCtrl: AlertController,
        private ref: ChangeDetectorRef, private lang: ChangelanguageService, private viewCtrl: ViewController, private appinfo: Appinfo, private baserestService: BaseRestService, private auth: AuthService) {
        this.auth.autorisation.subscribe(
            (autorisation) => {
                {
                    this.autorisation = autorisation;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
            }
        );
        this.auth.webhistroryquestionarieContent.subscribe(
            (webhistroryContent) => {
                this.webhistroryContent = webhistroryContent;
            }
        )
        this.histroyData = this.navParams.get('data');
        this.pagetitle = this.navParams.get('pagetitle');
        this.selectedItem = this.navParams.get('selectedItem');
        this.heading = this.navParams.get('heading');;

    }

    ngOnInit() {
        if (this.selectedItem && this.selectedItem.click_action_parameters.survey_uuid) {
            this.uuid = this.selectedItem.uuid;
            this.slctAction = this.selectedItem.action;
        }
        else {
            this.selectedItem = this.appinfo.getSlctdItem();
            this.uuid = this.selectedItem.uuid;
            this.slctAction = this.selectedItem.item.click_action;
            this.histroyData = this.webhistroryContent;
        }
        this.isLoading = true;
        this.questionairehistoryContent = this.selectedlanguage.questionairehistory;
        this.months = this.selectedlanguage.questionairehistory.months;
        this.setData();
    }
    setData() {
        let alldata = [];
        let dateobject: any;
         if(this.histroyData.result && this.histroyData.result.score) {
        for (let i in this.histroyData.result.score) {
            if (this.histroyData.result.score[i].fra) {
                var s = this.histroyData.result.score[i].fra.replace(/[ :]/g, "-").split("-");
                var datestring = s[0] + '-' + s[1] + '-' + s[2];
                var d  = new Date(datestring);
                let month = this.months[d.getMonth()];
                let currentdate = new Date();
                let valid = currentdate > d ? false : true;
                dateobject = {
                    day: s[2],
                    year: s[0] ,
                    month: month,
                    datestring: d
                }
                alldata.push({
                    date: dateobject,
                    color: this.histroyData.result.score[i].color,
                    beskrivelse: this.histroyData.result.score[i].beskrivelse,
                    score: this.histroyData.result.score[i].score,
                    icon: this.histroyData.result.score[i].icon,
                    aktivitet_uuid: this.histroyData.result.score[i].aktivitet_uuid,
                    showdots: true,
                    valid: valid
                })
            }
        }
        alldata.sort(function (a, b) {
            let dateA: any = new Date(a.date.datestring);
            let dateB: any = new Date(b.date.datestring);
            return dateB - dateA;
        });
        this.histroydataArray = alldata;
        this.isLoading = false;
    }
    this.isLoading = false;
    }

    openModal(item) {
        this.isLoading = true;
        this.clickedItem = item;
        this.baserestService.getquestionairehistroyDetails(this.histroyData.result.survey_uuid, item.aktivitet_uuid, this.autorisation).then(
            stressdata => { this.stressdata = stressdata; this.showmodal() }
        )

    }
    sortbyDate() {
        this.dateSorted = true;
        this.categorySorted = false;
        this.ascdateOrder = this.ascdateOrder === false ? true : false;
        if (this.ascdateOrder === false) {
            this.histroydataArray.sort(function (a, b) {
                let dateA: any = new Date(a.date.datestring);
                let dateB: any = new Date(b.date.datestring);
                return dateB - dateA;
            });
        }
        if (this.ascdateOrder === true) {
            this.histroydataArray.sort(function (a, b) {
                let dateA: any = new Date(a.date.datestring);
                let dateB: any = new Date(b.date.datestring);
                return dateA - dateB;
            });
        }
    }
    sortbyCategory() {
        this.dateSorted = false;
        this.categorySorted = true;
        this.asccategoryOrder = this.asccategoryOrder === false ? true : false;
        if (this.asccategoryOrder === false) {
            this.histroydataArray.sort(function (a, b) {
                let titleA = a.score;
                let titleB = b.score;
                return titleA - titleB;
            });
        }
        if (this.asccategoryOrder === true) {
            this.histroydataArray.sort(function (a, b) {
                let titleA = a.score;
                let titleB = b.score;
                return titleB - titleA;
            });
        }
    }
    showmodal() {
        const modal = this.modalCtrl.create(CharthistorymodalComponent, { modal: true, 'historydata': this.stressdata, 'clickedItem': this.clickedItem, 'autorisation': this.autorisation });
        this.isLoading = false;
        modal.present();
    }
    closemodal() {
        this.viewCtrl.dismiss();
    }
    deleteItem(item) {
        let alert = this.alrtCtrl.create({
            title: this.questionairehistoryContent.deletetitle,
            message: this.questionairehistoryContent.deletemessage,
            buttons: [
                {
                    text: this.questionairehistoryContent.deletecancel,
                    role: this.questionairehistoryContent.deletecancel,
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.questionairehistoryContent.deleteconfirm,
                    handler: () => {
                        this.isLoading = true;
                        this.baserestService.deletesurveyresultItem(this.histroyData.result.survey_uuid, item.aktivitet_uuid, this.autorisation).then(
                            sucessdata => {
                                this.sucessdata = sucessdata; this.gethistroyData();
                            }
                        )
                    }
                }
            ]
        });
        alert.present();
    }
    itemdraged(event, item, i, itemSlide) {
        this.activeitemSlide = itemSlide;
        this.draggeditemIndex = this.draggeditemIndex === undefined ? i : this.draggeditemIndex;
        if ((this.draggeditemIndex != i) == true) {
            this.histroydataArray[this.draggeditemIndex].showdots = true;
        }
        if (event.getSlidingPercent() === 1) {
            item.showdots = false;
            this.ref.detectChanges();
        }
        else if (event.getSlidingPercent() < 1) {
            this.histroydataArray[this.draggeditemIndex].showdots = true;
            this.activeitemSlide.close();
            this.activeitemSlide = null;
            this.ref.detectChanges();
        }
        this.draggeditemIndex = i;
    }
    gethistroyData() {
        this.baserestService.getsurveyResults(this.slctAction, this.sucessdata.result.survey_uuid, this.autorisation).then(
            histroyData => {
                this.histroyData = histroyData;
                this.setData();
            },
            error => { this.isLoading = false }
        )
    }
    customToogle() {
        this.islist = this.islist ? false : true;
    }

    showlist() {
        this.islist = true;
        this.auth.setislist(true);
    }
    showgraph() {
        this.islist = false;
        this.auth.setislist(false);
    }
    openItem(event, itemSlide, item, i) {
        itemSlide._openAmount = 0;
        itemSlide._startX = 0;
        this.draggeditemIndex = this.draggeditemIndex === undefined ? i : this.draggeditemIndex;
        event.stopPropagation();
        if (!this.activeitemSlide) {
            this.activeitemSlide = itemSlide;
            itemSlide.moveSliding(-150);
            itemSlide.moveSliding(-150);
            this.histroydataArray[this.draggeditemIndex].showdots = false;
        }
        else if (this.activeitemSlide && this.activeitemSlide.item) {
            if (itemSlide.item.id === this.activeitemSlide.item.id) {
                this.activeitemSlide.close();
                this.activeitemSlide = null;
                this.histroydataArray[this.draggeditemIndex].showdots = true;
            }
            else if (itemSlide.item.id != this.activeitemSlide.item.id) {
                this.activeitemSlide.close();
                this.activeitemSlide = null;
                this.activeitemSlide = itemSlide;
                itemSlide.moveSliding(-150);
                itemSlide.moveSliding(-150);
                this.histroydataArray[this.draggeditemIndex].showdots = false;
            }
        }
        this.draggeditemIndex = i;
        this.ref.detectChanges();
    }
}


