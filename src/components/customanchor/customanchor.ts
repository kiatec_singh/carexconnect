import { Component, Input, ChangeDetectorRef } from "@angular/core";
import { AuthService } from "../../providers/authenticationservice/auth.service";
import { AlertController, ViewController, NavController } from "ionic-angular";
import { EventLoggerProvider } from "../../providers/firebase/eventlogger";
import { deeplinkingService } from "../../providers/utils/deeplinkingService";
import { Events } from "ionic-angular/util/events";
import { ChangelanguageService } from "../../providers/utils/changelanguageservice";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { concat } from "rxjs/observable/concat";

var htmlAttributeParser = require("html-attribute-parser");

@Component({
  selector: "customanchor-viewer",
  templateUrl: "customanchor.html",
})
export class CustomanchorComponent {
  private url;
  private urltext;
  private isdownload = false;
  private isdelete = false;
  private userdata = null;
  private isvitaltest = false;
  private appconfig: any;
  public isLoading = false;
  private autorisation;
  private resultpath;
  private anchorContent;
  private isbutton = false;
  public iframedUrl = null;
  private textpart1 = null;
  private textpart2 = null;
  private links = [];
  private lastlength=0;
  private alllinks = [];

  @Input() content: string;
  private selectedlanguage;
  constructor(
    private auth: AuthService,
    private events: Events,
    private iab: InAppBrowser,
    public viewCtrl: ViewController,
    private lang: ChangelanguageService,
    private deeplinkService: deeplinkingService,
    private alrtCtrl: AlertController,
    private logprovider: EventLoggerProvider,
    private ref: ChangeDetectorRef
  ) {
    this.auth.user.subscribe((userdata) => {
      this.userdata = userdata;
    });
    this.auth.appconfig.subscribe((appconfig) => {
      this.appconfig = appconfig;
    });
    this.auth.autorisation.subscribe((autorisation) => {
      this.autorisation = autorisation;
    });
    events.subscribe("spinner", (spinner) => {
      this.isLoading = spinner;
      if (!this.ref["destroyed"]) {
        this.ref.detectChanges();
      }
    });
    this.lang.selectedlanguage.subscribe((selectedlanguage: any) => {
      if (selectedlanguage) {
        this.selectedlanguage = selectedlanguage;
        this.anchorContent = selectedlanguage.anchor;
      }
    });
    if (!this.userdata) {
      this.userdata = this.auth.getUserInfo();
    }
  }
  ngOnInit() {
    if (this.content.indexOf("<a") == 0) {
      const root: any = htmlAttributeParser(this.content);
      this.url = root.attributes["href"];
      this.urltext = root.value;
      if (
        root.attributes &&
        root.attributes["data-action"] &&
        root.attributes["data-action"] === "iframe"
      ) {
        this.iframedUrl = root.attributes["href"];
      }
      if (
        root.attributes &&
        root.attributes["role"] &&
        root.attributes["role"] === "button"
      ) {
        this.isbutton = true;
      }
    }
    if (this.content.indexOf("<a") > 0) {
        console.log(this.content.length);
        this.makeLinks(this.content);
      this.links = this.alllinks;
      console.log(this.links);
    }
  }
  makeLinks(content) {
    let aindex = content.indexOf("<a");
    let acloseindex = content.indexOf("</a>");
    let contentlength=0;
    if (aindex >0) {
        contentlength = content.substring(0, aindex).length;
        this.alllinks.push({
        isanchor: false,
        urltext: content.substring(0, aindex),
      });
      this.lastlength=this.lastlength+contentlength;
      if(this.lastlength!=this.content.length){
        this.makeLinks(this.content.substring(this.lastlength, this.content.length))
    }
    }
    else if (aindex == -1) {
        contentlength = content.length;
        this.alllinks.push({
        isanchor: false,
        urltext: content.substring(0, content.length),
      });
      this.lastlength=this.lastlength+contentlength;
      if(this.lastlength!=this.content.length){
        this.makeLinks(this.content.substring(this.lastlength, this.content.length))
    }
    } 
    else if (aindex == 0) {
      let aindex = content.indexOf("<a");
      let acloseindex = content.indexOf("</a>");
      let anchor = content.substring(aindex, acloseindex + 4);
      contentlength = anchor.length;
      const root: any = htmlAttributeParser(anchor);
      this.alllinks.push({
        isanchor: true,
        anchor: anchor,
        value: root.value,
      });
      this.lastlength=this.lastlength+contentlength;
      if(this.lastlength!=this.content.length){
        this.makeLinks(this.content.substring(this.lastlength,this.content.length))
    }
    }
  }
  ngOnDestroy(): void {
    this.events.unsubscribe("spinner");
  }
  openWindow(event, anchor) {
    event.preventDefault();
    this.openLink(anchor);
  }
  openLink(content) {
    const root: any = htmlAttributeParser(content);
    this.url = root.attributes["href"];    
    if (
      root.attributes &&
      root.attributes["data-action"] &&
      root.attributes["data-action"] === "getbutton"
    ) {
      this.auth.setSpinner(true);
      this.events.publish("spinner", true);
      this.deeplinkService.navigateinAPP(root.attributes.href);
    } else {
      this.openexternalUrl(content);
      this.events.publish("spinner", false);
    }
  }
  openexternalUrl(content) {
    this.isdownload =
      content.indexOf("downloadreport") != -1 ? true : false;
    this.isdelete = content.indexOf("delete") != -1 ? true : false;
    this.isvitaltest = content.indexOf("vitaltest") != -1 ? true : false;
    let userid;
    if (this.userdata) {
      userid = this.userdata.id ? this.userdata.id : this.userdata[3];
    }
    if (this.isdelete) {
      let deleteurl = this.url + "userId=" + userid + "&delete=true";
      let alert = this.alrtCtrl.create({
        title: this.anchorContent.deletetitle,
        message: this.anchorContent.deletemessage,
        buttons: [
          {
            text: this.anchorContent.deletecancel,
            role: this.anchorContent.deletecancel,
            handler: () => {
              console.log("Cancel clicked");
            },
          },
          {
            text: this.anchorContent.deleteconfirm,
            handler: () => {
              this.iab.create(
                deleteurl,
                "_system",
                "location=yes,hardwareback=yes"
              );
              if (this.appconfig.environment.trackanalytics === true) {
                this.logprovider.logEvent("VitalDeletereport");
              }
            },
          },
        ],
      });
      alert.present();
    }
    if (this.isdownload) {
      let downlaodurl = this.url + "userId=" + userid;
      this.iab.create(downlaodurl, "_system", "location=yes,hardwareback=yes");
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent("VitalDownloadreport");
      }
    }
    if (this.isvitaltest) {
      let vitalurl = this.url + userid;
      this.iab.create(vitalurl, "_system", "location=yes,hardwareback=yes");
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent("VitalTest");
      }
    }
    if (!this.isdelete && !this.isdownload && !this.isvitaltest) {
      this.iab.create(this.url, "_system", "location=yes,hardwareback=yes");
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent(this.url);
      }
    }
  }
}
