import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NemidPage } from '../../pages/nemid/nemid.page';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { ThemeChangeService } from '../../providers/restservice/themechange.service';
import { StatusBar } from '@ionic-native/status-bar';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';

@Component({
    selector: 'idverify-viewer',
    templateUrl: 'idverify.html'
})
export class IdverifyComponent {

    private heading;
    private paragraphs;
    private continue;
    private verifydata;
    private isLoading;
    private appconfig;
    private selectedlanguage;

    constructor(private navCtrl: NavController, private auth: AuthService,private baseservice:BaseRestService, private lang:ChangelanguageService, 
        private themechangeService: ThemeChangeService, private statusbar: StatusBar) {
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    this.appconfig = appconfig;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
                }
           );
    }


    ngOnInit() {
        this.isLoading = true;      
        if (this.appconfig && this.appconfig.configuration) {
            if(this.selectedlanguage){
                this.verifydata = this.selectedlanguage.idverify;
            }
            else{
                this.verifydata = this.appconfig.configuration.texts.da.idverify; 
            }
            this.setData();
        } else {
            this.baseservice.getappConfig().then(
                (appconfig: any) => {
                    this.appconfig = appconfig;
                    this.auth.setappconfig(appconfig);
                    this.statusbar.backgroundColorByHexString(appconfig.configuration.primary);
                    if(appconfig.configuration.statusbartext == 'light'){
                        this.statusbar.styleLightContent();
                      }
                      if(appconfig.configuration.statusbartext == 'dark'){
                        this.statusbar.styleDefault();
                      }
                    this.themechangeService.changetoDynamicTheme(appconfig);
                    this.auth.setappconfig(appconfig);
                    if(this.selectedlanguage){
                        this.verifydata = this.selectedlanguage.idverify;
                    }
                    else{
                        this.verifydata = appconfig.configuration.texts.da.idverify; 
                    }
                    this.setData();
                });
        }


        this.setData();
    }
    setData() {
        this.heading = this.verifydata.heading;
        this.paragraphs = this.verifydata.paragraphs;
        this.continue = this.verifydata.continue;
        this.isLoading = false;
    }
    gotoNemid() {
        this.navCtrl.push(NemidPage);
    }


}


