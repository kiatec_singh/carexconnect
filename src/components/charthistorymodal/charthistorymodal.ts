import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { changelanguageComponent } from '../changelanguage/changelanguage';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';



@Component({
    selector: 'charthistorymodal-viewer',
    templateUrl: 'charthistorymodal.html'
})
export class CharthistorymodalComponent {

    private searchselectedItem;
    private heading;
    private image;
    private teaser;
    private detailsparagraphs;
    private pagetitle: any;
    private bilagurl;
    private bilagName;
    private historydata: any;
    private clickedItem: any;
    private stressDetailsdata: any;
    private months;
    private questionairehistoryContent;
    private selectedlanguage;

    constructor(public viewCtrl: ViewController, private params: NavParams, private lang: ChangelanguageService) {
        this.historydata = this.params.get('historydata');
        this.clickedItem = this.params.get('clickedItem');
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if (selectedlanguage) {
                    this.selectedlanguage = selectedlanguage;
                }
            }
        );

    }

    ngOnInit() {
        console.log(this.clickedItem);
        if (this.historydata && this.historydata.result) {
            let details = [];
            let number = null;
            let counter = null;
            for (let i in this.historydata.result) {
                number = i;
                counter = number;
                for (let keyvalue in this.historydata.result[i]) {
                    number = i == counter ? number : null;
                    details.push({
                        number: number ? Number(number) + Number(1) : null,
                        name: keyvalue,
                        value: this.historydata.result[i][keyvalue]
                    })
                    counter++;
                }
            }
            this.stressDetailsdata = details;
        }
        this.getTitle();


    }
    getTitle() {
        if (this.clickedItem && this.clickedItem.date) {
            this.pagetitle = this.clickedItem.date.day + ' ' + this.clickedItem.date.month + ' ' + this.clickedItem.date.year;
        }
        else {
            this.questionairehistoryContent = this.selectedlanguage.questionairehistory;
            this.months = this.selectedlanguage.questionairehistory.months;
            var dateparts = this.clickedItem[0].split('-');
            this.pagetitle = dateparts[0] +" "+this.months[dateparts[1]-1] +" " +dateparts[2];
        }
    }
    closemodal() {
        this.viewCtrl.dismiss();
    }
}


