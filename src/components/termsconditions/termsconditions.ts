import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Platform, Events } from 'ionic-angular';
import { HomePage } from '../../pages/home/home.page';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Termsconditions } from '../../models/termsconditions.model';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { environment } from '../../environments/environment';
import { StatusBar } from '@ionic-native/status-bar';
import { ThemeChangeService } from '../../providers/restservice/themechange.service';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
    selector: 'termsconditions-viewer',
    templateUrl: 'termsconditions.html'
})
export class TermsconditionsComponent {

    private heading;
    private paragraphs: any;
    private termsdata: any;
    private loading;

    private termsread;
    private ismenupage;
    private termsForm;
    private userdata;
    private termspagecontent;
    private checkboxcontent;
    private acceptermmodel = true;
    private saveerror;
    private saveerrormessage;
    private termssubmitdata;
    private isLoading = false;
    private termsmodel: Termsconditions;
    private appversion_number;
    private title = "Betingelser";
    public appconfig: any;
    private selectedlanguage;


    constructor(private navCtrl: NavController,
        private lang:ChangelanguageService,
         private platform: Platform, private events: Events, private statusbar: StatusBar, private themechangeService: ThemeChangeService,
        private ref: ChangeDetectorRef, private auth: AuthService, private navprams: NavParams, private baserestService: BaseRestService, private fb: FormBuilder) {
        this.termsForm = this.fb.group({
            accepterm: ['', Validators.required]
        });
        this.auth.user.subscribe(
            (userdata) => {
                    this.userdata = userdata;
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    if (appconfig && appconfig.configuration) {
                        this.appconfig = appconfig;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
                }
           );
    }

    ngOnInit() {
        this.isLoading = true;
        this.ismenupage = this.navprams.get('menupage');
        this.appversion_number = environment.version;


        if (this.appconfig && this.appconfig.configuration) {
            this.getTermsData();
        } else {
            this.baserestService.getappConfig().then(
                (appconfig: any) => {
                    this.appconfig = appconfig;
                    this.auth.setappconfig(appconfig);
                    this.statusbar.backgroundColorByHexString(appconfig.configuration.primary);
                    if (appconfig.configuration.statusbartext == 'light') {
                        this.statusbar.styleLightContent();
                    }
                    if (appconfig.configuration.statusbartext == 'dark') {
                        this.statusbar.styleDefault();
                    }
                    this.themechangeService.changetoDynamicTheme(appconfig);
                    this.auth.setappconfig(appconfig);
                    this.getTermsData();
                });
        }

    }
    ngAfterViewInit() {
        if (this.ismenupage) {
            this.termsForm.controls['accepterm'].disable();
        }
    }
    getTermsData() {
        this.baserestService.getTermsandconditionsData(this.userdata.id).then(
            termsdata => { this.termsdata = termsdata; this.setData(termsdata); },
            error => { this.isLoading = false; }
        );
    }
    setData(termsdata) {
        this.termsdata = this.selectedlanguage.terms;
        this.termsmodel = new Termsconditions(termsdata.result);
        this.termsmodel.heading = this.termsmodel.heading ? this.termsmodel.heading : this.termsdata.heading;
        this.isLoading = false;
        this.ref.detectChanges();
    }
    submitTerms() {
        this.isLoading = true;
        this.saveerror = false;
        let activestatus = this.termsForm.value.accepterm == true ? 'aktiv' : 'inaktiv';
        this.baserestService.saveTermsandconditions(activestatus, this.userdata.id).then(
            termssubmitdata => {
                this.termssubmitdata = termssubmitdata; this.isLoading = false;
                this.events.unsubscribe('scroll');
                this.ref.detach();
                if (!(this.navCtrl.getActive().component.name === 'HomePage')) {
                    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
                }
                if (this.platform.width() >= 1400) {
                    let newnavbarlist = [];
                    this.navCtrl.setRoot(HomePage, {}, { animate: false, direction: 'back' });
                    this.auth.setwebtabsContent(null);
                    this.auth.setwebtabstoDisplay(null);
                    this.auth.setdetailContent(null);
                    this.auth.setnavbarlist(newnavbarlist);
                    this.auth.setwebsmartsearchContent('');
                    this.auth.setwebsearchdtlContent('');
                }
            },
            error => {
                this.saveerror = true;
                this.isLoading = false;
                this.saveerrormessage = error.error ? error.error : error.stausText;
            }
        );
    }
}


