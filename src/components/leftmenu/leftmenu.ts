import {
  Component,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  Input,
} from "@angular/core";
import { AlertController, NavParams } from "ionic-angular";
import { Nav, NavController } from "ionic-angular";
import { BaseRestService } from "../../providers/restservice/base.rest.service";
import { AuthService } from "../../providers/authenticationservice/auth.service";
import { environment } from "../../environments/environment";
import { MenugeneratorService } from "../../providers/utils/menugenerator";
import { Appinfo } from "../../providers/utils/appinfo";
import { EventLoggerProvider } from "../../providers/firebase/eventlogger";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { ChangelanguageService } from "../../providers/utils/changelanguageservice";

@Component({
  selector: "leftmenu-viewer",
  templateUrl: "leftmenu.html",
})
export class LeftmenuComponent {
  public pagedata: any;
  //public homedata=tryg;
  private homedata: any;
  public navdata: any;
  public logourl: string;
  @ViewChild(Nav) nav: Nav;
  public currentpageIndex;
  public pagemenuButtons: any = [];
  private loading;
  private pagedataModel;
  // private baseUrl;
  private userinfo: any;
  private isLoading;
  private appconfig;
  private baseUrl;
  private bg_image;
  private positionY = 0;
  private imageUrl: any;
  private authid;
  private detailsdata: any;
  private slctdItem: any;
  private tabs;
  private tabsgroup;
  private itemIndex;
  private pagemenuButtonsjson;
  private autorisation: any;
  private navbarlist;
  public isgroupbuttons = false;
  private url;
  private isdownload = false;
  private isdelete = false;
  private isvitaltest = false;
  private langContent;
  private userlanguage;

  @Input() leftmenuitems: any;

  constructor(
    public navCtrl: NavController,
    private logprovider: EventLoggerProvider,
    private iab: InAppBrowser,
    private alrtCtrl: AlertController,
    public appinfo: Appinfo,
    private menugenerator: MenugeneratorService,
    private lang: ChangelanguageService,
    public auth: AuthService,
    public navParams: NavParams,
    private baserestService: BaseRestService
  ) {
    this.baseUrl = environment.picturesUrl
      ? environment.picturesUrl
      : "https://connect.carex.dk";
    this.auth.user.subscribe((userinfo) => {
      this.userinfo = userinfo;
    });
    this.auth.pagemenuButtons.subscribe((pagemenuButtons) => {
      this.pagemenuButtonsjson = pagemenuButtons;
      this.pagemenuButtons = pagemenuButtons;
      this.itemIndex = null;
    });
    this.auth.appconfig.subscribe((appconfig: any) => {
      this.appconfig = appconfig;
      this.bg_image = appconfig.background_img;
      this.imageUrl = this.baseUrl + appconfig.background_img;
    });
    this.auth.autorisation.subscribe((autorisation) => {
      this.autorisation = autorisation;
    });
    this.auth.navbarlist.subscribe((navbarlist) => {
      this.navbarlist = navbarlist;
    });
    this.lang.userlanguage.subscribe((userlanguage) => {
      if (userlanguage) {
        this.userlanguage = userlanguage;
        this.langContent = this.appconfig.configuration.texts[
          this.userlanguage
        ];
      }
    });
  }

  ngOnInit(): void {
    this.navdata = this.navParams.get("pageData");
    this.currentpageIndex = this.navParams.get("itemIndex");
    if (this.leftmenuitems && this.leftmenuitems.length > 0) {
      this.isLoading = true;
      this.appinfo.setPagemenuItems(this.leftmenuitems);
      this.pagemenuButtons = this.leftmenuitems;
    }
    if (!this.userinfo) {
      this.userinfo = this.navParams.get("userinfo");
    }
  }

  detailsPage(slctdItem, index) {
    this.itemIndex = index;
    this.auth.setSpinner(true);
    //  this.navCtrl.push('webhome',this.pagemenuButtons);
    // for Smart Søg
    if (
      slctdItem.click_action &&
      slctdItem.click_action.includes("objektListSmartSearch")
    ) {
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logpageView(slctdItem.funktionsnavn);
      }
      if (this.navbarlist && this.navbarlist.length) {
        let navlist: any = this.navbarlist;
        navlist.length = 0;
        navlist.push(slctdItem);
        this.navbarlist = navlist;
        this.auth.setnavbarlist(this.navbarlist);
      } else {
        let navlist = [];
        navlist.push(slctdItem);
        this.navbarlist = navlist;
        this.auth.setnavbarlist(this.navbarlist);
      }
      this.baserestService
        .getdetails(
          slctdItem.click_action,
          slctdItem.skema_uuid,
          this.autorisation
        )
        .then(
          (detailsdata) => {
            this.detailsdata = detailsdata;
            this.auth.setwebsmartsearchContent(this.detailsdata);
            this.auth.setSpinner(false);
          },
          (error) => {
            console.log(error);
            this.auth.setSpinner(false);
          }
        );
    } else if (
      slctdItem.click_action &&
      slctdItem.click_action.includes("trivselsbarometer_historik")
    ) {
      if (this.navbarlist && this.navbarlist.length) {
        let navlist: any = this.navbarlist;
        navlist.length = 0;
        navlist.push(slctdItem);
        this.navbarlist = navlist;
        this.auth.setnavbarlist(this.navbarlist);
      }
    } else if (slctdItem.click_action === "externallink") {
      this.url = slctdItem.click_action_parameters.survey_uuid;
      this.openexternalUrl();
    } else {
      this.auth.setwebsmartsearchContent("");
      this.auth.setwebstressContent(null);
      this.auth.setwebcontactContent(null);
      this.auth.setwebsearchdtlContent("");
      this.auth.setwebhistroryquestionarieContent(null);
      this.auth.setwebQuestionaireContent(null);
      this.auth.setwebtabstoDisplay(null);
      // this.auth.setwebtabsContent(null);
      this.menugenerator.detailsPage(slctdItem, this.autorisation);
      if (slctdItem.elementtype === "group_buttons") {
        this.isgroupbuttons = true;
      }
    }
  }

  openexternalUrl() {
    this.isdownload = this.url.indexOf("downloadreport") != -1 ? true : false;
    this.isdelete = this.url.indexOf("delete") != -1 ? true : false;
    this.isvitaltest = this.url.indexOf("vitaltest") != -1 ? true : false;
    if (this.isdelete) {
      let deleteurl = this.url + "userId=" + this.userinfo.id + "&delete=true";
      let alert = this.alrtCtrl.create({
        title: this.langContent.anchor.deletetitle,
        message: this.langContent.anchor.deletemessage,
        buttons: [
          {
            text: this.langContent.anchor.deletecancel,
            role: this.langContent.anchor.deletecancel,
            handler: () => {
              console.log("Cancel clicked");
            },
          },
          {
            text: this.langContent.anchor.deleteconfirm,
            handler: () => {
              this.iab.create( deleteurl, "_system","location=yes,hardwareback=yes");
              if (this.appconfig.environment.trackanalytics === true) {
                this.logprovider.logEvent("VitalDeletereport");
              }
            },
          },
        ],
      });
      alert.present();
    }
    if (this.isdownload) {
      let downlaodurl = this.url + "userId=" + this.userinfo.id;
      this.iab.create(downlaodurl, "_system", "location=yes,hardwareback=yes");
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent("VitalDownloadreport");
      }
    }
    if (this.isvitaltest) {
      let vitalurl = this.url + this.userinfo.id;
      this.iab.create(vitalurl, "_system", "location=yes,hardwareback=yes");
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent("VitalTest");
      }
    }
    if (!this.isdelete && !this.isdownload && !this.isvitaltest) {
      this.iab.create(this.url, "_system", "location=yes,hardwareback=yes");
      if (this.appconfig.environment.trackanalytics === true) {
        this.logprovider.logEvent(this.url);
      }
    }
    this.auth.setSpinner(false);
  }
}
