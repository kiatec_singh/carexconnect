import { Component } from '@angular/core';
import { ModalController, ViewController, Platform } from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { Device } from '@ionic-native/device';
import { FcmProvider } from '../../providers/firebase/firebasemessaging';
import { Appinfo } from '../../providers/utils/appinfo';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
// import { ChangelanguageService } from '../../providers/utils/changelanguageservice';

@Component({
    selector: 'settings-viewer',
    templateUrl: 'settings.html'
})
export class SettingsComponent {

    private settings;
    private appconfig: any;
    private status;
    private userinfo;
    private autorisation;
    private notificationStatus;
    private userobj:any;
    constructor(public modalCtrl: ModalController, private auth: AuthService, private fcm: FCM, private platform: Platform, private fcmprovider: FcmProvider,private lang:ChangelanguageService, private appinfoservice:Appinfo,
        private device: Device, private baserestService: BaseRestService, private viewCtrl: ViewController) {
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if(selectedlanguage){
                this.settings = selectedlanguage.settings;
            }}
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                {
                    this.appconfig = appconfig;
                }
            }
        );
        this.auth.user.subscribe(
            (userinfo) => {
                {
                    this.userinfo = userinfo;
                }
            }
        );
        this.auth.autorisation.subscribe(
            (autorisation) => { this.autorisation = autorisation }
        );
    }

    ngOnInit() {
         //this.userobj = this.appinfoservice.getusermenuObject();
        // this.notificationStatus = this.userobj.appversion.profil.notification && this.userobj.appversion.profil.notification.push? this.userobj.appversion.profil.notification.push : true;
       //  this.settings = this.appconfig.configuration.texts.da.settings;
        // Tracking
        console.log("in settings");
    }
    toogleNotifications(event) {
       // let status = event.checked ===true ?'aktiv': 'inaktiv';
        if (event.checked === true && this.platform.is('cordova')) {
            this.fcmprovider.subscribforNotifications();
        }
        else {
            this.fcmprovider.unsubscribforNotifications(event.checked);
        }
        // this.fcm.getToken().then(token => {
        //     console.log(token);
        //     //   this.registeruserToken(token);
        //     // register this token  to DB
        //     this.baserestService.fcmdeviceRegister(deviceuuid, devicesrno, this.userinfo.id, token, this.autorisation, this.status).then(success => {
        //         console.log(success);
        //     })
        // });
        // ;

    }
    email(event) {
        console.log(event.checked);
    }
    selectLang() {
        // const modal = this.modalCtrl.create(changelanguageComponent, { modal: true });
        // modal.present();
        //     const modal = this.modalCtrl.create(StressmodalPage);
        //     modal.present();
        //   }

    }
    closemodal() {
        this.viewCtrl.dismiss();
    }
}
