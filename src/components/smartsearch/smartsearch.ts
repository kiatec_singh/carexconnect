import { Component, ViewChild, ViewChildren, Input, ChangeDetectorRef } from '@angular/core';
import { NavParams, Navbar, PopoverController, Platform } from 'ionic-angular';
import { Button, NavController, List } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
//import { ViewChildren } from '@angular/core/src/metadata/di';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { SearchDetailsPage } from '../../pages/searchdetails/searchdetails.page';
//  import { CustomanchorComponent } from '../customanchor/customanchor';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { DropDownPopOver } from '../dropdownpopover/dropdownpopover';
import { Keyboard } from '@ionic-native/keyboard';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import { environment } from '../../environments/environment';
import { concat } from 'rxjs/observable/concat';
import { Appinfo } from '../../providers/utils/appinfo';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';



@Component({
  selector: 'smartsearch-viewer',
  templateUrl: 'smartsearch.html'
})
export class SmartsearchComponent {
  private selectedMenuItem;
  private pagetitle;
  private selectedPage;
  private primaryColor;
  private currentTab;
  private secondaryColor;
  private tabsContent: any = [];
  private selectedContainer = 0;
  private searchData: any;
  private dropdownList;
  private searchedList;
  private textfieldList;
  private showsearchList = false;
  @Input() smartsearchwebContent: any;
  @ViewChild(Navbar) navbar: Navbar;
  @ViewChildren('navbuttons') navbuttons: Button;
  @ViewChild('detailsContent') detailsContent;
  @ViewChildren('pager') pager: List;
  private tabs;
  private tabstoDisplay: any = [];
  private showsearchbutton = false;
  private tabsdata;
  private searchContent;
  private loading: boolean;
  private view;
  private searchlabel;
  private user;
  private allsearchresultList;
  private searchdropdownValue;
  private dropdownName;
  private resetlabel;
  private searchdropdownValueArray;
  private emneModel;
  private hovedemneModel;
  private searchtext: any;
  private textfilteredList: any = [];
  private totallist = '0';
  private options: any;
  private allcatalogs: any;
  private movecontentTop = false;
  private movecontentTopEnable = 0;
  private myActiveSlide;
  private hideQuestion = false;
  private hidecontactus = false;
  private hidegird = false;
  private appconfig: any;
  private smartsearchContent: any;
  private childdropdownListvalues: any;
  private childdropdownList: any;
  private backlabel: any;
  private cancel: any;
  private ok: any;
  private filterdesc: any;
  private pictureUrl: any;
  private isfiltered = false;
  private showfilteredlabel: any;
  private parentsearchedList: any = [];
  private navbarlist: any;
  private selectedlanguage;

  private parentdropdownListvalues: any;
  private parentdropdownList: any;
  public appinfo: any

  constructor(private navParam: NavParams,
    private iab: InAppBrowser,
    private popoverCtrl: PopoverController,
    private platform: Platform,
    private lang:ChangelanguageService,
    private keyboard: Keyboard, private auth: AuthService, private ref: ChangeDetectorRef, public pogo: Appinfo, private logprovider: EventLoggerProvider, private navCtrl: NavController, private baserestService: BaseRestService) {

    this.selectedMenuItem = this.navParam.get('item');
    this.searchData = this.navParam.get('smartsearchData');
    this.auth.user.subscribe(
      (user) => {
        {
          this.user = user;
        }
      }
    );
    this.lang.selectedlanguage.subscribe(
      (selectedlanguage: any) => {
              this.selectedlanguage = selectedlanguage;
      }
  );
    this.auth.appconfig.subscribe(
      (appconfig: any) => {
        {
          this.appconfig = appconfig;
        }
      }
    );
    if (!this.searchData) {
      this.auth.websmartsearchContent.subscribe(
        (smartsearchwebContent) => {
          this.searchData = smartsearchwebContent;
          if (this.searchData && this.searchData.status) {
            this.setData();
          }
        }
      );
    }
    this.auth.navbarlist.subscribe(
      (navbarlist) => { this.navbarlist = navbarlist;}
    )


  }
  // this.pagetitle = this.selectedMenuItem[1];
  // this.tabs = this.selectedMenuItem[8];
  ngOnInit() {
    this.pictureUrl = this.appconfig.environment.picturesUrl;
    var tabsdata = [];
    let currentitem: any;
    this.showsearchbutton = true;
    if (this.searchData && this.searchData.status) {
      this.setData();
    }

  }

  setData() {
    let dropdownsdata: any;
    this.totallist = this.searchData.status.paging.total;
    this.searchedList = this.searchData.result.objekt_liste;
    dropdownsdata = this.searchData.result.filter_dropdowns;
    let dropdownlistarray = []
    for (let key in dropdownsdata) {
      dropdownlistarray.push(dropdownsdata[key]);
    }
    this.dropdownList = dropdownlistarray;
    this.parentdropdownList = dropdownlistarray[0];
    this.allsearchresultList = this.searchData.searchlist;
    this.textfilteredList = this.searchData.searchlist;
    this.textfieldList = this.searchData.text;
    this.searchlabel = this.selectedlanguage.smartsearch.showall;
    this.resetlabel = this.selectedlanguage.smartsearch.reset;
    this.pagetitle = this.selectedlanguage.smartsearch.title;
    this.ok = this.selectedlanguage.smartsearch.ok;
    this.cancel = this.selectedlanguage.smartsearch.cancel;
    this.allcatalogs = this.selectedlanguage.smartsearch.foundmessage;
    this.backlabel = this.selectedlanguage.back.title;
    this.filterdesc = this.selectedlanguage.smartsearch.filterdescription;
    this.showfilteredlabel = this.selectedlanguage.smartsearch.showfiltered;
    this.hovedemneModel = this.selectedlanguage.smartsearch.selecthovedenme + "";
  }

  searchDetails(searchsltdItem) {
    let views = this.navCtrl.getViews();
    if (this.appconfig.environment.trackanalytics === true) {
      this.logprovider.logCatalogEvent("Smart søg catalog", searchsltdItem.titel);
    }
    if (this.platform.width() < 1400 || (views && views.length>1)) {
      this.navCtrl.push(SearchDetailsPage, { 'selectedItem': searchsltdItem, 'pagetitle': this.pagetitle });
    }
    else {
      let navbarlist = this.navbarlist;
      navbarlist.push({ funktionsnavn: searchsltdItem.titel })
      this.navbarlist = navbarlist;
      this.auth.setnavbarlist(navbarlist);
      this.pogo.setSearchedList(this.searchedList);
      this.auth.setwebsearchdtlContent(searchsltdItem);
    }

  }

  parentCategory(key, childcontolname, c) {
    if (key.length !== 0 && childcontolname) {
      let filteredList: any = [];
      this.parentsearchedList = this.searchData.result.objekt_liste;
      this.searchedList = this.searchData.result.objekt_liste;
      this.totallist = this.searchData.status.paging.total ? this.searchData.status.paging.total : '0';
      this.dropdownList.forEach(element => {
        if (element.control_name == childcontolname) {
          this.childdropdownListvalues = element.values[key]
          this.childdropdownList = element;
        }
      });
      if (this.childdropdownListvalues && this.childdropdownListvalues.length) {
        this.childdropdownListvalues.forEach(hovedemner => {
          this.searchedList.forEach(element => {
            if (element.tag) {
              if (element.tag.indexOf(hovedemner.id) != -1) {
                filteredList.push(element);
              }
            }
          });
        });
      }
      let uniquefilteredList: any = filteredList.filter(function (item, pos) {
        return filteredList.indexOf(item) == pos;
      });
      this.parentsearchedList = uniquefilteredList;
      this.searchedList = uniquefilteredList;
      this.totallist = this.parentsearchedList.length ? this.parentsearchedList.length : '0';
      this.showsearchList = false;
      this.isfiltered = true;

      if (this.appconfig.environment.trackanalytics === true) {
        let title = c._text.trim();
        this.logprovider.logCatalogEvent("Smart søg", title);
      }
    }


  }
  childCategory(key, childcontolname, d) {
    let title = d._text.trim();
    this.getSearchedList(key,title);

  }

  getSearchedList(key,title) {
    let filteredList: any = [];

    this.parentsearchedList.forEach(element => {
      if (element.tag) {
        if (element.tag.indexOf(key) != -1) {
          filteredList.push(element);
        }
      }
    });

    let uniqueArray: any = filteredList.filter(function (item, pos) {
      return filteredList.indexOf(item) == pos;
    })

    this.searchedList = uniqueArray;
    this.totallist = this.searchedList.length ? this.searchedList.length : '0';
    this.showsearchList = false;
    this.isfiltered = true;
    if (this.appconfig.environment.trackanalytics === true) {
      this.logprovider.logCatalogEvent("Smart søg", title);
    }
  }

  search() {
    this.showsearchList = true;
    if (this.appconfig.environment.trackanalytics === true) {
      this.logprovider.logEvent("Smart søg");
    }
  }
  filteredSearch() {
    this.showsearchList = true;
  }
  reset(event) {

    this.searchedList = this.searchData.result.objekt_liste;
    this.totallist = this.searchData.status.paging.total;
    this.parentdropdownList = this.dropdownList[0];
    this.childdropdownList = null;
    this.childdropdownListvalues = null;
    this.hovedemneModel = null;
    this.emneModel = null;
    this.showsearchList = false;
    this.isfiltered = false;
  }

}
