import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AuthService} from '../../providers/authenticationservice/auth.service';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';

@Component({
  selector: 'back-viewer',
  templateUrl: 'back.html'
})


export class BackComponent {

private title:string;
@Input() pagetitle: string;
private backbuttontext;
private selectedlanguage
  constructor(private navCtrl: NavController, 
    private lang:ChangelanguageService,
    private auth: AuthService) {
    this.auth.subtitle.subscribe(
      (title) => {
          this.title = title;
      }
    );
    this.lang.selectedlanguage.subscribe(
      (selectedlanguage: any) => {
              this.selectedlanguage = selectedlanguage;
      }
  );
  }
  ngOnInit() {
    this.backbuttontext = this.selectedlanguage.back.title;
  }
  goback() {
    this.auth.setheaderpageotopicTitle(null);
    this.navCtrl.pop();
  }
}