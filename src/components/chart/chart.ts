import { Component, Inject, ChangeDetectorRef, Input } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { ViewController, ModalController, Platform } from 'ionic-angular';
import { ChartSelectEvent } from 'ng2-google-charts';
import { DOCUMENT } from '@angular/common';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { CharthistorymodalComponent } from '../charthistorymodal/charthistorymodal';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';

@Component({
  selector: 'chart-viewer',
  templateUrl: 'chart.html'
})
export class ChartComponent {

  private chartData;
  private linechartdata;
  public islist: any;
  public ticks: any;
  public ticksreversed: any;
  @Input() chartdata: any;
  public appconfig;
  public historydetaildata;
  public autorisation;
  private isLoading = false;
  private clickedItem;
  public chartheading;
  public chartContent: any;
  private selectedlanguage;

  constructor(private viewCtrl: ViewController,
    public modalCtrl: ModalController, private lang: ChangelanguageService,
    private baserestService: BaseRestService, public platform: Platform,
    private ref: ChangeDetectorRef, @Inject(DOCUMENT) document,
    private screenorientation: ScreenOrientation, private auth: AuthService) {
    this.auth.islist.subscribe(
      (islist) => {
        this.islist = islist;
      }
    );
    this.auth.autorisation.subscribe(
      (autorisation) => {
        this.autorisation = autorisation;
      }
    );
    this.lang.selectedlanguage.subscribe(
      (selectedlanguage: any) => {
        this.selectedlanguage = selectedlanguage;
      }
    );
  }

  ngOnInit() {
    this.chartContent = this.selectedlanguage.chart;
    if (this.islist.length == 0) {
      this.islist = false;
    }
    this.ticks = this.chartContent.ticks;

    this.ticksreversed = this.ticks.slice().reverse();
    this.generateGraph();
    this.screenorientation.onChange().subscribe(
      () => {
        this.generateGraph();
        this.ref.detectChanges();
      }
    );
  }
  ngAfterViewInit() {

  }
  dismissmodal() {
    this.viewCtrl.dismiss();
  }
  generateGraph() {
    let data = [];
    data.push(
      ['Date', 'Score', { 'type': 'string', 'role': 'style' }, { type: 'string', role: 'annotation' }]
    );
    if (this.chartdata.score.length > 0) {
      for (let index = 0; index < this.chartdata.score.length; index++) {
        if (this.chartdata.score[index].fra) {
          var s = this.chartdata.score[index].fra.replace(/[ :]/g, "-").split("-");
          var datestring = s[2] + '-' + s[1] + '-' + s[0];
          data.push(
            [datestring, this.chartdata.score[index].score, 'point { size: 10; shape-type: circle; fill-color: ' + this.chartdata.score[index].color + ';aktivitet_uuid: ' + this.chartdata.score[index].aktivitet_uuid + '  }', '' + this.chartdata.score[index].score]
          )
        }
      }

      // data.push(["",0,"point { size: 0; shape-type: circle; fill-color: #FFFFFF;aktivitet_uuid: c1717491-d315-4289-bd97-ca0751e71bed  }", "0"])
      let width: any;
      let winwidth = this.platform.width();
      if (winwidth >= 768) {
        if (data.length <= 5) {
          width = 400;
        }
        else {
          width = data.length * 150;
        }
      }
      else {
        if (data.length <= 5) {
          width = 400;
        }
        else {
          width = data.length * 72;
        }
      }
      width.toString();
      let vAxistitle = data.length > 1 ? '' : this.chartContent.chartnodatamsg;

      this.linechartdata = {
        chartType: 'LineChart',
        is3D: true,
        explorer: { axis: 'horizontal', keepInBounds: true },
        dataTable: data,
        options: {
          colors: ['#8e8e8e'],
          // height: 400,
          // width:500,
          lineWidth: 2,
          pointSize: 7,
          focusTarget: 'category',
          is3D: true,
          width: width,
          left: '30',
          height: 430,
          overflow: scroll,
          animation: {
            "startup": true,
            duration: this.chartContent.animationduration,
            easing: 'out'
          },
          backgroundColor: this.chartContent.chartbackgroundcolor,// '#f1f8e9',
          legend: { position: 'none' },
          tooltip: { isHtml: true },
          annotations: {
            textStyle: {
              fontSize: this.chartContent.annotationstextsize,
              bold: true,
              color: this.chartContent.annotationstextcolor,
            },
          },
          chartArea: {
            left: 35,
            right: 30,
            bottom: 60,
            top: 60,
            width: "100%",
            height: "100%"
          },
          hAxis: {
            title: vAxistitle,
            slantedText: true,
            slantedTextAngle: 48,
            textStyle: {
              fontSize: this.chartContent.haxistextsize,
              bold: true,
              color: this.chartContent.haxistextcolor
            }
          },
          vAxis: {
            viewWindow: {
              min: 0,
              max: 100
            },
            ticks: this.ticks,
            textStyle: {
              fontSize: this.chartContent.vaxistextsize,
              bold: true,
              color: this.chartContent.vaxistextcolor
            }
          }
        }
      }

    };



    setTimeout(() => {
      // this.makeSmiles();
    }, 2000);

  }
  makeSmiles() {
    var path_collection = document.getElementsByTagName('path');
    var path_array = Array.prototype.slice.call(path_collection, 0);
    var paths = path_array.filter(function (path) {
      return path.logicalname && path.logicalname.indexOf("line#") > -1;
    });

    paths.forEach(function (path, index1) {
      // convert coordinate format
      path.getAttribute('d').split('M').pop().split('L').forEach(function (str_point, index2) {
        var image = document.createElementNS("http://www.w3.org/2000/svg", "image");
        var point: any = str_point.split(",");
        let xpoint: any = point[0] - 1;
        let ypoint: any = point[1] - 10;
        image.setAttributeNS(null, "x", xpoint);
        image.setAttributeNS(null, "y", ypoint); console.log(point);
        if (ypoint) {
          image.setAttributeNS("http://www.w3.org/1999/xlink", "href", "https://rehapp-test.dk/carex_mobil/login/images/s_green.jpg");
          image.setAttributeNS(null, "height", "20px");
          image.setAttributeNS(null, "width", "20px");
          image.setAttributeNS(null, "border-radius", "2em");
          image.setAttributeNS(null, "style", "cursor:pointer");
        }


        path.parentNode.appendChild(image);
      });
    });
  }
  public select(event: ChartSelectEvent) {
    if (event.selectedRowFormattedValues && event.selectedRowFormattedValues[2]) {
      this.isLoading = true;
      this.clickedItem = event.selectedRowValues;
      let activityuuid = event.selectedRowFormattedValues[2];
      activityuuid = activityuuid.split('aktivitet_uuid:')[1];
      let formattedactivityuuid = activityuuid.replace(/\s/g, '');
      let cleanactivityuuid = formattedactivityuuid.replace('}', '');
      this.baserestService.getquestionairehistroyDetails(this.chartdata.survey_uuid, cleanactivityuuid, this.autorisation).then(
        historydetaildata => { this.historydetaildata = historydetaildata; this.showmodal() }
      )
    }

    // const modal = this.modalCtrl.create(StressmodalPage);
    // modal.present();
  }
  showmodal() {
    const modal = this.modalCtrl.create(CharthistorymodalComponent, { modal: true, 'historydata': this.historydetaildata, 'clickedItem': this.clickedItem });
    this.isLoading = false;
    modal.present();
  }
  closemodal() {
    this.viewCtrl.dismiss();
  }
}
