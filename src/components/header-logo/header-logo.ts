import { Component } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import {environment} from '../../environments/environment';



@Component({
    selector:'headerlogo-viewer',
    templateUrl: 'header-logo.html'
})
export class HeaderLogoComponent {
 
    private baseUrl = "https://connect.carex.dk/";
    private appconfig;
    private logo;
    constructor(private auth: AuthService) {
        this.auth.appconfig.subscribe(
            (appconfig:any) => {
              {
                this.appconfig = appconfig;
              }
            }
          );
        this.baseUrl= environment.contentUrl? environment.contentUrl : 'https://connect.carex.dk';
        this.logo = this.appconfig.headerlogo;
     }

    ngOnInit() {
    
    }
}


