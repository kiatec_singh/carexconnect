import { Component, ViewChild } from '@angular/core';
import { LoginPage } from '../../pages/login/login.page';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { Nav, LoadingController } from 'ionic-angular';
import { StorageService } from '../../providers/storageservice/storageservice';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ThemeChangeService } from '../../providers/restservice/themechange.service';
import { StatusBar } from '@ionic-native/status-bar';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';



@Component({
    selector: 'welcome-viewer',
    templateUrl: 'welcome.html'
})
export class WelcomeComponent {
    @ViewChild(Nav) nav: Nav;

    private welcomedata: any;
    private heading: any;
    private paragraphs;
    private condition;
    private isLoading = false;
    private continue;
    private userchecklistdata;
    private userinfo;
    private appconfig;
    private selectedlanguage:any;

    constructor(private baseservice: BaseRestService, public loadingCtrl: LoadingController, public lang: ChangelanguageService, private themechangeService: ThemeChangeService, private statusbar: StatusBar, private storageService: StorageService,
        private navCtrl: NavController, private auth: AuthService) {
        this.auth.userchecklistdata.subscribe(
            (userchecklistdata) => {
                    this.userchecklistdata = userchecklistdata;
            }
        );
        this.auth.user.subscribe(
            (userinfo) => {
                    this.userinfo = userinfo;
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    this.appconfig = appconfig;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
            }
        );

    }

    ngOnInit() {
        // Tracking
        this.isLoading = true;
        this.getWelcomeData();

    }
    updateCucumber() {
    }
    getWelcomeData() {
        if (this.appconfig && this.appconfig.configuration) {
            this.setData(this.appconfig);
        } else {
            this.baseservice.getappConfig().then(
                (appconfig: any) => {
                    this.appconfig = appconfig;
                    this.auth.setappconfig(appconfig);
                    this.statusbar.backgroundColorByHexString(appconfig.configuration.primary);
                    if(appconfig.configuration.statusbartext == 'light'){
                        this.statusbar.styleLightContent();
                      }
                      if(appconfig.configuration.statusbartext == 'dark'){
                        this.statusbar.styleDefault();
                      }
                    this.themechangeService.changetoDynamicTheme(appconfig);
                    this.auth.setappconfig(appconfig);
                    this.setData(appconfig);
                });
        }
    }
    setData(appconfig) {
        this.welcomedata = this.selectedlanguage.welcome;
        this.heading = this.welcomedata.heading;
        this.paragraphs = this.welcomedata.paragraphs;
        this.condition = this.welcomedata.conditions;
        this.continue = this.welcomedata.continue;
        this.isLoading = false;
    }
    gotoLogin() {
        this.storageService.set('welcome', true);
        this.navCtrl.push(LoginPage, {});
    }
}
