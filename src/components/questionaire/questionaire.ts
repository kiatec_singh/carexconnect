import { Component, Input, ViewChild, ViewChildren, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Slides, NavParams, ViewController, NavController } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { Toast } from '@ionic-native/toast';
import { QuestionaireService } from '../../providers/authenticationservice/questionaire.service';
import { ToastrService } from 'ngx-toastr';
import { Appinfo } from '../../providers/utils/appinfo';
import { HelperSerice } from '../../providers/utils/helperService';
import { QuestionaireresultComponent } from '../questionaireresult/questionaire.result';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';



@Component({
    selector: 'questionaire-viewer',
    templateUrl: 'questionaire.html'
})
export class QuestionaireComponent {

    private questionsForm: FormGroup;
    private questionsData;
    private items: FormArray;
    @ViewChild('questionForm') questionForm;
    @Input() questions: any;
    @ViewChildren('checkbox') Checkbox;
    // @ViewChild(Slides) slides: Slides;

    @ViewChild('questionsslide') questionsslide: Slides;
    @ViewChild('answersslide') answersslide: Slides;


    private checkboxgroup;
    private activeSlidenumber;
    private checkedCount = 0;
    private allcheckeditems: any = [];
    private checkeditems = [];
    private percentage;
    private submitvalid: false;
    private selectedquestionSlide = 0;
    private allquestionsdata: any;
    private pagetitle;
    private heading;
    private allquestions: any;
    public autorisation: any;
    private selectedItem: any;
    private isLoading = false;
    private questionairedataContent: any;
    public resultData: any;
    public appconfig: any;
    private fromnotification = false;
    private selectedlanguage;
    constructor(private auth: AuthService, private lang: ChangelanguageService,
        private ref: ChangeDetectorRef, public viewCtrl: ViewController, private toastr: ToastrService, private helperService: HelperSerice,
        private questionService: QuestionaireService, private navParam: NavParams, private appinfo: Appinfo,
        public navCtrl: NavController, private toastCtrl: Toast, private baserestService: BaseRestService) {
        this.allquestionsdata = this.navParam.get('data');
        this.pagetitle = this.navParam.get('pagetitle');
        this.heading = this.navParam.get('heading');
        this.fromnotification = this.navParam.get('fromnotification');
        this.auth.autorisation.subscribe(
            (autorisation) => {
                this.autorisation = autorisation;
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                if (appconfig) {
                    this.appconfig = appconfig;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if (selectedlanguage) {
                    this.selectedlanguage = selectedlanguage;
                    this.questionairedataContent = selectedlanguage.questionaire;
                }
            }
        );
    }
    ngOnInit() {
        this.isLoading = true;
        this.heading = this.heading ? this.heading : this.selectedlanguage.notifications.pagename;
        this.allquestionsdata = this.appinfo.getQuestionaireData();
        if (this.allquestionsdata.questionaireData) {
            this.allquestionsdata = this.allquestionsdata.questionaireData;
            this.setdata();
        }
    }
    getallQuestions() {
        let actionparameterkey = 'survey_uuid';
        let clickaction = 'ReadSurveyAndAnswers';
        let aktivitet_uuid = this.allquestionsdata.result.aktivitet_uuid ? this.allquestionsdata.result.aktivitet_uuid : '';
        this.baserestService.getquestionairdetails(clickaction, actionparameterkey, this.allquestionsdata.result.survey_uuid, aktivitet_uuid, this.autorisation).then(
            allquestionsdata => {
                this.allquestionsdata = allquestionsdata;
                this.setdata();
            },
            error => {
                this.showError();
                this.isLoading = false;

            }
        );
    }
    setdata() {
        this.allquestions = this.allquestionsdata.result;
        this.getPercentage();
    }
    getPercentage() {
        this.questionService.settotalansweredCountvalue(this.allquestionsdata.result.answers.number_of_answered_questions);
        this.questionService.settotalSlideCountarray(this.allquestionsdata.result.answers.number_of_questions);
        this.submitvalid = this.allquestionsdata.result.answers.afsluttet;
        let percentage: any = this.allquestionsdata.result.answers.number_of_answered_questions == 0 ? 0 : (this.allquestionsdata.result.answers.number_of_answered_questions / this.allquestionsdata.result.answers.number_of_questions) * 100;
        this.questionService.setcompleteProgressvalue(percentage);
        this.isLoading = false;
        if (!this.ref['destroyed']) {
            this.ref.detectChanges();
        }
    }
    calculateprogressbar(data) {
        this.questionService.settotalansweredCountvalue(data.result.number_of_answered_questions);
        this.questionService.settotalSlideCountarray(data.result.number_of_questions);
        this.submitvalid = data.result.afsluttet;
        let percentage: any = data.result.number_of_answered_questions == 0 ? 0 : (data.result.number_of_answered_questions / data.result.number_of_questions) * 100;
        this.questionService.setcompleteProgressvalue(percentage);
    }
    updateChange(event, dropdownitem, item, itemindex) {
        let chcCount = 0;
        this.isLoading = true;

        this.baserestService.updateAnswers(this.allquestions.survey_uuid, this.allquestions.aktivitet_uuid, item.emner[0].uuid, dropdownitem.uuid, this.autorisation).then(
            result => {
                this.getallQuestions();
                this.questionsslide.slideNext(800);
                this.answersslide.slideNext(800);
                this.selectedquestionSlide = this.questionsslide.getActiveIndex();
            }
        )
    }
    prepareCheckbox(selecteditem, selectedcheckbox) {
        var element: any;
        selecteditem.value.checkboxlist.map((item, index) => {
            if (item.label == selectedcheckbox) {
                item.checked = true;
                if (this.allcheckeditems.length == 0) {
                    this.allcheckeditems.push(selecteditem.value.label);
                } else {
                    if (!this.allcheckeditems.includes(selecteditem.value.label)) {
                        this.allcheckeditems.push(selecteditem.value.label);
                    }
                }
            }
            else {
                item.checked = false;
                if (this.allcheckeditems.length > 0 && item.label == selectedcheckbox && this.allcheckeditems.includes(selecteditem.value.label)) {
                    this.allcheckeditems.pop(selecteditem.value.label);
                    let percentage: any = this.allcheckeditems.length == 0 ? 0 : (this.allcheckeditems.length / this.items.controls.length) * 100;
                    this.questionService.setcompleteProgressvalue(percentage);
                }
            }
        });

        let percentage: any = this.allcheckeditems.length == 0 ? 0 : (this.allcheckeditems.length / this.items.controls.length) * 100;
        this.questionService.settotalansweredCountvalue(this.allcheckeditems.length);
        this.questionService.setcompleteProgressvalue(percentage);
        this.answersslide.slideNext();
        this.questionsslide.slideNext();
    }
    swipeQuestions($q) {
        if ($q.offsetDirection == 4 && this.questionsslide.isBeginning() != true) {
            this.answersslide.slidePrev(800);
            this.selectedquestionSlide = this.answersslide.getActiveIndex();
        }
        if ($q.offsetDirection == 2 && this.questionsslide.isEnd() != true) {
            this.answersslide.slideNext(800);
            this.selectedquestionSlide = this.answersslide.getActiveIndex();
        }
        this.questionsslide.stopAutoplay();
        this.answersslide.stopAutoplay();
    }
    swipeEvent($e) {
        console.log("in swyipe");
        if ($e.offsetDirection == 4 && this.answersslide.isBeginning() != true) {
            this.questionsslide.slidePrev(800);
            this.selectedquestionSlide = this.questionsslide.getActiveIndex();
        }
        if ($e.offsetDirection == 2 && this.answersslide.isEnd() != true) {
            this.questionsslide.slideNext(800);
            this.selectedquestionSlide = this.questionsslide.getActiveIndex();
        }
        this.questionsslide.stopAutoplay();
        this.answersslide.stopAutoplay();
    }
    gotoPage(i) {
        this.selectedquestionSlide = i;
        this.questionsslide.slideTo(i, 800);
        this.answersslide.slideTo(i, 800);
    }

    submitQuestionaire() {
        this.isLoading = true;
        this.baserestService.submitQuestionaire(this.allquestions.survey_uuid, this.allquestions.aktivitet_uuid, this.autorisation).then(
            result => { this.getResult() },
            error => { this.showError() }
        )
    }
    getResult() {
        this.isLoading = true;
        this.baserestService.getquestionaireResult(this.allquestions.survey_uuid, this.allquestions.aktivitet_uuid, this.autorisation).then(
            resultData => {
                this.resultData = resultData;
                this.contactPage(resultData);
            },
            error => { this.isLoading = false }
        )

    }
    contactPage(resultData) {
        this.isLoading = false;
        this.navCtrl.push(QuestionaireresultComponent, { modal: true, 'resultData': resultData, 'heading': this.pagetitle, 'allquestions': this.allquestions, 'autorisation': this.autorisation, 'pagename': this.questionairedataContent.pagename });
    }
    showError() {
        this.isLoading = false;
        let isApp = this.helperService.isApp();
        if (isApp === true) {
            const toast = this.toastCtrl.show(this.questionairedataContent.submitsuccessmsg, this.questionairedataContent.toastdration, this.questionairedataContent.toastposition).subscribe(
                toast => {
                    console.log(toast);
                    this.viewCtrl.dismiss();
                });
        }
        else {
            this.toastr.success(this.questionairedataContent.submiterrormsg, this.questionairedataContent.submiterrortitle, {
                timeOut: this.questionairedataContent.toastdration,
                positionClass: this.questionairedataContent.webtoastposition
            });

        }

    }

}
