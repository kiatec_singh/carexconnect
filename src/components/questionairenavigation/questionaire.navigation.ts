import { Component } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { QuestionaireService } from '../../providers/authenticationservice/questionaire.service';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
    selector: 'questionairenavigation-viewer',
    templateUrl: 'questionaire.navigation.html'
})
export class QuestionairenavigationComponent {


    private activeSlide;
    private activeSlidenumber;
    private totalSlideCount;
    private totalansweredCountvalue;
    private totalSlideCountarray;
    private completeProgress;
    private appconfig;
    public questionaireContent;
    private progresscolor;
    private progressbarincomplete;
    private progressbarcomplete;
    private selectedlanguage;


    constructor(private auth: AuthService,
        private lang: ChangelanguageService,
         private questionService: QuestionaireService) {
        this.questionService.completeProgressvalue.subscribe(
            (completeProgress) =>this.completeProgress = completeProgress
        );
        this.questionService.totalansweredCountvalue.subscribe(
            (totalansweredCountvalue) =>this.totalansweredCountvalue = totalansweredCountvalue
        );
        this.questionService.totalSlideCountarray.subscribe(
            (totalSlideCountarray) =>this.totalSlideCountarray = totalSlideCountarray
        );

        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    if(appconfig && appconfig.configuration)
                    this.appconfig = appconfig;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if(selectedlanguage){
                     this.selectedlanguage = selectedlanguage;
                    this.questionaireContent = selectedlanguage.questionaire;
                }
                }
           );
        this.totalansweredCountvalue = this.totalansweredCountvalue ? this.totalansweredCountvalue : 0;
        this.completeProgress = this.completeProgress ? this.completeProgress : 0;
        this.totalSlideCountarray = this.totalSlideCountarray ? this.totalSlideCountarray : 0;
        
    }


    ngOnInit() {
        this.progressbarcomplete = this.appconfig.configuration.progressbarcomplete;
        this.progressbarincomplete = this.appconfig.configuration.progressbarincomplete;
    }
}
