import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Events } from 'ionic-angular';
import { NotificationsPage } from '../../pages/notifications/notifications.page';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
    selector: 'notifications-viewer',
    templateUrl: 'notifications.html'
})
export class NotificationsComponent {
    rootPage: any = NotificationsPage;


    private newnotification;
    public allnotifications;
    private allstorednotifications;
    private headertitle;
    private appconfig;
    private notificationsContent: any;
    private user: any;
    private = false;
    private notifications
    private selectedNotification = null;
    public baseUrl: any = '';
    public logo: any = '';
    public notifycountvalue;
    public autorisation: any;
    public months;
    public isLoading = false;
    private selectedlanguage;

    constructor(
        private ref: ChangeDetectorRef, private basereservice: BaseRestService, private events: Events,
        private lang: ChangelanguageService,  private logprovider: EventLoggerProvider, 
        private auth: AuthService) {
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                    if (appconfig && appconfig.environment) {
                        this.appconfig = appconfig;
                        this.baseUrl = appconfig.environment.picturesUrl;
                        this.logo = appconfig.configuration.headerlogo;
                }
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                if (selectedlanguage) {
                    this.selectedlanguage = selectedlanguage;
                }
            }
        );
        this.auth.notifycount.subscribe(
            (notifycountvalue) => {
                    this.notifycountvalue = notifycountvalue;
            }
        );
        this.auth.user.subscribe(
            (user) => {
                    this.user = user;
            }
        );
        this.auth.autorisation.subscribe(
            (autorisation) => {
                    this.autorisation = autorisation;
            }
        );
        events.subscribe('spinner', (spinner) => {
            this.isLoading = spinner;
            this.ref.detectChanges();
        });
    }
    ngOnDestroy(): void {
        this.events.unsubscribe('spinner');
    }
    ngOnInit() {
        this.isLoading = true;
        this.notificationsContent = this.selectedlanguage.notifications;
        this.months = this.notificationsContent.months;
        this.headertitle = this.notificationsContent.pagename;
        this.basereservice.getnotifications(this.autorisation).then(
            allstorednotifications => {
                this.setData(allstorednotifications);
            },
            error => {
                console.log(error);
                this.isLoading = false;
            }

        );
    }
    setData(notification) {
        let receivednotifications = [];
        if (notification && notification.result && notification.result.length) {
            for (let i in notification.result) {
                if (notification.result[i].sendt && notification.result[i].body_array) {
                    var s = notification.result[i].sendt.replace(/[ :]/g, "-").split("-");
                    var datestring = s[0] + '-' + s[1] + '-' + s[2];
                    var estdate = new Date(datestring);
                    var pointdate = s[2] + '-' + this.months[estdate.getMonth()] + '-' + s[0];
                    let today = notification.result[i].today ? notification.result[i].today : false;
                    receivednotifications.push({
                        header: notification.result[i].header,
                        body: notification.result[i].body_array,
                        timestamp: s[3] + ':' + s[4],
                        logo: notification.result[i].ikon,
                        datestring: datestring,
                        id: notification.result[i].uuid,
                        seen: notification.result[i].seen,
                        icon: null,
                        ready: false,
                        today: today,
                        datestamp: pointdate,
                        fulldate: notification.result[i].sendt
                    })
                }
            }
            receivednotifications.sort(function (a, b) {
                let dateA: any = new Date(a.fulldate);
                let dateB: any = new Date(b.fulldate);
                return dateB - dateA;
            });
            this.allnotifications = receivednotifications;
            this.auth.setnotifycount(notification.status.antal_unread);
        }
        else if (notification && notification.status && notification.status.antal_unread) {
            this.notifycountvalue = notification && notification.status && notification.status.antal_unread;
            this.auth.setnotifycount(notification.status.antal_unread);
        }
        this.isLoading = false;
        this.auth.setSpinner(false);
        if (!this.ref['destroyed']) {
            this.ref.detectChanges();
        }
    }
    closeItem(item, i) {
        this.selectedNotification = null;
    }
    enableread(e, item, i) {
        if (this.selectedNotification === i) {
            this.selectedNotification = null;
            if (!this.ref['destroyed']) {
                this.ref.detectChanges();
            }
        }
        else {
            this.selectedNotification = i;
            if (!this.ref['destroyed']) {
                this.ref.detectChanges();
            }
            if (item.seen != true) {
                this.notifycountvalue = this.notifycountvalue - 1;
                this.auth.setnotifycount(this.notifycountvalue);
                item.seen = true;
                this.allnotifications[i] = item;
                this.ref.detectChanges();
                this.isLoading = true;
                this.basereservice.readnotification(item.id, this.autorisation).then(
                    readsuccess => {
                        this.isLoading = false;
                    },
                    error => {
                        this.isLoading = false;
                    }
                );
            }
        }

    }

}
