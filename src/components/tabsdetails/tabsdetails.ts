import { Component, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { Content } from 'ionic-angular';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';


@Component({
    selector: 'tabsdetails-viewer',
    templateUrl: 'tabsdetails.html'
})
export class tabsdetailsComponent {


    @Input() tabs: any;
    @ViewChild(Content) content: Content;
    public appconfig;
    public menuactive_color;
    public menuinactive_color;
    public menuinactivetext_color;
    public pagecontenttext_color;
    public menuactivetext_color;
    public pagecontent_color;
    public slcIndex = 0;
    private autorisation;
    private textContent;
    private isLoading;
    private appinfo;
    public slctItem;

    constructor(private auth: AuthService, private baserestService: BaseRestService, private logprovider: EventLoggerProvider) {
        this.auth.autorisation.subscribe(
            (autorisation) => {
                    this.autorisation = autorisation;
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig) => {
                this.appconfig = appconfig;
            }
        )
    }

    ngOnInit() {
        let slcttopic: any;
        if (this.tabs.length > 0) {
            slcttopic = this.tabs[0] && this.tabs[0].name ? this.tabs[0].name : this.tabs[0];
            this.auth.setheaderpageotopicTitle(slcttopic);
            if (this.appconfig.environment.trackanalytics === true) {
                this.logprovider.logEvent(slcttopic);
            }
        }

    }
    selectDetail(slcItem, slctIndex) {
        this.isLoading = true;
        this.slctItem = slcItem;
        this.slcIndex = slctIndex;
        if (this.appconfig.environment.trackanalytics === true) {
            this.logprovider.logEvent(slcItem.name);
        }
        this.baserestService.getdetails(slcItem.action, slcItem.id, this.autorisation).then(
            detailsdata => {
                this.textContent = detailsdata;
                this.changeContent(detailsdata);;
            },
            error => { this.isLoading = false; }
        )
    }
    changeContent(detailsdata) {
        this.isLoading = false;
        this.auth.setdetailContent(detailsdata.indhold.indhold);
        if (this.slctItem.name) {
            this.auth.setheaderpageotopicTitle(this.slctItem.name);
        }
    }
}
