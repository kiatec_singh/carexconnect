import { Component, NgZone, ChangeDetectorRef } from '@angular/core';
import { NavParams, ViewController, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { Platform } from 'ionic-angular/platform/platform';
import { ModalController } from 'ionic-angular';
import { StressmodalPage } from '../../pages/stressmodalpage/stressmodal.page';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { Appinfo } from '../../providers/utils/appinfo';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';



@Component({
    selector: 'stress-viewer',
    templateUrl: 'stress.html'
})
export class StressComponent {

    private searchselectedItem;
    private heading;
    private image;
    private teaser;
    private detailsparagraphs;
    private pagetitle: any;
    private bilagurl;
    private bilagName;
    private islist = true;
    private histroyData: any;
    private autorisation: any;
    private uuid: any;
    private histroydataArray = [];
    private stressdata: any;
    private sucessdata: any;
    private months = [];
    private isLoading = false;
    private selectedItem: any;
    private dateSorted = true;
    private categorySorted = false;
    private ascdateOrder = false;
    private asccategoryOrder = false;
    private appconfig: any;
    private stressContent: any;
    private showdots = true;
    private clickedItem: any;
    private draggeditemIndex = undefined;
    public scltdId: any;
    public slctdAction: any;
    public webstressContent: any;
    private activeitemSlide: any;
    private isopened = false;
    private selectedlanguage;
    constructor(public modalCtrl: ModalController, private lang :ChangelanguageService,
        private alrtCtrl: AlertController, private navParams: NavParams, private ref: ChangeDetectorRef,
         private appinfo: Appinfo, private viewCtrl: ViewController, private platform: Platform, 
         private baserestService: BaseRestService, private auth: AuthService) {
        this.auth.autorisation.subscribe(
            (autorisation) => {
                    this.autorisation = autorisation;
                }
        );
        this.auth.appconfig.subscribe(
            (appconfig) => {
                    this.appconfig = appconfig;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
                }
           );
        this.histroyData = this.navParams.get('data') ? this.navParams.get('data') : '';
        this.pagetitle = this.navParams.get('pagetitle') ? this.navParams.get('pagetitle') : '';
        this.selectedItem = this.navParams.get('selectedItem') ? this.navParams.get('selectedItem') : '';
        this.heading = this.navParams.get('heading') ? this.navParams.get('heading') : '';
        this.uuid = this.selectedItem.uuid ? this.selectedItem.uuid : '';
    }

    ngOnInit() {
        this.selectedItem = this.selectedItem ? this.selectedItem : this.appinfo.getSlctdItem();
        if (this.selectedItem) {
            if (this.selectedItem.id) {
                this.scltdId = this.selectedItem.id;
                this.slctdAction = this.selectedItem.action
            } else {
                this.scltdId = this.selectedItem.item.skema_uuid;
                this.slctdAction = this.selectedItem.item.click_action;
            }
        }
        this.auth.webstressContent.subscribe(
            (webstressContent) => {
                {
                    this.webstressContent = webstressContent;
                }
            }
        );
        this.isLoading = true;
        console.log(this.histroyData.result);

        console.log(this.appconfig);
        this.stressContent =  this.selectedlanguage.stress;
        this.months = this.stressContent.months;
        this.histroyData = this.histroyData ? this.histroyData : this.webstressContent;
        this.setData();
    }
    setData() {
        let alldata = [];
        let dateobject: any;
        for (let i in this.histroyData.result) {
            if (this.histroyData.result[i].fra) {
                var s = this.histroyData.result[i].fra.replace(/[ :]/g, "-").split("-");
                var d = new Date(s[0], s[1], s[2], s[3], s[4], s[5]);
                let mm = d.getMonth() - 1;
                let dag = d.getDate();
                let year = d.getFullYear();
                let month = this.months[mm];
                let currentdate = new Date();
                let valid = currentdate > d ? false : true;
                dateobject = {
                    day: dag,
                    year: year,
                    month: month,
                    datestring: d
                }
                alldata.push({
                    date: dateobject,
                    topic: this.histroyData.result[i].emne,
                    category: Object.keys(this.histroyData.result[i])[1],
                    uuid: this.histroyData.result[i].uuid,
                    showdots: true,
                    valid: valid
                })
            }

        }
        alldata.sort(function (a, b) {
            let dateA: any = new Date(a.date.datestring);
            let dateB: any = new Date(b.date.datestring);
            return dateB - dateA;
        });
        this.histroydataArray = alldata;
        console.log(alldata);
        this.isLoading = false;
    }

    openModal(item) {
        this.clickedItem = item;
        this.isLoading = true;
        this.baserestService.gethistroyDetails(this.scltdId, item.uuid, this.autorisation, this.histroyData.row.row_click_action).then(
            stressdata => { this.stressdata = stressdata; this.showmodal() }
        )

    }
    sortbyDate() {
        this.dateSorted = true;
        this.categorySorted = false;
        this.ascdateOrder = this.ascdateOrder === false ? true : false;
        if (this.ascdateOrder === false) {
            this.histroydataArray.sort(function (a, b) {
                let dateA: any = new Date(a.date.datestring);
                let dateB: any = new Date(b.date.datestring);
                return dateB - dateA;
            });
        }
        if (this.ascdateOrder === true) {
            this.histroydataArray.sort(function (a, b) {
                let dateA: any = new Date(a.date.datestring);
                let dateB: any = new Date(b.date.datestring);
                return dateA - dateB;
            });
        }


    }
    sortbyCategory() {
        this.dateSorted = false;
        this.categorySorted = true;
        this.asccategoryOrder = this.asccategoryOrder === false ? true : false;
        if (this.asccategoryOrder === false) {
            this.histroydataArray.sort(function (a, b) {
                let titleA = a.topic.toLowerCase();
                let titleB = b.topic.toLowerCase();
                if (titleA < titleB) return 1;
                if (titleA > titleB) return -1;
                return 0;
            });
        }
        if (this.asccategoryOrder === true) {
            this.histroydataArray.sort(function (a, b) {
                let titleA = a.topic.toLowerCase();
                let titleB = b.topic.toLowerCase();
                if (titleA < titleB) return -1;
                if (titleA > titleB) return 1;
                return 0;
            });
        }
    }
    showmodal() {
        const modal = this.modalCtrl.create(StressmodalPage, { modal: true, 'stressdata': this.stressdata, 'clickedItem': this.clickedItem });
        this.isLoading = false;
        modal.present();
    }
    closemodal() {
        this.viewCtrl.dismiss();
    }
    deleteItem(itemuuid) {
        let alert = this.alrtCtrl.create({
            title: this.stressContent.deletetitle,
            message: this.stressContent.deletemessage,
            buttons: [
                {
                    text: this.stressContent.deletecancel,
                    role: this.stressContent.deletecancel,
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.stressContent.deleteconfirm,
                    handler: () => {
                        this.isLoading = true;
                        this.baserestService.deletehistroyItem(this.scltdId, itemuuid, this.autorisation).then(
                            sucessdata => {
                                this.sucessdata = sucessdata; this.gethistroyData();
                            }
                        )
                    }
                }
            ]
        });
        alert.present();
    }
    itemdraged(event, item, i, itemSlide) {
        this.activeitemSlide = itemSlide;
        this.draggeditemIndex = this.draggeditemIndex === undefined ? i : this.draggeditemIndex;
        if ((this.draggeditemIndex != i) == true) {
            this.histroydataArray[this.draggeditemIndex].showdots = true;
        }
        if (event.getSlidingPercent() === 1) {
            item.showdots = false;
            this.ref.detectChanges();
        }
        else if (event.getSlidingPercent() < 1) {
            this.histroydataArray[this.draggeditemIndex].showdots = true;
            this.activeitemSlide.close();
            this.activeitemSlide = null;
            this.ref.detectChanges();
        }
        this.draggeditemIndex = i;
    }

    gethistroyData() {
        this.baserestService.getdetails(this.slctdAction, this.scltdId, this.autorisation).then(
            histroyData => {
                this.histroyData = histroyData;
                this.setData();
            },
            error => { this.isLoading = false }
        )
    }
    customToogle() {
        this.islist = this.islist ? false : true;
        console.log(this.islist);
    }

    showlist() {
        this.islist = true;
        console.log(this.islist);
        this.auth.setislist(true);
    }
    showgraph() {
        this.islist = false;
        console.log(this.islist);
        this.auth.setislist(false);
    }
    openItem(event, itemSlide, item, i) {
        console.log(itemSlide);
        itemSlide._openAmount=0;
        itemSlide._startX=0;
        this.draggeditemIndex = this.draggeditemIndex === undefined ? i : this.draggeditemIndex;
        console.log("in open");
        event.stopPropagation();
        if (!this.activeitemSlide) {
            this.activeitemSlide = itemSlide;
            itemSlide.moveSliding(-150);
            itemSlide.moveSliding(-150);
            this.histroydataArray[this.draggeditemIndex].showdots = false;
        }
        else if (this.activeitemSlide && this.activeitemSlide.item) {
            if (itemSlide.item.id === this.activeitemSlide.item.id) {
                this.activeitemSlide.close();
                this.activeitemSlide = null;
                this.histroydataArray[this.draggeditemIndex].showdots = true;
            }
           else if (itemSlide.item.id != this.activeitemSlide.item.id) {
                this.activeitemSlide.close();
                this.activeitemSlide = null;
                this.activeitemSlide = itemSlide;
                itemSlide.moveSliding(-150);
                itemSlide.moveSliding(-150);
                this.histroydataArray[this.draggeditemIndex].showdots = false;
            }
        }
        this.draggeditemIndex = i;
        this.ref.detectChanges();
    }
}