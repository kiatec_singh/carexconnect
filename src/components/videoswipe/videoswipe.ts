import { Component, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import { Slides } from 'ionic-angular';
import { YoutubePlayerModule } from 'ngx-youtube-player';

@Component({
    selector: 'videoswipe-viewer',
    templateUrl: 'videoswipe.html',
})
export class VideoswipeComponent {

    private safevideolist = [];
    private safeurls = [];
    private imgurls = [];
    private heading = '';
    private teaser = '';
    private iframereceiveddata: any;
    @ViewChild('videoSlide') slides: Slides;
    @ViewChild('headingSlide') hdngslides: Slides;
    private ik_player: any;
    private atvIndx = 0;
    private headingatvIndx = 0;
    private videoPlayed = false;
    private appconfig: any;
    // player: YT.Player;
    @Input() alliframes: any;
    constructor(private logprovider: EventLoggerProvider, public auth: AuthService, private palyer: YoutubePlayerModule, private ref: ChangeDetectorRef) {
        this.auth.appconfig.subscribe(
            (appconfig) => {
                this.appconfig = appconfig;
            }
        )
        this.auth.iframes.subscribe(
            (iframes) => {
                this.alliframes = iframes;
                this.createIframes();
            }
        )
    }
    ngOnInit() {
        this.createIframes();
    }
    ngOnDestroy(): void {
        this.iframereceiveddata = [];
        this.alliframes = [];
    }
    createIframes() {
        this.alliframes;
        let alliframesdata = [];
        let headingcontent: string;
        let iframeurl: string;
        let videoid: string;
        let isyoutubevideo:any;
        var regEx = /(src|width|height)=["']([^"']*)["']/gi;
        this.alliframes.forEach(element => {
            if (element.indexOf('<iframe') > 0) {
                headingcontent = element.slice(0, element.indexOf('iframe') - 1);
                element.replace(regEx, function (all, type, value) {
                    if (type == 'src') {
                        iframeurl = value;
                        videoid = value.substring(value.lastIndexOf("/") + 1, value.length);
                    }
                    isyoutubevideo = value.indexOf('youtube')!=-1 ? 'true':'false';
                });
                alliframesdata.push({
                    heading: headingcontent,
                    url: iframeurl,
                    id: videoid,
                    isyoutube:isyoutubevideo
                });
            }
        });
        this.iframereceiveddata = alliframesdata;
        setTimeout(() => {
            this.hdngslides.lockSwipes(true);
            this.hdngslides.update();
            this.slides.update();
            if (this.iframereceiveddata.length == 1) {
                this.slides.slideTo(0);
                this.hdngslides.slideTo(0);
            }
        }, 1500);
    }
    swipeEvent($e) {
        if ($e.offsetDirection == 4) {
            // Swiped right, for example:
            this.slides.slidePrev(1000);
            this.hdngslides.slidePrev(1000);
        }
        if ($e.offsetDirection == 2) {
            // Swiped left, for example:
            this.slides.slideNext(1000);
            this.hdngslides.slideNext(1000);
        }
        this.slides.stopAutoplay();
        this.hdngslides.stopAutoplay();
    }
    onStateChange(event) {
        if (event.data == 1) {
            this.videoPlayed = true;
            this.ref.detectChanges();
            if (this.appconfig.environment.trackanalytics === true) {
                let youtubeUrl = event.target.getVideoUrl();
                this.logprovider.logyoutubeEvent('youutube', youtubeUrl);
            }
        }
        if (event.data == 2) {
            this.videoPlayed = false;
        }
    }
    forward(){
        this.slides.slideNext(1000);
        this.hdngslides.slideNext(1000);
    }
    backward(){
        this.slides.slidePrev(1000);
        this.hdngslides.slidePrev(1000);
    }
    updateNumbers(event) {
        this.hdngslides.lockSwipes(false);
        this.atvIndx = this.slides.getActiveIndex();
        this.hdngslides.slideTo(this.slides.getActiveIndex());
        this.hdngslides.lockSwipes(true);
    }
    playVideo(e) {
        this.slides.stopAutoplay();
        this.hdngslides.stopAutoplay();
    }
}

