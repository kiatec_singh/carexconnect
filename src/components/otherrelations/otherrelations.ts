import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { EventLoggerProvider } from '../../providers/firebase/eventlogger';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
  selector: 'otherrelations-viewer',
  templateUrl: 'otherrelations.html'
})
export class OtherrelationsComponent {

  private heading;
  private paragraphs;
  private loading;
  private ismenupage;
  private otherRelations;
  private baseUrl = "https://connect.carex.dk/";
  private appconfig;
  private anroidlogo;
  private ioslogo;
  private selectedlanguage;

  constructor(private navprams: NavParams, private logprovider: EventLoggerProvider, private lang: ChangelanguageService, private auth: AuthService) {
    this.auth.appconfig.subscribe(
      (appconfig: any) => {
        if (appconfig && appconfig.environment) {
          this.appconfig = appconfig;
          this.baseUrl = appconfig.environment.picturesUrl;
        }
      }
    );
    this.lang.selectedlanguage.subscribe(
      (selectedlanguage: any) => {
        this.selectedlanguage = selectedlanguage;
      }
    );
  }

  ngOnInit() {
    // Tracking
    this.ismenupage = this.navprams.get('menupage');
    this.otherRelations = this.selectedlanguage.otherrelations;
    this.ioslogo = this.otherRelations.appstoreimage;
    this.anroidlogo = this.otherRelations.playstoreimage;
    this.setData();
  }

  setData() {
    this.loading = false
    this.heading = this.otherRelations.heading;
    this.paragraphs = this.otherRelations.paragraphs;
    if (this.appconfig.environment.trackanalytics === true) {
      this.logprovider.logpageView('otherrelationspage');
    }
  }
}


