import { Component, Input} from '@angular/core';

@Component({
    selector: 'customiframe-viewer',
    templateUrl: 'customiframe.html'
})
export class CustomiframeComponent {
    @Input() iframeUrl: string;
    constructor() { }
    ngOnInit() {
        console.log("in iframe");
    }
}