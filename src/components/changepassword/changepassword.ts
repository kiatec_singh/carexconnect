import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { StorageService } from '../../providers/storageservice/storageservice';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { PasswordValidation } from '../../providers/validators/password-validator';
import { HomePage } from '../../pages/home/home.page';
import { CPRPage } from '../../pages/cpr/cpr.page';
import { IdverifyPage } from '../../pages/idverify/idverify.page';
import { LoadingController } from 'ionic-angular';
import { TermsconditionPage } from '../../pages/termsconditions/termsconditions.page';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';


@Component({
    selector: 'changepassword-viewer',
    templateUrl: 'changepassword.html'
})
export class changepasswordComponent {

    rootPage: any;
    private loginForm: FormGroup;
    private userInfo;
    private password;
    private confirmPassword;
    private userid;
    private cp_content;
    private userchecklistdata;
    private isLoading= false;
    private serviceerror = false;
    private serviceerrormessage;
    public checklistdata: any;
    private selectedlanguage;



    constructor(private fb: FormBuilder, private navCtrl: NavController, private lang: ChangelanguageService, private ref: ChangeDetectorRef, public navParams: NavParams,
        public loadingCtrl: LoadingController,
        private auth: AuthService, private baserestService: BaseRestService, private storageService: StorageService) {
        this.loginForm = this.fb.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        }, {
                validator: PasswordValidation.MatchPassword
            });

        this.auth.userchecklistdata.subscribe(
            (userchecklistdata) => {
                    this.userchecklistdata = userchecklistdata;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (selectedlanguage: any) => {
                    this.selectedlanguage = selectedlanguage;
            }
        );
    }

    ngOnInit() {
        this.userid = this.navParams.get('userid');
        this.cp_content = this.selectedlanguage.changepassword;
    }

    gotologin() {
        this.isLoading = true;
        this.serviceerror = false;
        this.password = this.loginForm.value.password;
        this.confirmPassword = this.loginForm.value.confirmPassword;
        this.baserestService.changePassword(this.userid.id, this.password).then(
            (userInfo) => { 
                this.userInfo = userInfo; 
                this.checkactivestatus(); 
            },
            error => {
                this.serviceerror = true;
                this.serviceerrormessage = error && error.error ? error.error : error.statusText;
                console.log(error.error);
            }
        )
    }

    checkactivestatus() {
        this.serviceerror = false;
        this.baserestService.checkactiveList(this.userid.id).then(
            checklistdata => {
                this.checklistdata = checklistdata;
                this.storageService.set('checklistdata', checklistdata);
                this.auth.setuserchecklistData(checklistdata);
                this.auth.setUserinfo(this.userInfo);
                this.storageService.set("user", this.userInfo);
                this.decideflow(checklistdata);
            },
            error => {
                this.serviceerror = true;
                this.isLoading = false;
                this.serviceerrormessage = error && error.error ? error.error.besked : error.statusText;
            }
        );
    }
    decideflow(checklistdata) {
        this.isLoading = false;
        if (this.checklistdata && this.checklistdata.result && this.checklistdata.result.generalaccept) {
            this.storageService.set('terms', this.checklistdata.result.generalaccept);
        }
        if (!this.checklistdata.result || this.checklistdata.result.length == 0) {
            this.navCtrl.push(IdverifyPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && !this.checklistdata.result.cpr && !this.checklistdata.result.nemid) {
            this.navCtrl.push(IdverifyPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && this.checklistdata.result.cpr === false && this.checklistdata.result.nemid) {
            this.navCtrl.push(CPRPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && this.checklistdata.result.cpr === true && this.checklistdata.result.nemid) {
            this.navCtrl.push(TermsconditionPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && this.checklistdata.result.generalaccept === true && this.checklistdata.result.cpr === false && this.checklistdata.result.nemid) {
            this.navCtrl.push(CPRPage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && this.checklistdata.result.generalaccept === true && this.checklistdata.result.cpr === true && this.checklistdata.result.nemid) {
            this.navCtrl.push(HomePage);
            return;
        }
        if (this.checklistdata && this.checklistdata.result && !this.checklistdata.result.generalaccept && this.checklistdata.result.cpr === true && this.checklistdata.result.nemid) {
            this.navCtrl.push(TermsconditionPage);
            return;
        }

    }
    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.ref.detach();
       
    }
    resetForm() {
        this.serviceerror = false;
        this.loginForm.reset();
    }
    inputvalueChange() {
        this.serviceerror = false;
        this.ref.reattach();
         this.ref.detectChanges();
    }
}