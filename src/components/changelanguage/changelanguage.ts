import { Component, ViewChild } from '@angular/core';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { Nav, LoadingController, NavParams, ViewController, Platform } from 'ionic-angular';
import { StorageService } from '../../providers/storageservice/storageservice';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ChangelanguageService } from '../../providers/utils/changelanguageservice';
import { WelcomePage } from '../../pages/welcome/welcome.page';
import { ThemeChangeService } from '../../providers/restservice/themechange.service';
import { Toast } from '@ionic-native/toast';
import { StatusBar } from '@ionic-native/status-bar';
import { ToastrService } from 'ngx-toastr';
import { HelperSerice } from '../../providers/utils/helperService';



@Component({
    selector: 'changelanguage-viewer',
    templateUrl: 'changelanguage.html'
})
export class changelanguageComponent {

    rootPage: any;
    @ViewChild(Nav) nav: Nav;
    private changelangdata: any;
    private heading: any;
    private paragraphs;
    private condition;
    private isLoading = false;
    private continue;
    private userchecklistdata;
    private userinfo;
    private appconfig;
    private alllanguages = [];
    public baseUrl = '';
    private frommodal: boolean;

    constructor(private baserestService: BaseRestService, private statusbar: StatusBar, public viewCtrl: ViewController, public lang: ChangelanguageService, private toastr: ToastrService,
        public loadingCtrl: LoadingController, private navParams: NavParams,
        private toastCtrl: Toast, private themechangeService: ThemeChangeService,
        private helperService: HelperSerice,
        private storageService: StorageService, private navCtrl: NavController, private auth: AuthService) {
        this.auth.userchecklistdata.subscribe(
            (userchecklistdata) => {
                this.userchecklistdata = userchecklistdata;
            }
        );
        this.auth.user.subscribe(
            (userinfo) => {
                this.userinfo = userinfo;
            }
        );
        this.auth.appconfig.subscribe(
            (appconfig: any) => {
                this.appconfig = appconfig;
            }
        );
        this.lang.selectedlanguage.subscribe(
            (changelangdata: any) => {
                if (changelangdata) {
                    this.changelangdata = changelangdata.language;
                }
            }
        );
    }

    ngOnInit() {
        this.frommodal = this.navParams.get('modal');
        this.isLoading = true;
        if (this.appconfig && this.appconfig.configuration) {
            this.getLanguagesData();
        } else {
            this.baserestService.getappConfig().then(
                (appconfig: any) => {
                    this.auth.setappconfig(appconfig);
                    this.statusbar.backgroundColorByHexString(appconfig.configuration.primary);
                    if (appconfig.configuration.statusbartext == 'light') {
                        this.statusbar.styleLightContent();
                    }
                    if (appconfig.configuration.statusbartext == 'dark') {
                        this.statusbar.styleDefault();
                    }
                    this.themechangeService.changetoDynamicTheme(appconfig);
                    this.getLanguagesData();
                });
        }
    }
    getLanguagesData() {
        let languages = [];
        for (var key in this.appconfig.configuration.texts) {
            languages.push(this.appconfig.configuration.texts[key].language);
        }
        this.baseUrl = this.appconfig.environment.picturesUrl;
        this.alllanguages = languages;
        this.isLoading = false;
    }
    closemodal() {
        this.viewCtrl.dismiss();
    }
    selectLang(userLang) {
        this.isLoading = true;
        let lang = userLang.lang;
        this.storageService.set('selectedlanguage', lang);
        this.lang.setselectedlanguge(lang);
        this.lang.setuserlang(lang);
        if (this.frommodal === true) {
            this.baserestService.saveLanguage(this.userinfo.id, lang).then(
                languagechanged => {
                    this.isLoading = false;
                    this.successblock();
                },
                error => {
                this.isLoading = false; this.viewCtrl.dismiss();
                    this.errorblock();
                }
            )
        }
        else {
            this.isLoading = false;
            this.navCtrl.push(WelcomePage, {});
        }
    }
    successblock() {
        this.isLoading = false;
        let isapp = this.helperService.isApp();
        if (isapp === true) {
            const toast = this.toastCtrl.show(this.changelangdata.submitsuccessmsg, this.changelangdata.toastdration, this.changelangdata.toastposition).subscribe(
                toast => {
                    this.viewCtrl.dismiss();
                });
        }
        else {
            this.toastr.success(this.changelangdata.submitsuccessmsg, this.changelangdata.submitsuccesstitle, {
                timeOut: this.changelangdata.toastdration,
                positionClass: this.changelangdata.webtoastposition
            });
            this.viewCtrl.dismiss();

        }
    }
    errorblock() {
        this.isLoading = false;
        let isApp = this.helperService.isApp();
        if (isApp === true) {
            const toast = this.toastCtrl.show(this.changelangdata.submiterrormsg, this.changelangdata.toastdration, this.changelangdata.toastposition).subscribe(
                toast => {
                    this.viewCtrl.dismiss();
                });
        }
        else {
            this.toastr.success(this.changelangdata.submiterrormsg, this.changelangdata.submiterrortitle, {
                timeOut: this.changelangdata.toastdration,
                positionClass: this.changelangdata.webtoastposition
            });
            this.viewCtrl.dismiss();
        }
    }
}
