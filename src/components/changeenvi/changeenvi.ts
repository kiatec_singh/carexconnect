import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StorageService } from '../../providers/storageservice/storageservice';
import { BaseRestService } from '../../providers/restservice/base.rest.service';
import { AuthService } from '../../providers/authenticationservice/auth.service';
import { LoadingController } from 'ionic-angular';



@Component({
    selector: 'changeenvi-viewer',
    templateUrl: 'changeenvi.html'
})
export class changeenviComponent {

    rootPage: any;
    private loginForm: FormGroup;
    private info = false;
    private envi = true;
    public checklistdata: any; s
    public appconfig;
    public currentEnvi;
    public appVersion_no;
    private touched = false;
    public enviModel = null;
    public appModel = null;



    constructor(private fb: FormBuilder, private viewCtrl: ViewController, private params: NavParams, private ref: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private auth: AuthService, private baserestService: BaseRestService, private storageService: StorageService) {
        this.currentEnvi = this.params.get('environment')
        this.appVersion_no = this.params.get('appVersion');
        this.appconfig = this.params.get('appconfig');
    }

    ngOnInit() {

    }
    toogleView() {
        this.envi = this.envi === true ? false : true;
        this.info = this.info === true ? false : true;
    }

    ChangeEnvi(value) {
        this.touched = true;
        console.log(value);

    }
    Close() {
        this.viewCtrl.dismiss();

    }
    ChangeApp(value) {
        this.touched = true;
        console.log(value);
    }
    Changereload() {
        console.log("Change App")
     //   this.storageService.clear();
        this.storageService.set('Envi', this.enviModel);
        this.storageService.set('Appmodel', this.appModel);
        //this.viewCtrl.dismiss();
        //needds to revert
       // this.navCtrl.setRoot(WelcomePage);
    }
}